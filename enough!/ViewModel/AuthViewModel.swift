//
//  AuthViewModel.swift
//  enough!
//
//  Created by Pankaj Sonava on 08/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class AuthViewModel: NSObject {
    var countryList = [Country]()
    
    func getCountryList(viewcontroller:UIViewController,callback:@escaping () -> Void){
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.countryList_endPoint, param: nil, viewcontroller: viewcontroller) { (json) in
            if let dic = json?.dictionary{
                if let country_list = dic["data"]?.array{
                    for countryDic in country_list {
                        let cntry = Country(dic: countryDic.dictionary!)
                        self.countryList.append(cntry)
                    }
                }
                callback()
            }
        }
    }
    func registerUser(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.register_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                callback(dic)
            }
        }
    }
    func loginUser(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.login_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                callback(dic)
            }
        }
    }
    func updateUser(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.updateUser_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
    func mobileVerify(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.mobileVerify_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
    func getVerifiedStatus(viewController:UIViewController,callback:@escaping () -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getVerifiedStatus_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let verifyStatus = dic["data"]?.dictionary{
                    Enough.shared.loginUser.mobile_verify_status = verifyStatus["mobile_verify_status"]?.string
                    Enough.shared.loginUser.email_verify_status = verifyStatus["email_verify_status"]?.string
                    Enough.shared.loginUser.id_verify_status = verifyStatus["id_verify_status"]?.string
                }
                callback()
            }
        }
    }
}
