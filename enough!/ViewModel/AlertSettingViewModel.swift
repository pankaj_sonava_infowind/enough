//
//  AlertSettingViewModel.swift
//  enough!
//
//  Created by mac on 27/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//


import UIKit
import SVProgressHUD
import SwiftyJSON

class AlertSettingViewModel: NSObject {

    var getAlertSettingArr:[String:String] = [:]
  
    var alertDistance = ""
    var alert_Community = ""
    var alert_emergency_type = ""
    var alert_time_diffrance = ""
    var alert_status = ""
    
    var alert_list = [Alert]()
    func getDistanceRequestValue(uiValue:String) -> String {
        if uiValue == "100 meters from my location"{
            return "less_than_100"
        }else if uiValue == "100m to 500m from my location"{
            return "between_100_to_500"
        }else if uiValue == "500m to 1km from my location"{
            return "between_500_to_1km"
        }else if uiValue == "Over 1km from my location"{
            return "over_1km"
        }
        return ""
    }
    func getEmergencyRequestValue(uiValue:String) -> String {
        if uiValue == "All"{
            return ""
        }else if uiValue == "Domestic"{
            return "Domestic"
        }else if uiValue == "Public Space Indoor"{
            return "Indoor"
        }else if uiValue == "Public Space Outdoor"{
            return "Outdoor"
        }
        return ""
    }
    func getCommunityRequestValue(uiValue:String) -> String {
        if uiValue == "Show Community"{
            return "community"
        }else if uiValue == "Show only my contacts"{
            return "safecontact"
        }
        return ""
    }
    func getTimeRequestValue(uiValue:String) -> String {
        if uiValue == "Less than 5 minutes ago"{
            return "less_than_5"
        }else if uiValue == "5 to 10 minutes ago"{
            return "between_5_to_10"
        }else if uiValue == "Over 10 minutes ago"{
            return "over_10"
        }
        return ""
    }
    func getStatusRequestValue(uiValue:String) -> String {
        if uiValue == "All"{
            return ""
        }else if uiValue == "Confirmed by Victim"{
            return "victim"
        }else if uiValue == "Confirmed by Safe Contact"{
            return "safecontact"
        }else if uiValue == "Confirmed by Responders"{
            return "responder"
        }
        return ""
    }
    func userAlertSetting(viewController:UIViewController,param :String,value:String,callback:@escaping (_ result:[String:JSON]) -> Void)  {
       SVProgressHUD.show(withStatus: "Wait...")
       let param = [param:value,"user_id":Enough.shared.loginUser.id]
       ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.userAlertSetting_endPoint, param: param, viewcontroller: viewController) { (json) in
           if let dic = json?.dictionary{
               print(dic)
                   callback(dic)
           }
       }
   }
func getUserAlertSetting(viewController:UIViewController,callback:@escaping () -> Void) {
    SVProgressHUD.show(withStatus: "Wait...")
    let param = ["user_id":Enough.shared.loginUser.id]
    ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getUserAlertSetting_endPoint, param: param, viewcontroller: viewController) { (json) in
        if let dic = json?.dictionary{
            if let requests = dic["data"]?.dictionary{
                for request in requests{
                    let value = request.value.string!
                    self.getAlertSettingArr[request.key] = value
                }
            }
        }
        callback()
    }
    }
    func createTrigerAlert(viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,"mode":Enough.shared.appMode!.rawValue,
                     "date":Enough.shared.getDateStr(formate: "yyyy-MM-dd", date: Date()),
                     "time":Enough.shared.getDateStr(formate: "HH:mm:ss", date: Date()),
                     "location":Enough.shared.loginUser.city!,"latitude":"\(Enough.shared.currentLoc?.coordinate.latitude ?? 0.0)",
            "longitude":"\(Enough.shared.currentLoc?.coordinate.longitude ?? 0.0)"]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.createTrigerAlert_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
    func getUserAlertList(viewController:UIViewController,callback:@escaping () -> Void) {
    SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,"distance":getDistanceRequestValue(uiValue: alertDistance),
                     "date":Enough.shared.getDateStr(formate: "yyyy-MM-dd", date: Date()),
                     "time":Enough.shared.getDateStr(formate: "HH:mm:ss", date: Date()),
                     "community":getCommunityRequestValue(uiValue: alert_Community),
                     "user_lat":"\(Enough.shared.currentLoc?.coordinate.latitude ?? 0.0)",
            "user_long":"\(Enough.shared.currentLoc?.coordinate.longitude ?? 0.0)",
            "emergency_type":getEmergencyRequestValue(uiValue: alert_emergency_type),"time_diffrance":getTimeRequestValue(uiValue: alert_time_diffrance),
            "status":getStatusRequestValue(uiValue: alert_status)]
    ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getUserAlertList_endPoint, param: param, viewcontroller: viewController) { (json) in
        if let dic = json?.dictionary{
            if let alerts = dic["data"]?.array{
                for alert in alerts{
                    self.alert_list.append(Alert(dic: alert.dictionary!))
                }
            }
        }
        callback()
    }
    }
}
