//
//  AddContactViewModel.swift
//  enough!
//
//  Created by Pankaj Sonava on 16/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class AddContactViewModel: NSObject {
    var allRequest = [Request]()
    var safeContacts = [Request]()
    var fearContacts = [Request]()
    func searchUser(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getUser_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
    func addFearContact(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.addFearContact_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
    func addSafeContact(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.addContact_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
    func getFearContacts(viewController:UIViewController,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getFearContact_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let requests = dic["data"]?.array{
                    for request in requests{
                        self.fearContacts.append(Request.init(dic: request.dictionary!))
                    }
                }
            }
            callback()
        }
    }
    func getSafeContacts(viewController:UIViewController,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,
                     "issafe":"1"]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getContact_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let requests = dic["data"]?.array{
                    for request in requests{
                        self.safeContacts.append(Request.init(dic: request.dictionary!))
                    }
                }
            }
            callback()
        }
    }
    func getRequestsList(viewController:UIViewController,callback:@escaping () -> Void) {
            SVProgressHUD.show(withStatus: "Wait...")
            let param = ["user_id":Enough.shared.loginUser.id]
            ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getRequestsList_endPoint, param: param, viewcontroller: viewController) { (json) in
                if let dic = json?.dictionary{
                    if let requests = dic["data"]?.array{
                        for request in requests{
                            self.allRequest.append(Request.init(dic: request.dictionary!))
                        }
                    }
                }
                callback()
            }
        }

    func aaceptReq(req_id:String,viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,
                     "id":req_id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.acceptReq_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
    func cancelReq(req_id:String,viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,
                     "id":req_id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.rejectReq_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
    func deleteContact(contact_id:String,viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,
                     "id":contact_id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.deleteContact_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
}
