//
//  CodeViewModel.swift
//  enough!
//
//  Created by mac on 23/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//


import UIKit
import SVProgressHUD
import SwiftyJSON

class CodeViewModel: NSObject {

    var getCodeArray:[String:String] = [:]

    func updateCode(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
           SVProgressHUD.show(withStatus: "Wait...")
           ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.codeUpdate_endPoint, param: parameter, viewcontroller: viewController) { (json) in
               if let dic = json?.dictionary{
                   print(dic)
                       callback(dic)
               }
           }
       }

    func getCode(viewController:UIViewController,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getCode_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let requests = dic["data"]?.dictionary{
                    for request in requests{
                        let value = request.value.string!
                        self.getCodeArray[request.key] = value
                    }
                }
            }
            callback()
        }
    }

    func cancelCode(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.cancelCode_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
    
    func emailVerification(viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["email":Enough.shared.loginUser.email]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.emailVerification_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
}
