//
//  AddHouseholdViewModel.swift
//  enough!
//
//  Created by mac on 20/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class AddHouseholdViewModel: NSObject {
    var allHouseholdList = [Request]()
    
    func addHousehold(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
           SVProgressHUD.show(withStatus: "Wait...")
           ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.addHousehold_endPoint, param: parameter, viewcontroller: viewController) { (json) in
               if let dic = json?.dictionary{
                   print(dic)
                       callback(dic)
               }
           }
       }
    
    func getHouseholdList(viewController:UIViewController,callback:@escaping () -> Void) {
           SVProgressHUD.show(withStatus: "Wait...")
           let param = ["user_id":Enough.shared.loginUser.id]
           ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getHouseholdList_endPoint, param: param, viewcontroller: viewController) { (json) in
               if let dic = json?.dictionary{
                   if let requests = dic["data"]?.array{
                       for request in requests{
                           self.allHouseholdList.append(Request.init(dic: request.dictionary!))
                       }
                   }
               }
               callback()
           }
       }
    
    func editHousehold(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.updateHousehold_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
    
    func deleteHousehold(req_id:String,viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,
                     "household_contact_id":req_id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.deleteHousehold_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
}
