//
//  MissingPersonViewModel.swift
//  enough!
//
//  Created by Pankaj Sonava on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class MissingPersonViewModel: NSObject {
    
    var missingPerson = [Person]()
    
    func addOrUpdateMissingPerson(viewController:UIViewController,parameter:[String:String],picData:Data?,endPoint:String,callback:@escaping (_ result:[String:JSON]) -> Void)  {//
        SVProgressHUD.show(withStatus: "Wait...")
        if let data = picData{
            ApiManager.shared.hitRequestWithData(method: HTTP_Method.post, end_point: endPoint, param: parameter, dataKye: "file", data: data, viewcontroller: viewController) { (json) in
                if let dic = json?.dictionary{
                    print(dic)
                        callback(dic)
                }
            }
        }else{
            ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point:endPoint, param: parameter, viewcontroller: viewController) { (json) in
                if let dic = json?.dictionary{
                    print(dic)
                        callback(dic)
                }
            }
        }
    }
    func getPersonRecord(viewController:UIViewController,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getMissingPerson_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let requests = dic["data"]?.array{
                    for request in requests{
                        self.missingPerson.append(Person.init(dic: request.dictionary!))
                    }
                }
            }
            callback()
        }
    }
    func deleteRecord(template_id:String,viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id,
                     "template_id":template_id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.deletePerson_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
}
