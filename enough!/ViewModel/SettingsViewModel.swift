//
//  SettingsViewModel.swift
//  enough!
//
//  Created by Pankaj Sonava on 23/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class SettingsViewModel: NSObject {
    func uploadProPic(picData:Data,viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Updating...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequestWithData(method: HTTP_Method.post, end_point: ApiManager.updateProfilePic_endPoint, param: param, dataKye: "file", data: picData, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
    func getAppMode(viewController:UIViewController,callback:@escaping () -> Void)  {
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getUserMode_EndPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let mode = dic["data"]?.dictionary{
                    if mode["mode"]?.string == "live"{
                        Enough.shared.appMode = .live
                    }else{
                        Enough.shared.appMode = .test
                    }
                }
            }
            callback()
        }
    }
    func updateUserMode(userMode:String,viewController:UIViewController,callback:@escaping () -> Void)  {
        SVProgressHUD.show(withStatus: "Updating...")
        let param = ["user_id":Enough.shared.loginUser.id,"user_mode":userMode]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.updateUserMode_EndPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let status = dic["status"]?.int{
                    if status == 1{
                        if userMode == "live"{
                            Enough.shared.appMode = .live
                        }else{
                             Enough.shared.appMode = .test
                        }
                    }else{
                        Enough.shared.showOnlyAlert(message: dic["message"]?.string ?? "", viewController: viewController)
                    }
                }
            }
            callback()
        }
    }
}
