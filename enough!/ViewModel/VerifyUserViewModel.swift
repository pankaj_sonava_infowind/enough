//
//  VerifyUserViewModel.swift
//  enough!
//
//  Created by mac on 24/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class VerifyUserViewModel: NSObject {

func emailVerification(viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["email":Enough.shared.loginUser.email]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.emailVerification_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
    func photoVerification(parameter:[String:String],key:String,pathImage:Data,viewController:UIViewController,callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Uploading...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequestWithData(method: HTTP_Method.post, end_point: ApiManager.photoVerification_endPoint, param: param, dataKye: key, data: pathImage, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                callback(dic)
            }
        }
    }
    func getHomeSteps(viewController:UIViewController,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":Enough.shared.loginUser.id]
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.getHomeSteps_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let dic = dic["data"]?.dictionary{
                    print(dic)
                    Enough.shared.assignHomeSteps(dic: dic)
                }
            }
            callback()
        }
    }
    func updateHomeSteps(viewController:UIViewController,param:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: ApiManager.updateHomeStep_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                print(dic)
                    callback(dic)
            }
        }
    }
}
