//
//  SideMenuViewController.swift
//  enough!
//
//  Created by mac on 10/08/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tblSideMenu: UITableView!
    @IBOutlet weak var btnMenu: UIButton!
    
    var arrSideMenu = ["Home","Alerts","Map","Community","Account","Settings","Get involved","About"]
    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil{
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControl.Event.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer());
        }
    }
    @IBAction func actionSideMenu(_ sender: Any) {
        if revealViewController() != nil{
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControl.Event.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer());
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSideMenu.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.lbl.text = arrSideMenu[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            let settingVC = Main_Storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            navigationController?.pushViewController(settingVC, animated: true)
        }
    }
}

class SideMenuCell:UITableViewCell{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl: UILabel!
}
