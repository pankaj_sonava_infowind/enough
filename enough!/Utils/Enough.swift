//
//  Enough.swift
//  enough!
//
//  Created by Pankaj Sonava on 06/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import CoreLocation

class Enough {
    static let shared = Enough()
    private init(){}
    var loginUser:User!
    var enough_status = Enough_Ststus()
    var appMode:App_Mode?
    var triggerMode:Trigger_Mode = .off
    var currentLoc: CLLocation?
    
    func checkAcc_status(field:String) -> Bool {
        if field == EnoughStatus_Field.create_acc{
            return Enough.shared.enough_status.acc_created
        }else if field == EnoughStatus_Field.complete_pro{
            return Enough.shared.enough_status.pro_completed
        }else if field == EnoughStatus_Field.verified_acc{
            return Enough.shared.enough_status.acc_veryfied
        }else if field == EnoughStatus_Field.record_custom_audio{
            return Enough.shared.enough_status.rec_cust_aud_tri
        }else if field == EnoughStatus_Field.create_code{
            return Enough.shared.enough_status.code_created
        }else if field == EnoughStatus_Field.addSafe_contact{
            return Enough.shared.enough_status.add_safe_contact
        }else if field == EnoughStatus_Field.createRec_missPerson{
            return Enough.shared.enough_status.miss_person_record
        }else if field == EnoughStatus_Field.test_alert{
            if Enough.shared.enough_status.acc_created && Enough.shared.enough_status.pro_completed && Enough.shared.enough_status.acc_veryfied && Enough.shared.enough_status.code_created{
                return true
            }
        }
        return false
    }
    func getRemainingCondition(field:String) -> String {
        if !Enough.shared.enough_status.acc_created {
            return EnoughStatus_Field.create_acc
        }else{
            if !Enough.shared.enough_status.pro_completed {
                return EnoughStatus_Field.complete_pro
            }else{
                if !Enough.shared.enough_status.acc_veryfied {
                    return EnoughStatus_Field.verified_acc
                }else{
                    return EnoughStatus_Field.create_code
                }
            }
        }
    }
    func getMultipleFontText(key:String,value:String) -> NSAttributedString {
        let myString = "\(key) \(value)"
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font:UIFont(name: "NunitoSans-Bold", size: 16)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "lightPurple")!, range: NSRange(location:0,length:myString.count))
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "NunitoSans-Regular", size: 16)!, range: NSRange(location:key.count,length:value.count))
        return myMutableString
    }
    // Check Iphone X or not
    static var hasSafeArea: Bool {
        guard #available(iOS 11.0, *), let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24 else {
            return false
        }
        return true
    }
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    // Get random string
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    // MAKE GRADIENT LAYER
    func createGradient(colors:[CGColor],frame:CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        return gradient
    }
    
    // Register table view cell
    func registerTableViewCell(identifier:String,tableView:UITableView)  {
        let cellNib = UINib(nibName: identifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: identifier)
    }
    // Get Current Date String
    func getDateStr(formate:String,date:Date) -> String {
        Date_Formatter.dateFormat = formate
        return Date_Formatter.string(from: date)
    }
    // Email validate
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // Show alert
    func showOnlyAlert(title:String = Alert_Msg.Opps,message:String,viewController:UIViewController)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Alert_Msg.Ok, style: .default, handler: nil)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }

    func showAlertOneAction(title:String = Alert_Msg.Opps,okayTitle:String=Alert_Msg.Ok,message:String,viewController:UIViewController,callback:@escaping () -> Void)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: okayTitle, style: .default){ (_) in
            callback()
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    func showAlertTwoAction(title:String = Alert_Msg.Opps,okayTitle:String=Alert_Msg.Ok,cancelTitle:String=Alert_Msg.Cancel,message:String,viewController:UIViewController,callback:@escaping (_ isOkay:Bool) -> Void)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: okayTitle, style: .default){ (_) in
            callback(true)
        }
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in
            callback(false)
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    func assignHomeSteps(dic:[String:JSON]) {
        var dict = [String:String]()
        if dic["complete_your_profile"]?.string == "1"{
            Enough.shared.enough_status.pro_completed = true
            dict["complete_your_profile"] = dic["complete_your_profile"]?.string
        }
        if dic["verify_your_account"]?.string == "1"{
            Enough.shared.enough_status.acc_veryfied = true
            dict["verify_your_account"] = dic["verify_your_account"]?.string
        }
        if dic["create_code"]?.string == "1"{
            Enough.shared.enough_status.code_created = true
            dict["create_code"] = dic["create_code"]?.string
        }
        if dic["record_audio_trigger"]?.string == "1"{
            Enough.shared.enough_status.rec_cust_aud_tri = true
            dict["record_audio_trigger"] = dic["record_audio_trigger"]?.string
        }
        if dic["add_safe_contacts"]?.string == "1"{
            Enough.shared.enough_status.add_safe_contact = true
            dict["add_safe_contacts"] = dic["add_safe_contacts"]?.string
        }
        if dic["missing_person_template"]?.string == "1"{
            Enough.shared.enough_status.miss_person_record = true
            dict["missing_person_template"] = dic["missing_person_template"]?.string
        }
        if dic["trigger_test_alert"]?.string == "1"{
            Enough.shared.enough_status.triger_test_alert = true
            dict["trigger_test_alert"] = dic["trigger_test_alert"]?.string
        }
        UserDefaults.standard.set(dict, forKey: "HomeSteps")
    }
    func assignHomeStepsFromDic(dic:[String:String])  {
        if dic["complete_your_profile"] == "1"{
            Enough.shared.enough_status.pro_completed = true
        }
        if dic["verify_your_account"] == "1"{
            Enough.shared.enough_status.acc_veryfied = true
        }
        if dic["create_code"] == "1"{
            Enough.shared.enough_status.code_created = true
        }
        if dic["record_audio_trigger"] == "1"{
            Enough.shared.enough_status.rec_cust_aud_tri = true
        }
        if dic["add_safe_contacts"] == "1"{
            Enough.shared.enough_status.add_safe_contact = true
        }
        if dic["missing_person_template"] == "1"{
            Enough.shared.enough_status.miss_person_record = true
        }
        if dic["trigger_test_alert"] == "1"{
            Enough.shared.enough_status.triger_test_alert = true
        }
    }
    func getCoreDic(jsonDic:[String:JSON]) -> [String:String?] {
        
        let dic = ["city":jsonDic["city"]?.string,
                   "firstname":jsonDic["firstname"]?.string,
                   "username":jsonDic["username"]?.string,
                   "gender":jsonDic["gender"]?.string,
                   "country":jsonDic["country"]?.string,
                   "name":jsonDic["name"]?.string,
                   "dob":jsonDic["dob"]?.string,
                   "mobile":jsonDic["mobile"]?.string,
                   "gender_optional":jsonDic["gender_optional"]?.string,
                   "alias":jsonDic["alias"]?.string,
                   "lastname":jsonDic["lastname"]?.string,
                   "headline":jsonDic["headline"]?.string,
                   "gender_pronoun":jsonDic["gender_pronoun"]?.string,
                   "id":jsonDic["id"]?.string,
                   "email":jsonDic["email"]?.string,
                   "id_verify_status":jsonDic["id_verify_status"]?.string,
                   "profile_image":jsonDic["profile_image"]?.string,
                   "street":jsonDic["street"]?.string,
                   "apartment":jsonDic["apartment"]?.string,
                   "state":jsonDic["state"]?.string,
                   "zipcode":jsonDic["zipcode"]?.string,
                   "email_verify_status":jsonDic["email_verify_status"]?.string,
                   "mobile_verify_status":jsonDic["mobile_verify_status"]?.string,
                   "mode":jsonDic["mode"]?.string,
                   "latitude":jsonDic["latitude"]?.string,
                   "longitude":jsonDic["longitude"]?.string
        ]
        return dic
    }
}
