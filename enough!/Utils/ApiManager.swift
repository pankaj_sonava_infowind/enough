//
//  ApiManager.swift
//  enough!
//
//  Created by Pankaj Sonava on 14/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import SVProgressHUD
import SwiftyJSON

class ApiManager {
    private init() {}
    static let shared = ApiManager()
    
    static let base_url = "https://thepeacenetwork.org/"
    
    // Used API
    static let register_endPoint = "api/user.php?act=user-signup"
    static let login_endPoint = "api/user.php?act=user-login"
    static let countryList_endPoint = "api/user.php?act=get-country-list"
    static let updateUser_endPoint = "api/user.php?act=update-profile"
    static let getUser_endPoint = "api/user.php?act=search-contacts"
    static let addContact_endPoint = "api/user.php?act=add-contacts"
    static let addFearContact_endPoint = "api/user.php?act=add-fear-contacts"
    static let getContact_endPoint = "api/user.php?act=get-contacts-list"
    static let getMissingPerson_endPoint = "api/user.php?act=get-missing-persons-template"
    static let getFearContact_endPoint = "api/user.php?act=get-fear-contacts-list"
    static let getRequestsList_endPoint  = "api/user.php?act=get-request-list"
    static let acceptReq_endPoint = "api/user.php?act=request-accept"
    static let rejectReq_endPoint = "api/user.php?act=request-reject"
    static let deleteContact_endPoint = "api/user.php?act=delete-contacts"
    static let deletePerson_endPoint = "api/user.php?act=delete-missing-persons-template"
    static let addHousehold_endPoint = "api/user.php?act=add-household-members"
    static let getHouseholdList_endPoint = "api/user.php?act=get-household-members-list"
    static let deleteHousehold_endPoint = "api/user.php?act=delete-household-members"
    static let updateHousehold_endPoint = "api/user.php?act=update-household-members"
    static let addMissingPerson_endPoint = "api/user.php?act=add-missing-persons-template"
    static let updateMissingPerson_endPoint = "api/user.php?act=update-missing-persons-template"
    static let updateProfilePic_endPoint = "/api/user.php?act=update-profile-image"
    static let mobileVerify_endPoint = "/api/user.php?act=mobile-verify"
    static let getVerifiedStatus_endPoint = "/api/user.php?act=get-verify-status"
    static let codeUpdate_endPoint = "api/user.php?act=update-user-codes"
    static let getCode_endPoint = "api/user.php?act=get-user-codes"
    static let cancelCode_endPoint = "api/user.php?act=cancel-user-codes"
    static let emailVerification_endPoint = "api/user.php?act=send-email-verification"
    static let photoVerification_endPoint = "api/user.php?act=photo-verification"
    static let getHomeSteps_endPoint = "/api/user.php?act=get-home-steps"
    static let updateHomeStep_endPoint = "api/user.php?act=update-home-steps"
    static let getUserMode_EndPoint = "api/user.php?act=get-user-mode"
    static let updateUserMode_EndPoint = "api/user.php?act=update-user-mode"
    static let userAlertSetting_endPoint = "api/user.php?act=user-alert-setting"
    static let getUserAlertSetting_endPoint = "api/user.php?act=get-user-alert-setting"
    static let getUserAlertList_endPoint = "api/user.php?act=get-alert-list"
    static let createTrigerAlert_endPoint = "api/user.php?act=add-trigger-alert"
  
    // Unused API
    static let forgotPassword_endPoint = "api/user.php?act=forgot-password"
    static let otpVerify_endPoint = "api/user.php?act=otp-verify"
    static let updatePassword_endPoint = "api/user.php?act=update-password"
    
    
    func hitRequest(method:String,end_point:String,param:[String:String]?,viewcontroller:UIViewController,callback:@escaping (_ json:JSON?) -> Void) {
            
            let urlStr = ApiManager.base_url + end_point
            
            var request = URLRequest(url: URL(string: urlStr)!)
            request.httpMethod = method
            if let pr = param{
                let keys = Array(pr.keys)
                var postString = ""
                for key in keys {
                    postString = postString + "\(key)=\(pr[key] ?? "")&"
                }
                let postData = postString.data(using: .utf8)
                request.httpBody = postData
            }
            //SVProgressHUD.show(withStatus: "Wait...")
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                }
                if error == nil{
                    if let json = try? JSON(data: data!){
                        callback(json)
                    }else{
                        DispatchQueue.main.async {
                            Enough.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: Alert_Msg.Wrong, viewController: viewcontroller) { (isTrue) in
                                if isTrue{
                                    self.hitRequest(method: method, end_point: end_point, param: param, viewcontroller: viewcontroller, callback: callback)
                                }
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        Enough.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: error!.localizedDescription, viewController: viewcontroller) { (isTrue) in
                            if isTrue{
                                self.hitRequest(method: method, end_point: end_point, param: param, viewcontroller: viewcontroller, callback: callback)
                            }
                        }
                    }
                    callback(nil)
                }
            }
            task.resume()
    }
    func hitRequestWithData(method:String,end_point:String,param:[String:String],dataKye:String,data:Data,viewcontroller:UIViewController,callback:@escaping (_ json:JSON?) -> Void) {
            let imgData = data
            let urlStr = ApiManager  .base_url + end_point
            let url = NSURL(string: urlStr)! as URL
            let request: NSMutableURLRequest = NSMutableURLRequest(url:url )
            let boundary = generateBoundaryString()
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = method
        if dataKye == "voice-recorder-file"{
            let postData = createBodyWithParametersForAudio(parameters: param, filePathKey: dataKye, videoData: data, boundary: boundary)
            request.httpBody = postData
        }else{
            let postData = createBodyWithParameters(param, filePathKey: dataKye, imageDataKey: data, boundary: boundary)
            request.httpBody = postData
        }
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                }
                if error == nil{
                    if let json = try? JSON(data: data!){
                        callback(json)
                    }else{
                        DispatchQueue.main.async {
                            Enough.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: Alert_Msg.Wrong, viewController: viewcontroller) { (isTrue) in
                                if isTrue{
                                    SVProgressHUD.show(withStatus: "Wait...")
                                    self.hitRequestWithData(method: method, end_point: end_point, param: param, dataKye: dataKye, data:  imgData, viewcontroller: viewcontroller, callback: callback)
                                }
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        Enough.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: error!.localizedDescription, viewController: viewcontroller) { (isTrue) in
                            if isTrue{
                                self.hitRequestWithData(method: method, end_point: end_point, param: param, dataKye: dataKye, data:  imgData, viewcontroller: viewcontroller, callback: callback)
                            }
                        }
                    }
                    callback(nil)
                }
            }
            task.resume()
    }
    func generateBoundaryString() -> String{
        return "Boundary-\(UUID().uuidString)"
    }
    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        if imageDataKey.count != 0 {
            let filename = "picture.png"
            let mimetype = "image/jpg"
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imageDataKey)
            body.appendString(string: "\r\n")
        }else{
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"\r\n\r\n")
            let value : String = ""
            body.appendString(string: "\(value)\r\n")
        }
        body.appendString(string: "--\(boundary)--\r\n")
        return body as Data
    }
    func createBodyWithParametersForAudio(parameters: [String: String]?, filePathKey: String?, videoData: Data, boundary: String) -> Data {
        let body = NSMutableData();
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string:"--\(boundary)\r\n")
                body.appendString(string:"Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string:"\(value)\r\n")
            }
        }
        let mimetype = "audio/wav"
        body.appendString(string:"--\(boundary)\r\n")
        body.appendString(string:"Content-Disposition: form-data; name=\"voice-recorder-file\"; filename=\"zhcmessagestemp.wav\"\r\n")
        body.appendString(string:"Content-Type: \(mimetype)\r\n\r\n")
        body.append(videoData)
        body.appendString(string:"\r\n")
        body.appendString(string:"--\(boundary)--\r\n")
        return body as Data
    }
}
