//
//  Extensions.swift
//  enough!
//
//  Created by Pankaj Sonava on 03/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func placeholderColor(text:String,color:UIColor)  {
        self.attributedPlaceholder = NSAttributedString(string: text,
        attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    enum PaddingSpace {
        case left(CGFloat)
        case right(CGFloat)
        case equalSpacing(CGFloat)
    }
    func addPadding(padding: PaddingSpace) {

        self.leftViewMode = .always
        self.layer.masksToBounds = true

        switch padding {

        case .left(let spacing):
            let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = leftPaddingView
            self.leftViewMode = .always

        case .right(let spacing):
            let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = rightPaddingView
            self.rightViewMode = .always

        case .equalSpacing(let spacing):
            let equalPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = equalPaddingView
            self.leftViewMode = .always
            // right
            self.rightView = equalPaddingView
            self.rightViewMode = .always
        }
    }

}
extension UIColor{
    class var darkPink:UIColor {return UIColor(red: 240.0/255.0, green: 84.0/255.0, blue: 113.0/255.0, alpha: 1.0)}
    class var lightPink:UIColor {return UIColor(red: 240.0/255.0, green: 120.0/255.0, blue: 91.0/255.0, alpha: 1.0)}
    class var btnSelected:UIColor {return UIColor(red: 19.0/255.0, green: 201.0/255.0, blue: 118.0/255.0, alpha: 1.0)}
    class var btnDeselected:UIColor {return UIColor(red: 173.0/255.0, green: 181.0/255.0, blue: 189.0/255.0, alpha: 1.0)}
    class var lightMilky:UIColor {return UIColor(red: 2.0, green: 197.0, blue: 172.0, alpha: 1.0)}
}


extension UIView {
  
  @IBInspectable
  var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable
  var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable
  var borderColor: UIColor? {
    get {
      if let color = layer.borderColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.borderColor = color.cgColor
      } else {
        layer.borderColor = nil
      }
    }
  }
  
  @IBInspectable
  var shadowRadius: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.shadowRadius = newValue
    }
  }
  
  @IBInspectable
  var shadowOpacity: Float {
    get {
      return layer.shadowOpacity
    }
    set {
      layer.shadowOpacity = newValue
    }
  }
  
  @IBInspectable
  var shadowOffset: CGSize {
    get {
      return layer.shadowOffset
    }
    set {
      layer.shadowOffset = newValue
    }
  }
  
  @IBInspectable
  var shadowColor: UIColor? {
    get {
      if let color = layer.shadowColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.shadowColor = color.cgColor
      } else {
        layer.shadowColor = nil
      }
    }
  }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    func addBottomBorder(width:CGFloat = 0.5,color:UIColor = UIColor.lightGray){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        bottomLine.backgroundColor = color.cgColor
        layer.addSublayer(bottomLine)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
    func strikeThrough() {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self.text!)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }
    func setMultipleFont(key:String,value:String)  {
        let myString = "\(key) \(value)"
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font:UIFont(name: "NunitoSans-Bold", size: 16)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "lightPurple")!, range: NSRange(location:0,length:myString.count))
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "NunitoSans-Regular", size: 16)!, range: NSRange(location:key.count,length:value.count))
        attributedText = myMutableString
    }
}
extension UIImageView {
  func enableZoom() {
    let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(startZooming(_:)))
    isUserInteractionEnabled = true
    addGestureRecognizer(pinchGesture)
  }

  @objc
  private func startZooming(_ sender: UIPinchGestureRecognizer) {
    let scaleResult = sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)
    guard let scale = scaleResult, scale.a > 1, scale.d > 1 else { return }
    sender.view?.transform = scale
    sender.scale = 1
  }
}
extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: true)
        append(data!)
    }
}
extension UIViewController:HeaderActions{
    func onOffToggle(sender: UIButton) {
        let headerPopUp = HeaderPopUp(frame: view.bounds)
        headerPopUp.delegate = self as? HeaderProtocol
        if !sender.isSelected{
            view.addSubview(headerPopUp)
        }else{
            Enough.shared.triggerMode = .off
            sender.isSelected = false
        }
    }
    func testLiveToggle(sender: UIButton) {
        let headerPopUp = HeaderPopUp(frame: view.bounds)
        headerPopUp.isLiveBtnPress = true
        headerPopUp.viewSetup()
        headerPopUp.delegate = self as? HeaderProtocol
        if !sender.isSelected{
            view.addSubview(headerPopUp)
        }else{
            SettingsViewModel().updateUserMode(userMode: "test", viewController: self) {
                DispatchQueue.main.async {
                    Enough.shared.appMode = .test
                    sender.isSelected = false
                }
            }
        }
    }
    func menuButtonAction(sender: UIButton) {
        if revealViewController() != nil{
            sender.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControl.Event.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer());
        }
        
    }
}
