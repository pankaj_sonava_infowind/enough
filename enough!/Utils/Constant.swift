//
//  Constant.swift
//  enough!
//
//  Created by Pankaj Sonava on 06/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit

let Date_Formatter = DateFormatter()
let Main_Storyboard = UIStoryboard(name: "Main", bundle: nil)
let KBundleIdentifier = "Sonava.enough-"
let relationArray = [Relationships.father,Relationships.mother,Relationships.huby,Relationships.wife,Relationships.bro,Relationships.sis,Relationships.bro_inLaw,Relationships.sis_inLaw,Relationships.uncle,Relationships.aunt,Relationships.cousin,Relationships.friend,Relationships.other]

struct EnoughAccount {
    static let already_acc = "Already has an account"
    static let hasNot_acc = "Doesn't have an account"
}
struct Relationships {
    static let father = "Father"
    static let mother = "Mother"
    static let huby = "Husband/Boyfriend"
    static let wife = "Wife/Girlfriend"
    static let bro = "Brother"
    static let sis = "Sister"
    static let bro_inLaw = "Brother in law"
    static let sis_inLaw = "Sister in law"
    static let uncle = "Uncle"
    static let aunt = "Aunt"
    static let cousin = "Cousin"
    static let friend = "Friend"
    static let other = "Other:"
}
struct Alert_Distances {
    static let meter_100 = "100 meters from my location"
    static let meter_100to500 = "100m to 500m from my location"
    static let meter_500to1km = "500m to 1km from my location"
    static let km_1 = "Over 1km from my location"
}
struct Alert_Emergency_Type {
    static let all = "All"
    static let domestic = "Domestic"
    static let public_space_ind = "Public Space Indoor"
    static let public_space_out = "Public Space Outdoor"
}
struct Alert_Status {
    static let all = "All"
    static let by_victim = "Confirmed by Victim"
    static let by_safeContact = "Confirmed by Safe Contact"
    static let by_responder = "Confirmed by Responders"
}
struct Alert_Time {
    static let less_5min = "Less than 5 minutes ago"
    static let betwen_5to10Min = "5 to 10 minutes ago"
    static let over_10Min = "Over 10 minutes ago"
}
struct Alert_Show {
    static let show_community = "Show Community"
    static let my_contact = "Show only my contacts"
}
enum App_Mode : String {
    case live
    case test
}
enum Trigger_Mode : String {
    case on
    case off
}
struct EnoughStatus_Field {
    static let create_acc = "Create your account"
    static let complete_pro = "Complete your profile"
    static let verified_acc = "Verify your account"
    static let create_code = "Create youe code"
    static let record_custom_audio = "Record your custom audio triggers"
    static let addSafe_contact = "Add your safe contacts"
    static let createRec_missPerson = "Create your missing persons records"
    static let test_alert = "Trigger a test alerts"
}
struct Enough_Ststus {
    var acc_created = false
    var pro_completed = false
    var acc_veryfied = false
    var code_created = false
    var rec_cust_aud_tri = false
    var add_safe_contact = false
    var miss_person_record = false
    var triger_test_alert = false
}

enum DateFormate {
    static let full_date = "dd/MMMMM/yyyy"
}
struct Alert_Msg {
    static let Ok = "Okay"
    static let TryAgain = "Try Again"
    static let Wrong = "Something went wrong"
    static let Pic_Updatted = "Profile updated successfuly"
    static let Cancel = "Cancel"
    static let Opps = "Alert!"
    static let Leave = "Leave"
    static let Continue = "Continue"
    static let Done = "Done!"
    static let LogOut = "Logout"
    static let LogOut_Confirm = "Are you sure you want to logout?"
    static let Registration_Success = "Registration succesfully"
    static let Contact_Addedd = "Contact has been addedd."
    static let Req_Accepted = "Request has been accepted."
    static let Req_Deleted = "Request has been deleted."
    
    static let ProfileUpdate_Success = "Profile Update succesfully"
    static let Confirm_Delete_Contact = "Are you sure you want to\n delete this contact?"
    static let addHousehold_Success = "Household added succesfully"
    static let editHousehold_Success = "Household edited succesfully"
    static let Household_Deleted = "Household has been deleted."
    static let Confirm_Delete_Record = "Are you sure you want to\ndelete this missing person\nrecord?"
    static let Confirm_AddHouseHold_Record = "Are you sure you want to add\nthis person to your household?"
    static let CodeUpdate_Success = "Code Updated succesfully"
    static let CodeDelete_Success = "Code deleled succesfully"
    static let photoUploaded_Success = "uploaded succesfully "
    
}
struct HTTP_Method {
    static let post = "POST"
    static let get = "GET"
}
struct Error_Alert {
    static let Missing_Username = "Please enter user name."
    static let Missing_Email = "Please enter email address."
    static let Missing_Password = "Please enter password."
    static let Missing_Mobile = "Please enter mobile number."
    static let Missing_Gender = "Please select gender."
    static let Missing_Eye = "Please select eye."
    static let Missing_HairColor = "Please select hair color."
    static let Missing_Glass = "Please select glasses."
    static let Missing_HairLeanth = "Please select hair length."
    static let Missing_Ethnicity = "Please select Ethnicity."
    static let Missing_HairStyle = "Please select hair style."
    static let Missing_Sign = "Please enter unique signs."
    static let Missing_HealthCondition = "Please enter health condition."
    static let Missing_Pic = "Please select picture."
    
    static let Missing_Build = "Please select build."
    static let Missing_DOB = "Please select date of birth."
    static let Missing_Country = "Please select your country."
    static let Invalid_Email = "Please enter a valid email."
    static let Missing_Relation = "Please enter relation name."
    static let Missing_Alias = "Please enter alias."
    static let Miss_Relation = "Please select relation."
    static let Contact_NotAddedd = "Contact not addedd."
    static let Miss_Height = "Please select height."
    static let Miss_Weight = "Please select weight."
    static let Missing_Name = "Please enter name."
    static let Missing_Lastname = "Please enter lastname."
    static let Missing_Headline = "Please enter headline."
    static let Missing_PhonNumber = "Please enter phonNumber."
    static let Missing_Address = "Please select your country."
    static let Missing_Pronoun = "Please enter pronoun."
    static let Missing_Code = "Please enter code."
}
