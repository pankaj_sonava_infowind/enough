//
//  RequestModel.swift
//  enough!
//
//  Created by Pankaj Sonava on 17/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Request {
    let asset_id:String
    let ordering:String
    let state:String
    let checked_out:String
    let checked_out_time:String
    let created_by:String
    let modified_by:String
    let user:String
    let contactid:String
    let firstname:String?
    let lastname:String?
    let id:String
    let contact_email:String
    let contactphone:String
    let contact_type:String
    let relationship:String
    let accepted:String
    let is_invite:String
    let sender_name:String
    let reciver_name:String
    
    
    init(dic:[String:JSON]) {
        self.contact_email = dic["contact_email"]?.string ?? ""
        self.asset_id = dic["asset_id"]?.string ?? ""
        self.ordering = dic["ordering"]?.string ?? ""
        self.lastname = dic["lastname"]?.string ?? ""
        self.firstname = dic["firstname"]?.string ?? ""
        self.id = dic["id"]?.string ?? ""
        self.contactphone = dic["contactphone"]?.string ?? ""
        self.state = dic["state"]?.string ?? ""
        self.checked_out = dic["checked_out"]?.string ?? ""
        self.checked_out_time = dic["checked_out_time"]?.string ?? ""
        self.created_by = dic["created_by"]?.string ?? ""
        self.modified_by = dic["modified_by"]?.string ?? ""
        self.user = dic["user"]?.string ?? ""
        self.contactid = dic["contactid"]?.string ?? ""
        self.contact_type = dic["contact_type"]?.string ?? ""
        self.relationship = dic["relationship"]?.string ?? ""
        self.accepted = dic["accepted"]?.string ?? ""
        self.is_invite = dic["is_invite"]?.string ?? ""
        self.sender_name = dic["sender_name"]?.string ?? ""
        self.reciver_name = dic["reciver_name"]?.string ?? ""
    }
}
