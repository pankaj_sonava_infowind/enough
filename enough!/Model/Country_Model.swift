//
//  Country_Model.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 28/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Country {
    let id:String
    let name:String?
    let sortname:String
    let phonecode:String
    init(dic:[String:JSON]) {
        self.id = (dic["id"]?.string) ?? ""
        self.name = dic["name"]?.string
        self.sortname = (dic["sortname"]?.string) ?? ""
        self.phonecode = "+\((dic["phonecode"]?.string) ?? "")"
    }
}
