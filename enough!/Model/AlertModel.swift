//
//  AlertModel.swift
//  enough!
//
//  Created by Pankaj Sonava on 30/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Alert {
    let id:String
    let asset_id:String
    let ordering:String
    let state:String
    let checked_out:String
    let checked_out_time:String
    let created_by:String
    let modified_by:String
    let userid:String
    let emergency_type:String
    let mode:String
    let date:String
    let time:String
    let location:String?
    let latitude:String?
    let longitude:String?
    let status:String?
    let livefeed:String?
    let groupcall:String?
    let callauthorities:String?
    let cloudrecording:String?
    let audio_cloudrecording:String?
    let groupchat:String?
    let alertcommunity:String?
    let alerttype:String?
    let file_report:String?
    let is_my_alert:String?
    
    let timediffrance:String
    let community:String
    let owned_by:String
    let owner_user_id:String
    let update_authorities:String
    let update_health:String
    let update_next_step:String
    let distance:String
    let victim:String
    let safecontact:String
    let update_safe_contact:String
    let update_status:String
    
    let responder:[Responder]
    let user:Alert_User
    
    init(dic:[String:JSON]) {
        self.asset_id = dic["asset_id"]?.string ?? ""
        self.ordering = dic["ordering"]?.string ?? ""
        self.id = dic["id"]?.string ?? ""
        self.state = dic["state"]?.string ?? ""
        self.checked_out = dic["checked_out"]?.string ?? ""
        self.checked_out_time = dic["checked_out_time"]?.string ?? ""
        self.created_by = dic["created_by"]?.string ?? ""
        self.modified_by = dic["modified_by"]?.string ?? ""
        self.userid = dic["userid"]?.string ?? ""
        self.emergency_type = dic["emergency_type"]?.string ?? ""
        self.mode = dic["mode"]?.string ?? ""
        self.date = dic["date"]?.string ?? ""
        self.time = dic["time"]?.string ?? ""
        self.location = dic["location"]?.string ?? ""
        self.latitude = dic["latitude"]?.string ?? ""
        self.longitude = dic["longitude"]?.string ?? ""
        self.status = dic["status"]?.string ?? ""
        self.livefeed = dic["livefeed"]?.string ?? ""
        self.groupcall = dic["groupcall"]?.string ?? ""
        self.callauthorities = dic["callauthorities"]?.string ?? ""
        self.cloudrecording = dic["cloudrecording"]?.string ?? ""
        self.audio_cloudrecording = dic["audio_cloudrecording"]?.string ?? ""
        self.groupchat = dic["groupchat"]?.string ?? ""
        self.alertcommunity = dic["alertcommunity"]?.string ?? ""
        self.alerttype = dic["alerttype"]?.string ?? ""
        self.file_report = dic["file_report"]?.string ?? ""
        self.is_my_alert = dic["is_my_alert"]?.string ?? ""
        
        self.timediffrance = dic["timediffrance"]?.string ?? ""
        self.community = dic["community"]?.string ?? ""
        self.owned_by = dic["owned_by"]?.string ?? ""
        self.owner_user_id = dic["owner_user_id"]?.string ?? ""
        self.update_authorities = dic["update_authorities"]?.string ?? ""
        self.update_health = dic["update_health"]?.string ?? ""
        self.update_next_step = dic["update_next_step"]?.string ?? ""
        self.distance = dic["distance"]?.string ?? ""
        self.victim = dic["victim"]?.string ?? ""
        self.safecontact = dic["safecontact"]?.string ?? ""
        self.update_safe_contact = dic["update_safe_contact"]?.string ?? ""
        self.update_status = dic["update_status"]?.string ?? ""
        
        var res = [Responder]()
        if let responders = dic["responder"]?.array{
            for responder in responders {
                res.append(Responder.init(dic: responder.dictionary!))
            }
        }
        self.responder = res
        self.user = Alert_User.init(dic: (dic["user"]?.dictionary)!)
    }
}

struct Responder {
    let id:String
    let alertid:String
    let userid:String
    init(dic:[String:JSON]) {
        self.id = dic["id"]?.string ?? ""
        self.alertid = dic["alertid"]?.string ?? ""
        self.userid = dic["userid"]?.string ?? ""
    }
}
struct Alert_User {
    let id:String
    let name:String
    let username:String
    let email:String
    let id_verify_status:String
    let profile_image:String
    let email_verify_status:String
    let mobile_verify_status:String
    init(dic:[String:JSON]) {
        self.id = dic["id"]?.string ?? ""
        self.name = dic["name"]?.string ?? ""
        self.username = dic["username"]?.string ?? ""
        self.email = dic["email"]?.string ?? ""
        self.id_verify_status = dic["id_verify_status"]?.string ?? ""
        self.profile_image = dic["profile_image"]?.string ?? ""
        self.email_verify_status = dic["email_verify_status"]?.string ?? ""
        self.mobile_verify_status = dic["mobile_verify_status"]?.string ?? ""
    }
}
