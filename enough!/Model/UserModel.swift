//
//  UserModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 29/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct User {
    let email:String
    let username:String
    let firstname:String?
    let lastname:String?
    let id:String
    let mobile:String
    let gender:String?
    let dob:String?
    let gender_optional:String?
    let headline:String?
    let city:String?
    let country:String?
    let gender_pronoun:String?
    let name:String?
    let alias:String?
    var profile_image:String?
    var id_verify_status:String?
    let street:String?
    let apartment:String?
    let state:String?
    let zipcode:String?
    var email_verify_status:String?
    var mobile_verify_status:String?
    let mode:String?
    let latitude:String?
    let longitude:String?
    init(dic:[String:String?]) {
        self.email = dic["email"] as? String ?? ""
        self.username = dic["username"] as? String ?? ""
        self.firstname = dic["firstname"] as? String
        self.lastname = dic["lastname"] as? String
        self.id = dic["id"] as? String ?? ""
        self.mobile = dic["mobile"] as? String ?? ""
        self.gender = dic["gender"] as? String
        self.dob = dic["dob"] as? String
        self.gender_optional = dic["gender_optional"] as? String
        self.headline = dic["headline"] as? String
        self.city = dic["city"] as? String
        self.country = dic["country"] as? String
        self.gender_pronoun = dic["gender_pronoun"] as? String
        self.name = dic["name"] as? String
        self.alias = dic["alias"] as? String
        self.profile_image = dic["profile_image"] as? String
        self.id_verify_status = dic["id_verify_status"] as? String
        self.street = dic["street"] as? String
        self.apartment = dic["apartment"] as? String
        self.state = dic["state"] as? String
        self.zipcode = dic["zipcode"] as? String
        self.email_verify_status = dic["email_verify_status"] as? String
        self.mobile_verify_status = dic["mobile_verify_status"] as? String
        self.mode = dic["mode"] as? String
        self.latitude = dic["profile_image"] as? String
        self.longitude = dic["longitude"] as? String
    }
}

struct User_Details {
    let user_id:String
    let member_id:String
    let username:String
    let token:String
    let page_id:String
    
    init(dic:[String:JSON]) {
        self.user_id = dic["user_id"]?.string ?? ""
        self.username = dic["username"]?.string ?? ""
        self.member_id = dic["member_id"]?.string ?? ""
        self.token = dic["token"]?.string ?? ""
        self.page_id = dic["page_id"]?.string ?? ""
    }
}
