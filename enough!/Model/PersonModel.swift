//
//  PersonModel.swift
//  enough!
//
//  Created by Pankaj Sonava on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Person {
    let id:String
    let asset_id:String
    let ordering:String
    let state:String
    let checked_out:String
    let checked_out_time:String
    let created_by:String
    let modified_by:String
    let userid:String
    let contactid:String
    let firstname:String?
    let lastname:String?
    
    let alias:String?
    let height:String?
    let height_type:String?
    let weight:String?
    let weight_type:String?
    let dob:String?
    let gender:String?
    let build:String?
    let eyes:String?
    
    let haircolor:String?
    let glasses:String?
    let hairlength:String?
    let ethnicity:String?
    let hairstyle:String?
    let relation:String?
    let unique_sign:String?
    let special_health_condition:String?
    let image_path:String?
    
    init(dic:[String:JSON]) {
        self.asset_id = dic["asset_id"]?.string ?? ""
        self.ordering = dic["ordering"]?.string ?? ""
        self.lastname = dic["lastname"]?.string ?? ""
        self.firstname = dic["firstname"]?.string ?? ""
        self.id = dic["id"]?.string ?? ""
        self.state = dic["state"]?.string ?? ""
        self.checked_out = dic["checked_out"]?.string ?? ""
        self.checked_out_time = dic["checked_out_time"]?.string ?? ""
        self.created_by = dic["created_by"]?.string ?? ""
        self.modified_by = dic["modified_by"]?.string ?? ""
        self.userid = dic["userid"]?.string ?? ""
        self.contactid = dic["contactid"]?.string ?? ""

        
        self.alias = dic["alias"]?.string ?? ""
        self.height = dic["height"]?.string ?? ""
        self.height_type = dic["height_type"]?.string ?? ""
        self.weight = dic["weight"]?.string ?? ""
        self.weight_type = dic["weight_type"]?.string ?? ""
        self.dob = dic["dob"]?.string ?? ""
        self.gender = dic["gender"]?.string ?? ""
        self.build = dic["build"]?.string ?? ""
        self.eyes = dic["eyes"]?.string ?? ""
        
        self.haircolor = dic["haircolor"]?.string ?? ""
        self.glasses = dic["glasses"]?.string ?? ""
        self.hairlength = dic["hairlength"]?.string ?? ""
        self.ethnicity = dic["ethnicity"]?.string ?? ""
        self.hairstyle = dic["hairstyle"]?.string ?? ""
        self.relation = dic["relation"]?.string ?? ""
        self.unique_sign = dic["unique_sign"]?.string ?? ""
        self.special_health_condition = dic["special_health_condition"]?.string ?? ""
        self.image_path = dic["image_path"]?.string ?? ""
    }
}

