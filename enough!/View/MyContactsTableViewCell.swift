//
//  MyContactsTableViewCell.swift
//  enough!
//
//  Created by mac on 06/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class MyContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_veryfied: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnSendTest: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    var contactDetails:Request?{
        didSet{
            lblName.text = contactDetails?.reciver_name
            let id = contactDetails!.id
            btnCancel.layer.setValue(id, forKey: "contact_id")
        }
    }
}
