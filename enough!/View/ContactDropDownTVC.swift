//
//  ContactDropDownTVC.swift
//  enough!
//
//  Created by Pankaj Sonava on 15/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class ContactDropDownTVC: UITableViewCell {

    @IBOutlet weak var lbl_relation: UILabel!
    @IBOutlet weak var txt_other: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
