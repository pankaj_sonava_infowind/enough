//
//  PersonRecordTVC.swift
//  enough!
//
//  Created by Pankaj Sonava on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SDWebImage

class PersonRecordTVC: UITableViewCell {
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var btn_plush: UIButton!
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var btn_edit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    var personDetails:Person?{
        didSet{
            lbl_name.text = "\(personDetails?.firstname ?? "") \(personDetails?.lastname ?? "")"
            var img_url = ApiManager.base_url + personDetails!.image_path!
            if let range = img_url.range(of: "./../"){
                img_url.removeSubrange(range)
            }
            img_user.sd_setImage(with: URL(string: img_url)) { (image, error, cacheType, url) in
                if let img = image{
                    self.img_user.image = img
                }
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
