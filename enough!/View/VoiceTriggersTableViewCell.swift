//
//  VoiceTriggersTableViewCell.swift
//  enough!
//
//  Created by mac on 09/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class VoiceTriggersTableViewCell: UITableViewCell {

    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnTest: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var btnMicroPhone: UIButton!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

class SectionHeader:UITableViewCell{
    @IBOutlet weak var lblHeader: UILabel!
}
