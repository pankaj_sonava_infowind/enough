//
//  OptionsTableViewCell.swift
//  enough!
//
//  Created by mac on 01/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblOption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    func setRowData(status:Bool) {
        if lblOption.text == EnoughStatus_Field.test_alert{
            imgArrow.image = UIImage(named: "arrow-right")
            if status{
                imgArrow.tintColor = UIColor.darkGray
                lblOption.textColor = UIColor.darkGray
            }else{
                imgArrow.tintColor = UIColor.lightGray
                lblOption.textColor = UIColor.lightGray
            }
        }else{
            if status{
                imgArrow.image = UIImage(named: "check")
                imgArrow.tintColor = UIColor(named: "darkPink")
                lblOption.textColor = UIColor(named: "darkPink")
            }else{
                imgArrow.image = UIImage(named: "arrow-right")
                imgArrow.tintColor = UIColor.darkGray
                lblOption.textColor = UIColor.darkGray
            }
        }
        imgArrow.image = imgArrow.image?.withRenderingMode(.alwaysTemplate)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
