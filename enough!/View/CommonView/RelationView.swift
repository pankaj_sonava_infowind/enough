//
//  RelationView.swift
//  enough!
//
//  Created by Pankaj Sonava on 20/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol RelationViewActions {
    func croBtnClick(sender:UIButton)
    func okBtnClick(sender:UIButton)
}
class RelationView: UIView,UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate {

    var delegate:RelationViewActions?
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btn_ok: UIButton!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var tbl_list: UITableView!
    
    var dropDownValues = [String]()
    var selectedValue = ""
    var otherRelatoin = ""
    var selectedRelation = ""
    var selectedCell_bg = UIColor(named: "lightPink")
    var selectedCell_text = UIColor(named: "darkPurple")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let bundle = Bundle(identifier: KBundleIdentifier)
        bundle?.loadNibNamed("RelationView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        
        let nib = UINib(nibName: "RelationTVC", bundle: bundle)
        tbl_list.register(nib, forCellReuseIdentifier: "RelationTVC")
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBAction func crossBtnAction(_ sender: UIButton) {
        removeFromSuperview()
        delegate?.croBtnClick(sender: btn_ok)
    }
    @IBAction func selectBtnAction(_ sender: UIButton) {
        var value = ""
        if selectedValue == Relationships.other{
            value = otherRelatoin
        }else{
            value = selectedValue
        }
        btn_ok.layer.setValue(value, forKey: "SelectedValue")
        removeFromSuperview()
        delegate?.okBtnClick(sender: btn_ok)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RelationTVC") as? RelationTVC
        cell?.selectionStyle = .none
        cell?.lbl_relation.text  = dropDownValues[indexPath.row]
        cell?.txt_other.delegate = self
        if selectedValue == dropDownValues[indexPath.row]{
            cell?.backgroundColor = selectedCell_bg
            cell?.lbl_relation?.textColor = selectedCell_text
            if selectedValue == Relationships.other{
                cell?.txt_other.becomeFirstResponder()
            }
        }else{
            cell?.backgroundColor = UIColor.white
            cell?.lbl_relation?.textColor = UIColor.black
        }
        if dropDownValues[indexPath.row] == Relationships.other{
            cell?.txt_other.isHidden = false
            cell?.txt_other.text = otherRelatoin
        }else{
            cell?.txt_other.isHidden = true
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedValue = dropDownValues[indexPath.row]
        tbl_list.reloadData()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedValue = Relationships.other
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        otherRelatoin = textField.text!
        tbl_list.reloadData()
    }
}
