//
//  InfoPopUpView.swift
//  enough!
//
//  Created by Pankaj Sonava on 20/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol InfoPopUpActions {
    func crossBtnAction(sender:UIButton)
    func okBtnAction(sender:UIButton)
}
class InfoPopUpView: UIView {
    @IBOutlet var btn_ok: UIButton!
    @IBOutlet var btn_cross: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lbl_info: UILabel!
    var delegate:InfoPopUpActions?
    override init(frame: CGRect) {
        super.init(frame: frame)
        let bundle = Bundle(identifier: KBundleIdentifier)
        bundle?.loadNibNamed("InfoPopUpView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBAction func okBtnAction(_ sender: UIButton) {
        removeFromSuperview()
        delegate?.okBtnAction(sender: btn_ok)
    }
    @IBAction func crossBtnAction(_ sender: UIButton) {
        removeFromSuperview()
        delegate?.crossBtnAction(sender: btn_cross)
    }
}
