//
//  RequestTVC.swift
//  enough!
//
//  Created by Pankaj Sonava on 17/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class RequestTVC: UITableViewCell {

    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var btn_accept: UIButton!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    var requestDetails:Request?{
        didSet{
            let req_id = requestDetails!.id
            btn_accept.layer.setValue(req_id, forKey: "req_id")
            btn_cancel.layer.setValue(req_id, forKey: "req_id")
            if requestDetails?.created_by == Enough.shared.loginUser.id{
                lbl_userName.text = requestDetails?.reciver_name
                btn_accept.isHidden = true
            }else{
                lbl_userName.text = requestDetails?.sender_name
                btn_accept.isHidden = false
            }
        }
    }

}
