//
//  HeaderPopUp.swift
//  enough!
//
//  Created by Pankaj Sonava on 11/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol HeaderProtocol {
    func okButtonPress(isLive:Bool)
    func closeButtonPress(isLive:Bool)
}

class HeaderPopUp: UIView {

    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var view_bottom: UIStackView!
    @IBOutlet weak var lbl_yourSafeContact: UILabel!
    @IBOutlet weak var btn_ok: UIButton!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_liveOrTest: UILabel!
    @IBOutlet var contentView: UIView!
    var delegate:HeaderProtocol?
    var isLiveBtnPress = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewSetup()
    }
    func viewSetup() {
        let bundle = Bundle(identifier: KBundleIdentifier)
        bundle?.loadNibNamed("HeaderPopUp", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        if isLiveBtnPress{
            lbl_liveOrTest.text = "LIVE"
            lbl_description.text = "If you trigger an alert,\nthe community will be notified."
            btn_ok.setTitle("YES, go LIVE!", for: .normal)
            btn_cancel.titleLabel?.underline()
            btn_cancel.isHidden = false
            lbl_yourSafeContact.isHidden = true
            view_bottom.isHidden = true
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func okBtnAction(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.okButtonPress(isLive: isLiveBtnPress)
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.closeButtonPress(isLive: isLiveBtnPress)
    }
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.removeFromSuperview()
        delegate?.closeButtonPress(isLive: isLiveBtnPress)
    }
}
