//
//  HeaderView.swift
//  enough!
//
//  Created by Pankaj Sonava on 11/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol HeaderActions {
    func onOffToggle(sender:UIButton)
    func testLiveToggle(sender:UIButton)
    func menuButtonAction(sender:UIButton)
}

class HeaderView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btn_menu: UIButton!
    @IBOutlet weak var btnTestLiveToggle: UIButton!
    @IBOutlet weak var btnOnOfToggle: UIButton!
    var headerDelegate:HeaderActions?
    override init(frame: CGRect) {
        super.init(frame: frame)
        DispatchQueue.main.async {
            self.viewSetup()
        }
        headerDelegate?.menuButtonAction(sender: btn_menu)
    }
    func viewSetup() {
        let bundle = Bundle(identifier: KBundleIdentifier)
        bundle?.loadNibNamed("HeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        if Enough.shared.appMode == nil{
            SettingsViewModel().getAppMode(viewController: UIViewController()) {
                DispatchQueue.main.async {
                    self.checkTriggerMode()
                }
            }
        }else{
            checkTriggerMode()
        }
    }
    func hideButtons(){
        btnOnOfToggle.isHidden = true
        btnTestLiveToggle.isHidden = true
        btn_menu.isEnabled = false
    }
    func checkTriggerMode() {
        if Enough.shared.triggerMode == .off{
            btnTestLiveToggle.isHidden = true
            btnOnOfToggle.isSelected = false
        }else{
            btnOnOfToggle.isSelected = true
            btnTestLiveToggle.isHidden = false
        }
        if let appMode = Enough.shared.appMode {
            if appMode == .test{
                btnTestLiveToggle.isSelected = false
            }else{
                btnTestLiveToggle.isSelected = true
            }
        }
    }
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }
    @IBAction func onOffToggleBtnAction(_ sender: UIButton) {
        if sender.isSelected{
            btnTestLiveToggle.isHidden = true
        }
        headerDelegate?.onOffToggle(sender: sender)
    }
    @IBAction func testLiveBtnAction(_ sender: UIButton) {
        headerDelegate?.testLiveToggle(sender: sender)
    }
    @IBAction func menuBtnAction(_ sender: UIButton) {
        headerDelegate?.menuButtonAction(sender: sender)
    }
}
