//
//  ContactSectionTVC.swift
//  enough!
//
//  Created by Pankaj Sonava on 14/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class ContactSectionTVC: UITableViewCell {

    @IBOutlet weak var btn_contact: UIButton!
    @IBOutlet weak var btn_addContact: UIButton!
    @IBOutlet weak var btn_toolTip: UIButton!
    @IBOutlet weak var lbl_safeContact: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
