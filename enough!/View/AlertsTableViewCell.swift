//
//  AlertsTableViewCell.swift
//  enough!
//
//  Created by mac on 02/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class AlertsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_emergencyType: UILabel!
    @IBOutlet weak var lbl_responder: UILabel!
    @IBOutlet weak var lbl_heppning: UILabel!
    @IBOutlet weak var lbl_timeAgo: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_victim: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var alert_details:Alert?{
        didSet{
            lbl_victim.text = alert_details?.user.name
            lbl_responder.text = "\(alert_details!.responder.count) responders"
            lbl_emergencyType.text = alert_details?.emergency_type
            if let distance = alert_details?.distance{
                let distanceNumber = Double(distance)!
                if distanceNumber > 999{
                    lbl_distance.text = "\((distanceNumber/1000).rounded(toPlaces: 1))km from your location"
                }else{
                    lbl_distance.text = "\(distanceNumber)m from your location"
                }
            }
            lbl_timeAgo.text =  alert_details?.timediffrance
        }
    }

}
