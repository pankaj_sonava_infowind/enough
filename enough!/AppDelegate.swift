//
//  AppDelegate.swift
//  enough!
//
//  Created by Pankaj Sonava on 01/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import GoogleMaps
import GooglePlaces
//
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window:UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Date_Formatter.locale = Locale(identifier: "en_US_POSIX")
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        checkSession()
        GMSServices.provideAPIKey("AIzaSyBjJZqtyNKWJ-L636Ha9v4jqRkUyaZBBBc")
        GMSPlacesClient.provideAPIKey("AIzaSyBjJZqtyNKWJ-L636Ha9v4jqRkUyaZBBBc")
        return true
    }
    func checkSession() {
        if let loginData = UserDefaults.standard.value(forKey: "Login_Data") as? [String:String]{
            if let homeStep = UserDefaults.standard.value(forKey: "HomeSteps") as? [String:String]{
                Enough.shared.assignHomeStepsFromDic(dic: homeStep)
            }
            let loginUser = User.init(dic: loginData)
            Enough.shared.loginUser = loginUser
            Enough.shared.enough_status.acc_created = true
            guard let rootVC = Main_Storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as? SWRevealViewController  else {
                print("Tab Bar not found")
                return
            }
            SettingsViewModel().getAppMode(viewController: UIViewController()) {
            }
            let rootNC = UINavigationController(rootViewController: rootVC)
            rootNC.navigationBar.isHidden = true
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "enough_")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

