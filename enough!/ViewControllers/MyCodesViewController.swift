//
//  MyCodesViewController.swift
//  enough!
//
//  Created by mac on 20/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol CodeUpdate {
    func updateHome()
}

class MyCodesViewController: UIViewController,UIPopoverPresentationControllerDelegate,codeDeleteProtocol,updateEditCodeProtocol{
    
    @IBOutlet weak var viewOk: UIView!
    @IBOutlet weak var viewPanic: UIView!
    @IBOutlet weak var viewPanicNet: UIView!
    @IBOutlet weak var btnQ1: UIButton!
    @IBOutlet weak var btnQ2: UIButton!
    @IBOutlet weak var btnQ3: UIButton!
    @IBOutlet weak var My1: UILabel!
    @IBOutlet weak var ok: UILabel!
    @IBOutlet weak var code1: UILabel!
    @IBOutlet weak var My2: UILabel!
    @IBOutlet weak var panic: UILabel!
    @IBOutlet weak var code2: UILabel!
    @IBOutlet weak var My3: UILabel!
    @IBOutlet weak var panic2: UILabel!
    @IBOutlet weak var code3: UILabel!
    @IBOutlet weak var user: UILabel!
    @IBOutlet weak var btnEdit1: UIButton!
    @IBOutlet weak var btnCancel1: UIButton!
    @IBOutlet weak var btnAdd1: UIButton!
    @IBOutlet weak var btnEdit2: UIButton!
    @IBOutlet weak var btnCancel2: UIButton!
    @IBOutlet weak var btnAdd2: UIButton!
    @IBOutlet weak var btnEdit3: UIButton!
    @IBOutlet weak var btnCancel3: UIButton!
    @IBOutlet weak var btnAdd3: UIButton!
    @IBOutlet weak var tooltip1: UITextView!
    @IBOutlet weak var tooltip2: UITextView!
    @IBOutlet weak var tooltip3: UITextView!
    
    var q1 : Bool = false
    var q2 : Bool = false
    var q3 : Bool = false
    
    var codeVM = CodeViewModel()
    var verifyUserVM = VerifyUserViewModel()
    var delegate:AlertsViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        getCode()
        tooltip1.isHidden = true
        tooltip2.isHidden = true
        tooltip3.isHidden = true
    }
    
    func getCode(){
        codeVM.getCode(viewController: self) {
            print(self.codeVM.getCodeArray)
            DispatchQueue.main.async {
                if self.codeVM.getCodeArray["ok_code"] == ""{
                    self.viewOk.backgroundColor = .lightGray
                    self.btnAdd1.isHidden = false
                    self.btnEdit1.isHidden = true
                    self.btnCancel1.isHidden = true
                    self.My1.textColor = .darkGray
                    self.ok.textColor = .darkGray
                    self.code1.textColor = .darkGray
                }else{
                    self.viewOk.backgroundColor = UIColor(named: "lightPink")
                    self.btnAdd1.isHidden = true
                    self.btnEdit1.isHidden = false
                    self.btnCancel1.isHidden = false
                    self.My1.textColor = UIColor(named: "darkPink")
                    self.ok.textColor = UIColor(named: "darkPink")
                    self.code1.textColor = UIColor(named: "darkPink")
                }
                if self.codeVM.getCodeArray["panic_code"] == ""{
                    self.viewPanic.backgroundColor = .lightGray
                    self.btnAdd2.isHidden = false
                    self.btnEdit2.isHidden = true
                    self.btnCancel2.isHidden = true
                    self.My2.textColor = .darkGray
                    self.panic.textColor = .darkGray
                    self.code2.textColor = .darkGray
                }else{
                    self.viewPanic.backgroundColor = UIColor(named: "lightPink")
                    self.btnAdd2.isHidden = true
                    self.btnEdit2.isHidden = false
                    self.btnCancel2.isHidden = false
                    self.My2.textColor = UIColor(named: "darkPink")
                    self.panic.textColor = UIColor(named: "darkPink")
                    self.code2.textColor = UIColor(named: "darkPink")
                }
                if self.codeVM.getCodeArray["panic_network_code"] == ""{
                    self.viewPanicNet.backgroundColor = .lightGray
                    self.btnAdd3.isHidden = false
                    self.btnEdit3.isHidden = true
                    self.btnCancel3.isHidden = true
                    self.My3.textColor = .darkGray
                    self.panic2.textColor = .darkGray
                    self.code3.textColor = .darkGray
                }else{
                    self.viewPanicNet.backgroundColor = UIColor(named: "lightPink")
                    self.btnAdd3.isHidden = true
                    self.btnEdit3.isHidden = false
                    self.btnCancel3.isHidden = false
                    self.My3.textColor = UIColor(named: "darkPink")
                    self.panic2.textColor = UIColor(named: "darkPink")
                    self.code3.textColor = UIColor(named: "darkPink")
                }
                if self.codeVM.getCodeArray["ok_code"] != "" && self.codeVM.getCodeArray["panic_code"] != "" && self.codeVM.getCodeArray["panic_network_code"] != ""{
                    self.updateHomeSteps(value:"1")
                }else{
                    self.updateHomeSteps(value: "")
                }
            }
        }
    }
    func updateHomeSteps(value:String) {
        let param = ["user_id":Enough.shared.loginUser.id,"create_code":value]
        print("param:",param)
        Enough.shared.enough_status.acc_veryfied = true
        verifyUserVM.updateHomeSteps(viewController: self, param: param){ (result) in
            if result["status"]?.int == 1{
                if var homeStep = UserDefaults.standard.value(forKey: "HomeSteps") as? [String:String]{
                    homeStep["create_code"] = value
                    UserDefaults.standard.set(homeStep, forKey: "HomeSteps")
                    //self.delegate!.updateHome()
                }
            }
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionQ1(_ sender: Any) {
        if q1 == false{
            tooltip1.isHidden = false
            tooltip2.isHidden = true
            tooltip3.isHidden = true
            q1 = true
        }else{
            tooltip1.isHidden = true
            tooltip2.isHidden = true
            tooltip3.isHidden = true
            q1 = false
        }
    }
    @IBAction func actionQ2(_ sender: Any) {
        if q2 == false{
            tooltip1.isHidden = true
            tooltip2.isHidden = false
            tooltip3.isHidden = true
            q2 = true
        }else{
            tooltip1.isHidden = true
            tooltip2.isHidden = true
            tooltip3.isHidden = true
            q2 = false
        }
        
    }
    @IBAction func actionQ3(_ sender: Any) {
        if q3 == false{
            tooltip1.isHidden = true
            tooltip2.isHidden = true
            tooltip3.isHidden = false
            q3 = true
        }else{
            tooltip1.isHidden = true
            tooltip2.isHidden = true
            tooltip3.isHidden = true
            q3 = false
        }
        
    }
    @IBAction func actionEdit1(_ sender: Any) {
        presentEditCode(typeCode: "ok")
    }
    @IBAction func actionCancel1(_ sender: Any) {
        presentDeleteCode(typeCode: "ok")
    }
    @IBAction func actionAdd1(_ sender: Any) {
        presentEditCode(typeCode: "ok")
    }
    @IBAction func actionEdit2(_ sender: Any) {
        presentEditCode(typeCode: "panic")
    }
    @IBAction func actionCancel2(_ sender: Any) {
        presentDeleteCode(typeCode: "panic")
    }
    @IBAction func actionAdd2(_ sender: Any) {
        presentEditCode(typeCode: "panic")
    }
    @IBAction func actionEdit3(_ sender: Any) {
        presentEditCode(typeCode: "panic_net")
    }
    @IBAction func actionCancel3(_ sender: Any) {
        presentDeleteCode(typeCode: "panic_net")
    }
    @IBAction func actionAdd3(_ sender: Any) {
        
        presentEditCode(typeCode: "panic_net")
    }
    
    func presentEditCode(typeCode:String){
        let EnterCodePopupVC = self.storyboard?.instantiateViewController(withIdentifier: "EnterCodePopupViewController") as! EnterCodePopupViewController
        EnterCodePopupVC.modalPresentationStyle = .overCurrentContext
        EnterCodePopupVC.modalTransitionStyle = .crossDissolve
        EnterCodePopupVC.codeVC = codeVM
        EnterCodePopupVC.typeCode = typeCode
        EnterCodePopupVC.delegate = self
        self.present(EnterCodePopupVC, animated: true, completion: nil)
    }
    
    func presentDeleteCode(typeCode:String){
        let DeleteCodePopupVC = self.storyboard?.instantiateViewController(withIdentifier: "DeleteCodePopupViewController") as! DeleteCodePopupViewController
        DeleteCodePopupVC.modalPresentationStyle = .overCurrentContext
        DeleteCodePopupVC.modalTransitionStyle = .crossDissolve
        DeleteCodePopupVC.codeVC = codeVM
        DeleteCodePopupVC.typeCode = typeCode
        DeleteCodePopupVC.delegate = self
        
        self.present(DeleteCodePopupVC, animated: true, completion: nil)
    }
    
    func deleteSuccess(typeCode:String){
        getCode()
    }
    
    func updateEditSuccess(typeCode:String){
        getCode()
    }
}
