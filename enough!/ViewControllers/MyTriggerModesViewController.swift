//
//  MyTriggerModesViewController.swift
//  enough!
//
//  Created by mac on 08/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class MyTriggerModesViewController: UIViewController {
    
    @IBOutlet weak var actionAudioTriggersLiveOff: UIButton!
    @IBOutlet weak var actionAlertButtonLiveOff: UIButton!
    @IBOutlet weak var actionOnOffLiveOff: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
 
}
