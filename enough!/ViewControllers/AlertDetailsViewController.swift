//
//  AlertDetailsViewController.swift
//  enough!
//
//  Created by mac on 06/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class AlertDetailsViewController: UIViewController {
    
    @IBOutlet weak var lbl_mode: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTypeUser: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var lblAddress3: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblTrigger: UILabel!
    @IBOutlet weak var lblResponder: UILabel!
    @IBOutlet weak var lblConfirmedBy: UILabel!
    var alert:Alert!
    var alert_list:[Alert]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
    }
    func viewSetup() {
        lblName.text = alert.user.name
        lblResponder.text = "\(alert.responder.count) responders"
        lblType.text = alert.emergency_type
        var img_url = ApiManager.base_url + alert.user.profile_image
        if let range = img_url.range(of: "./../"){
            img_url.removeSubrange(range)
        }
        img_user.sd_setImage(with: URL(string: img_url)) { (image, error, cacheType, url) in
            if let img = image{
                self.img_user.image = img
            }
        }
        lblTrigger.text =  alert?.timediffrance
        if alert.mode == App_Mode.test.rawValue{
            lbl_mode.text = "TEST MODE"
        }else{
            lbl_mode.text = "LIVE MODE"
        }
        if alert.user.id == "1"{
            lblTypeUser.text = "Verified"
        }else{
            lblTypeUser.text = "Unverified"
        }
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func actionLiveFeed(_ sender: Any) {
    }
    @IBAction func actionGrpCall(_ sender: Any) {
    }
    @IBAction func viewMapBtnAction(_ sender: Any) {
        let alertLocationVC = storyboard?.instantiateViewController(withIdentifier: "AlertLocationVC") as! AlertLocationVC
        alertLocationVC.alert = alert
        alertLocationVC.alertList = alert_list
        navigationController?.pushViewController(alertLocationVC, animated: true)
    }
    @IBAction func actionCallAuthority(_ sender: Any) {
    }
    @IBAction func actionCloudRecording(_ sender: Any) {
    }
    @IBAction func actionGrpChat(_ sender: Any) {
    }
    @IBAction func actionAlertCommunity(_ sender: Any) {
    }
    @IBAction func actionUpdateAlert(_ sender: Any) {
    }
}
