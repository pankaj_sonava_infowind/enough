//
//  DeleteCodePopupViewController.swift
//  enough!
//
//  Created by mac on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol codeDeleteProtocol {
    func deleteSuccess(typeCode:String)
}

class DeleteCodePopupViewController: UIViewController {
    
    var codeVC:CodeViewModel?
    var typeCode:String?
    var delegate:MyCodesViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func actionCros(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOk(_ sender: Any) {
        if typeCode == "ok"{
            deleteOkCode()
        }else if typeCode == "panic"{
                   deletePanicCode()
               }else  if typeCode == "panic_net"{
                         deletePanicNetCode()
                      }
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func deleteOkCode(){
      let param = ["user_id":Enough.shared.loginUser.id,"code_id":codeVC!.getCodeArray["ok_code_id"]!]
        print("param",param)
        codeVC!.cancelCode(viewController: self, parameter: param ) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                        Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.CodeDelete_Success, viewController: self) {
                            self.delegate!.deleteSuccess(typeCode:self.typeCode!)
                            self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    print("result",result)
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    func deletePanicCode(){
       let param = ["user_id":Enough.shared.loginUser.id,"code_id":codeVC!.getCodeArray["panic_code_id"]!]
               print("param",param)
               codeVC!.cancelCode(viewController: self, parameter: param ) { (result) in
                   DispatchQueue.main.async {
                       if result["status"]?.int == 1{
                           
                               Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.CodeDelete_Success, viewController: self) {
                                self.delegate!.deleteSuccess(typeCode:self.typeCode!)
                                self.dismiss(animated: true, completion: nil)
                           }
                       }else{
                           print("result",result)
                           Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                       }
                   }
               }
    }
    func deletePanicNetCode(){
        let param = ["user_id":Enough.shared.loginUser.id,"code_id":codeVC!.getCodeArray["panic_network_code_id"]!]
               print("param",param)
               codeVC!.cancelCode(viewController: self, parameter: param ) { (result) in
                   DispatchQueue.main.async {
                       if result["status"]?.int == 1{
                           
                               Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.CodeDelete_Success, viewController: self) {
                                self.delegate!.deleteSuccess(typeCode:self.typeCode!)
                                self.dismiss(animated: true, completion: nil)
                           }
                       }else{
                           print("result",result)
                           Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                       }
                   }
               }
    }
}
