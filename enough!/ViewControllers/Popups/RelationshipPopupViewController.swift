//
//  RelationshipPopupViewController.swift
//  enough!
//
//  Created by mac on 10/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol RelationProtocol {
    func selectedRelation(relation:String)
}

class RelationshipPopupViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var txtFOther: UITextField!
    @IBOutlet weak var tblVRelationship: UITableView!
    var delegate:EditAddHouseholdPopupViewController?
    var delegate2: MissingPersonTemplateViewController?
    var relation : String?
    
    var arrRelation = ["Father","Mother","Husband/Boyfriend","Wife/Girlfriend","Brother","Sister","Brother in law","Sister in law","Uncle","Aunt","Cousin","Friend"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRelation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVRelationship.dequeueReusableCell(withIdentifier: "RelationshipCell") as! RelationTableViewCell
        cell.lblRelation.text = arrRelation[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tblVRelationship.cellForRow(at: indexPath) as! RelationTableViewCell
        relation = cell.lblRelation.text!

           print(cell.lblRelation.text!)
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        if txtFOther.text != ""{
            relation = txtFOther.text!
        }
        if delegate != nil && relation != nil{
            delegate!.selectedRelation(relation:relation!)
        }
        if delegate2 != nil && relation != nil{
            delegate2!.selectedRelation(relation:relation!)
        }
        if relation == nil{
            Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.ProfileUpdate_Success, viewController: self) {
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}

class RelationTableViewCell:UITableViewCell{
    
    @IBOutlet weak var lblRelation: UILabel!
}
