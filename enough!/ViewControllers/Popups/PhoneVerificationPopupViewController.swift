//
//  PhoneVerificationPopupViewController.swift
//  enough!
//
//  Created by mac on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import FirebaseAuth
import SVProgressHUD

class PhoneVerificationPopupViewController: UIViewController {

   @IBOutlet weak var txtFNo: UITextField!
    var keyStatus = ""
    var mobileNumber = ""
    let authVM = AuthViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionOk(_ sender: Any) {
        print(txtFNo.text!)
        if txtFNo.text!.isEmpty{
            return
        }
        let credential = PhoneAuthProvider.provider().credential(
        withVerificationID: keyStatus,
        verificationCode: txtFNo.text!)
        
        SVProgressHUD.show(withStatus: "Verifying...")
        Auth.auth().signIn(with: credential) { (authResult, error) in
          if let error = error {
            Enough.shared.showOnlyAlert(message: error.localizedDescription, viewController: self)
            return
            }
            let param = ["mobile":self.mobileNumber]
            self.authVM.mobileVerify(viewController: self, parameter: param) { (result) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionResendCode(_ sender: Any) {
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumber, uiDelegate: nil) { (statusId, error) in
            if let stsID = statusId{
                self.keyStatus = stsID
            }else{
                Enough.shared.showOnlyAlert(message: error?.localizedDescription ?? Alert_Msg.Wrong, viewController: self)
            }
        }
    }
    @IBAction func actionRestart(_ sender: Any) {
        txtFNo.text = ""
    }
    @IBAction func action1(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "1"
    }
    @IBAction func action2(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "2"
    }
    
    @IBAction func action3(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "3"
    }
    @IBAction func action4(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "4"
    }
    @IBAction func action5(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "5"
    }
    @IBAction func action6(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "6"
    }
    @IBAction func action7(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "7"
    }
    @IBAction func action8(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "8"
    }
    @IBAction func action9(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "9"
    }
}
