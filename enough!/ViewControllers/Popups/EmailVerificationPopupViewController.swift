//
//  EmailVerificationPopupViewController.swift
//  enough!
//
//  Created by mac on 22/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol EmailVerificationDelegate {
    func checkStatus()
}

class EmailVerificationPopupViewController: UIViewController,EmailVerifiedDelegate {
    var delegate:VerifyAccountViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionOk(_ sender: Any) {
        /*let EmailVerifiedPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailVerifiedPopupViewController") as! EmailVerifiedPopupViewController
        EmailVerifiedPopupVC.modalPresentationStyle = .overCurrentContext
        EmailVerifiedPopupVC.modalTransitionStyle = .crossDissolve
        EmailVerifiedPopupVC.delegate = self
        self.present(EmailVerifiedPopupVC, animated: true, completion: nil)*/
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionResendEmail(_ sender: Any) {
    }
    
    func emialVerified(){
        self.dismiss(animated: true, completion: nil)
        delegate!.checkStatus()
    }
}
