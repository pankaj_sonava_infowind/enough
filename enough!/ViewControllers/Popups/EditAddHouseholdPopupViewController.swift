//
//  EditAddHouseholdPopupViewController.swift
//  enough!
//
//  Created by mac on 11/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol HouseholdProtocolSecond {
    func getHouseholdList()
}

class EditAddHouseholdPopupViewController: UIViewController, RelationProtocol,UIPopoverPresentationControllerDelegate{
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblSelectRelationship: UILabel!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtFLastName: UITextField!
    var addHouseholdVM:AddHouseholdViewModel?
    var typeScreen:String?
    var id:String?
    var index:Int?
    var delegate:MyHouseholdViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if typeScreen! == "Add"{
            lblHeading.text = "Add household Member"
        }else{
            txtFName.text = addHouseholdVM!.allHouseholdList[index!].firstname!
            txtFLastName.text = addHouseholdVM!.allHouseholdList[index!].lastname!
            lblSelectRelationship.text = addHouseholdVM!.allHouseholdList[index!].relationship
        }
    }
    
    @IBAction func actionSelectRalationship(_ sender: Any) {
        let RelationshipPopUp = self.storyboard?.instantiateViewController(withIdentifier: "RelationshipPopupViewController") as! RelationshipPopupViewController
        RelationshipPopUp.modalPresentationStyle = .overCurrentContext
        RelationshipPopUp.modalTransitionStyle = .crossDissolve
        RelationshipPopUp.delegate = self
        self.present(RelationshipPopUp, animated: true, completion: nil)
    }
    
    func selectedRelation(relation: String) {
        lblSelectRelationship.text! = relation
    }
    
    @IBAction func actionSave(_ sender: Any) {
        if typeScreen! == "Add"{
            addHousehold()
        }else{
            editHousehold()
        }
    }
    
    func addHousehold(){
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let param = [ "user_id":Enough.shared.loginUser.id,"firstname":txtFName.text!,"lastname":txtFLastName.text!,"contact_email":"","contactphone":"","relationship":lblSelectRelationship.text!,"type":"2"]
        print("param",param)
        addHouseholdVM!.addHousehold(viewController: self, parameter: param) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                        Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.addHousehold_Success, viewController: self) {
                            self.delegate!.getHouseholdList()
                            self.dismiss(animated: true, completion: nil)
                        }
                }else{
                    print("result",result)
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    
    func editHousehold(){
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let param = [ "user_id":Enough.shared.loginUser.id,"firstname":txtFName.text!,"lastname":txtFLastName.text!,"contact_email":"","contactphone":"","relationship":lblSelectRelationship.text!,"type":"2","household_contact_id":self.id!]
        print("param",param)
        addHouseholdVM!.editHousehold(viewController: self, parameter: param) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                      print("edit successfull")
                        Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.editHousehold_Success, viewController: self) {
                            self.delegate!.getHouseholdList()
                            self.dismiss(animated: true, completion: nil)
                        }
                }else{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    
    func isValidUserEntry() -> (Bool,String) {
    if txtFName.text!.isEmpty{
        return (false,Error_Alert.Missing_Name)
    }
    if txtFLastName.text!.isEmpty {
        return (false,Error_Alert.Missing_Lastname)
    }
    if lblSelectRelationship.text! == "Select relationship"{
        return (false,Error_Alert.Missing_Relation)
    }
        return (true,"")
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
