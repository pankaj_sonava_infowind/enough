//
//  VerifyPhotoPopupViewController.swift
//  enough!
//
//  Created by mac on 22/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
protocol ProfilePicAction {
    func selectImage(image:UIImage)
}
class VerifyPhotoPopupViewController: UIViewController {

    var imagePicker = UIImagePickerController()
    var delegate:ProfilePicAction?
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func actionTakePhoto(_ sender: Any) {
        self.imagePicker.sourceType = .camera
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    @IBAction func actionUploadPhoto(_ sender: Any) {
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    @IBAction func actionViewPhoto(_ sender: Any) {
    }
    @IBAction func actionDeletePhoto(_ sender: Any) {
    }
}
extension VerifyPhotoPopupViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            if let data = img.jpegData(compressionQuality: 0.5){
               let sizeKB = data.count/1024
                delegate?.selectImage(image: img)
                picker.dismiss(animated: true, completion: nil)
                dismiss(animated: true, completion: nil)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
