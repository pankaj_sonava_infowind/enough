//
//  EnterCodePopupViewController.swift
//  enough!
//
//  Created by mac on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol updateEditCodeProtocol {
    func updateEditSuccess(typeCode:String)
}

class EnterCodePopupViewController: UIViewController {
    
    @IBOutlet weak var txtFNo: UITextField!
    var codeVC:CodeViewModel?
    var typeCode:String?
    var delegate:MyCodesViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOk(_ sender: Any) {
        print(txtFNo.text!)
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        if typeCode == "ok"{
                   updateOkCode()
               }else if typeCode == "panic"{
                          updatePanicCode()
                      }else  if typeCode == "panic_net"{
                                updatePanicNetCode()
                             }
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionRestart(_ sender: Any) {
        txtFNo.text = ""
    }
    @IBAction func action1(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "1"
    }
    @IBAction func action2(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "2"
    }
    
    @IBAction func action3(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "3"
    }
    @IBAction func action4(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "4"
    }
    @IBAction func action5(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "5"
    }
    @IBAction func action6(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "6"
    }
    @IBAction func action7(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "7"
    }
    @IBAction func action8(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "8"
    }
    @IBAction func action9(_ sender: Any) {
        txtFNo.text = (txtFNo.text!) + "9"
    }
    
    func updateOkCode(){
        let param = ["user_id":Enough.shared.loginUser.id,"ok_code":txtFNo.text!,"panic_code":codeVC!.getCodeArray["panic_code"]!,"panic_network_code":codeVC!.getCodeArray["panic_network_code"]!]
        print("param",param)
        codeVC!.updateCode(viewController: self, parameter: param ) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                    
                        Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.CodeUpdate_Success, viewController: self) {
                            self.delegate!.updateEditSuccess(typeCode: self.typeCode!)
                            self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    print("result",result)
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    func updatePanicCode(){
        let param = ["user_id":Enough.shared.loginUser.id,"ok_code":codeVC!.getCodeArray["ok_code"]!,"panic_code":txtFNo.text!,"panic_network_code":codeVC!.getCodeArray["panic_network_code"]!]
               print("param",param)
               codeVC!.updateCode(viewController: self, parameter: param ) { (result) in
                   DispatchQueue.main.async {
                       if result["status"]?.int == 1{
                           
                               Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.CodeUpdate_Success, viewController: self) {
                                self.delegate!.updateEditSuccess(typeCode: self.typeCode!)
                                self.dismiss(animated: true, completion: nil)
                           }
                       }else{
                           print("result",result)
                           Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                       }
                   }
               }
    }
    func updatePanicNetCode(){
        let param = ["user_id":Enough.shared.loginUser.id,"ok_code":codeVC!.getCodeArray["ok_code"]!,"panic_code":codeVC!.getCodeArray["panic_code"]!,"panic_network_code":txtFNo.text!]
               print("param",param)
               codeVC!.updateCode(viewController: self, parameter: param ) { (result) in
                   DispatchQueue.main.async {
                       if result["status"]?.int == 1{
                           
                               Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.CodeUpdate_Success, viewController: self) {
                                self.delegate!.updateEditSuccess(typeCode: self.typeCode!)
                                self.dismiss(animated: true, completion: nil)
                           }
                       }else{
                           print("result",result)
                           Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                       }
                   }
               }
    }
    
    
    func isValidUserEntry() -> (Bool,String) {
        if txtFNo.text!.isEmpty{
            return (false,Error_Alert.Missing_Code)
        }
        return (true,"")
    }
    
}
