//
//  DeleteHouseholdPopupViewController.swift
//  enough!
//
//  Created by mac on 20/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol HouseholdProtocol {
    func getHouseholdList()
}

class DeleteHouseholdPopupViewController: UIViewController {

    var id :String?
    var addHouseholdVM:AddHouseholdViewModel?
    var index:Int?
    var delegate:MyHouseholdViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionOk(_ sender: UIButton) {
        addHouseholdVM!.deleteHousehold(req_id: id!, viewController: self) { result in
                       DispatchQueue.main.async {
                           if result["status"]?.int == 0{
                               Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                           }else{
                               Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.Household_Deleted, viewController: self) {
                                self.addHouseholdVM!.allHouseholdList.remove(at: self.index!)
                                self.delegate!.getHouseholdList()
                                self.dismiss(animated: true, completion: nil)
                               }
                           }
                       }
                   }
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
