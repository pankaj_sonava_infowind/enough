//
//  EmailVerifiedPopupViewController.swift
//  enough!
//
//  Created by mac on 22/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol EmailVerifiedDelegate {
    func emialVerified()
}

class EmailVerifiedPopupViewController: UIViewController {
    var delegate:EmailVerificationPopupViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionOk(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate!.emialVerified()
       }
       
}
