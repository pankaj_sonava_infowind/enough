//
//  TastVoiceCommandPopupViewController.swift
//  enough!
//
//  Created by mac on 29/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class TastVoiceCommandPopupViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblVoiceCommand: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblVoiceCommand.isHidden = true
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionTestNow(_ sender: Any) {
    }
    @IBAction func actionCAncel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
