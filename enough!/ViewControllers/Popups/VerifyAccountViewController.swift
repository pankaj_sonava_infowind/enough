//
//  VerifyAccountViewController.swift
//  enough!
//
//  Created by mac on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import FirebaseAuth

class VerifyAccountViewController: UIViewController,EmailVerificationDelegate,IdVerifyDelegate {
    
    @IBOutlet weak var txtFEmail: UITextField!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtFDOB: UITextField!
    @IBOutlet weak var txtFPhoneNo: UITextField!
    @IBOutlet weak var btn_verify: UIButton!
    @IBOutlet weak var btnEmailverify: UIButton!
    @IBOutlet weak var btnVerifyId: UIButton!
    let authVM = AuthViewModel()
    let verifyUserVM = VerifyUserViewModel()
    //var delegate:SettingsViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFEmail.text = Enough.shared.loginUser.email
        txtFName.text = Enough.shared.loginUser.firstname
        txtFDOB.text = Enough.shared.loginUser.dob
        txtFPhoneNo.text = Enough.shared.loginUser.mobile
        self.isVerifyAccount()
        getStatusAPIcall()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func getStatusAPIcall(){
        authVM.getVerifiedStatus(viewController: self) {
            DispatchQueue.main.async {
                self.isVerifyAccount()
                if self.btn_verify.tag == 1 && self.btnEmailverify.tag == 1 && self.btnVerifyId.tag == 1{
                    self.updateHomeSteps(value: "1")
                }else{
                    self.updateHomeSteps(value: "")
                }
            }
        }
    }
    func updateHomeSteps(value:String) {
        let param = ["user_id":Enough.shared.loginUser.id,"verify_your_account":value]
        if value == "1"{
            Enough.shared.enough_status.acc_veryfied = true
        }else{
            Enough.shared.enough_status.acc_veryfied = false
        }
        verifyUserVM.updateHomeSteps(viewController: self, param: param){ (result) in
            if result["status"]?.int == 1{
                if var homeStep = UserDefaults.standard.value(forKey: "HomeSteps") as? [String:String]{
                    homeStep["verify_your_account"] = value
                    UserDefaults.standard.set(homeStep, forKey: "HomeSteps")
                }
            }
        }
    }
    func isVerifyAccount() {
        if Enough.shared.loginUser.mobile_verify_status == "1"{
            let attributedString = NSMutableAttributedString(string: "Verified  ")
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            btn_verify.setAttributedTitle(attributedString, for: .normal)
            btn_verify.tintColor = UIColor(named: "lightMilky")
            btn_verify.tag = 1
        }
        if Enough.shared.loginUser.email_verify_status == "1"{
            let attributedString = NSMutableAttributedString(string: "Verified  ")
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            btnEmailverify.setAttributedTitle(attributedString, for: .normal)
            btnEmailverify.tintColor =  UIColor(named: "lightMilky")
            btnEmailverify.tag = 2
        }
        if Enough.shared.loginUser.id_verify_status == "1"{
            let attributedString = NSMutableAttributedString(string: "Verified  ")
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            btnVerifyId.setAttributedTitle(attributedString, for: .normal)
            btnVerifyId.tintColor = UIColor(named: "lightMilky")
            btnVerifyId.tag = 3
        }
    }
    @IBAction func actionEmailVerify(_ sender: Any) {
        if btnEmailverify.tag == 2{
            return
        }
        verifyUserVM.emailVerification(viewController: self) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                    
                    let EmailVerificationPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailVerificationPopupViewController") as! EmailVerificationPopupViewController
                    EmailVerificationPopupVC.modalPresentationStyle = .overCurrentContext
                    EmailVerificationPopupVC.modalTransitionStyle = .crossDissolve
                    EmailVerificationPopupVC.delegate = self
                    self.present(EmailVerificationPopupVC, animated: true, completion: nil)
                }else{
                    print("result",result)
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
        @IBAction func actionVerifyId(_ sender: Any) {
            if btnVerifyId.tag == 3{
                return
            }
            let VerifyIdentityPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyIdentityPopupViewController") as! VerifyIdentityPopupViewController
            VerifyIdentityPopupVC.modalPresentationStyle = .overCurrentContext
            VerifyIdentityPopupVC.modalTransitionStyle = .crossDissolve
            VerifyIdentityPopupVC.delegate = self
            self.present(VerifyIdentityPopupVC, animated: true, completion: nil)
        }
        @IBAction func actionVerifyPhoneNo(_ sender: Any) {
            if btn_verify.tag == 1{
                return
            }
            var mobileNumber = Enough.shared.loginUser.mobile
            mobileNumber = mobileNumber.replacingOccurrences(of: " ", with: "")
            if mobileNumber.first! != "+"{
                mobileNumber = "+" + mobileNumber
            }
            PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumber, uiDelegate: nil) { (statusId, error) in
                if let stsID = statusId{
                    DispatchQueue.main.async {
                        let PhoneVerificationPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "PhoneVerificationPopupViewController") as! PhoneVerificationPopupViewController
                        PhoneVerificationPopupVC.keyStatus = stsID
                        PhoneVerificationPopupVC.mobileNumber = mobileNumber
                        PhoneVerificationPopupVC.modalPresentationStyle = .overCurrentContext
                        PhoneVerificationPopupVC.modalTransitionStyle = .crossDissolve
                        self.present(PhoneVerificationPopupVC, animated: true, completion: nil)
                    }
                }else{
                    Enough.shared.showOnlyAlert(message: error?.localizedDescription ?? Alert_Msg.Wrong, viewController: self)
                }
            }
        }
        @IBAction func actionCross(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
    
    func checkStatus() {
        getStatusAPIcall()
    }
}
