//
//  DeleteVoiceCommandPopUpViewController.swift
//  enough!
//
//  Created by mac on 29/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class DeleteVoiceCommandPopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionOk(_ sender: Any) {
    }
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
