//
//  AddressPopupViewController.swift
//  enough!
//
//  Created by mac on 11/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol addressProtocol{
    func addressValue(country:String,street:String,apartment:String,city:String,state:String,zipcode:String)
}

class Address_PopupViewController: UIViewController {
    
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var txtFStreetNo: UITextField!
    @IBOutlet weak var txtFApartment: UITextField!
    @IBOutlet weak var txtFCity: UITextField!
    @IBOutlet weak var txtFState: UITextField!
    @IBOutlet weak var txtFZipcode: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var height_picker: NSLayoutConstraint!
    @IBOutlet var view_picker: UIView!
    @IBOutlet weak var btnSave: UIButton!
    
    var delegate:MyProfileViewController?
    let authVM = AuthViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUp()
    }
    
    func viewSetUp(){
        txtFStreetNo.placeholderColor(text: "Street and number", color: UIColor.darkGray)
        txtFApartment.placeholderColor(text: "Apartment, suite, unit, building, floor etc", color: UIColor.darkGray)
        txtFCity.placeholderColor(text: "City", color: UIColor.darkGray)
        txtFState.placeholderColor(text: "state / province / region", color: UIColor.darkGray)
        txtFZipcode.placeholderColor(text: "Zipcode", color: UIColor.darkGray)
        
        txtFStreetNo.addPadding(padding: .equalSpacing(5))
        txtFApartment.addPadding(padding: .equalSpacing(5))
        txtFCity.addPadding(padding: .equalSpacing(5))
        txtFState.addPadding(padding: .equalSpacing(5))
        txtFZipcode.addPadding(padding: .equalSpacing(5))
        
        _ = isValidUserEntry()
    }
    
    @IBAction func actionCountry(_ sender: Any) {
        if authVM.countryList.count > 0{
            showCountryPicker()
        }else{
            authVM.getCountryList(viewcontroller: self) {
                DispatchQueue.main.async {
                    self.showCountryPicker()
                }
            }
        }
    }
    
    func showCountryPicker() {
        pickerView.isHidden = false
        pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func actionSave(_ sender: Any) {
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let country = lblCountry.text!
        let street = txtFStreetNo.text!
        let apartment = txtFApartment.text!
        let city = txtFCity.text!
        let state = txtFState.text!
        let zipcode = txtFZipcode.text!
        
        if delegate != nil{
            delegate!.addressValue(country:country,street:street,apartment:apartment,city:city,state:state,zipcode:zipcode)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectPickerContentAction(_ sender: UIButton) {
        lblCountry.text = authVM.countryList[pickerView.selectedRow(inComponent: 0)].name
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 0
            self.view.layoutIfNeeded()
        }
        _ = isValidUserEntry()
    }
    func isValidUserEntry() -> (Bool,String) {
            if lblCountry.text! == "Country"{
                 checkButtonEnable(isValid: false)
                return (false,"Select Country")
            }
            if txtFStreetNo.text!.isEmpty {
                 checkButtonEnable(isValid: false)
                return (false,"Enter street no")
            }
            if txtFApartment.text!.isEmpty{
                 checkButtonEnable(isValid: false)
                return (false,"Enter Apartment, suite, unit, building, floor etc")
        }
            if txtFCity.text!.isEmpty {
                 checkButtonEnable(isValid: false)
                return (false,"Enter city")
            }
            if txtFState.text!.isEmpty{
                 checkButtonEnable(isValid: false)
                return (false,"Enter state")
            }
            if txtFZipcode.text!.isEmpty{
                 checkButtonEnable(isValid: false)
                return (false,"Enter zipcode")
            }
         checkButtonEnable(isValid: true)
            return (true,"")
    }
    func checkButtonEnable(isValid:Bool) {
        if isValid{
            btnSave.backgroundColor = UIColor(named: "darkPink")
            btnSave.isSelected = true
        }else{
            btnSave.backgroundColor = UIColor.lightGray
            btnSave.isSelected = false
        }
    }
}

extension Address_PopupViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return authVM.countryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return authVM.countryList[row].name
    }
}

extension Address_PopupViewController:UITextFieldDelegate,UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        _ = isValidUserEntry()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = isValidUserEntry()
    }
}
