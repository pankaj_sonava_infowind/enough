//
//  VerifyIdentityPopupViewController.swift
//  enough!
//
//  Created by mac on 21/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol IdVerifyDelegate {
    func checkStatus()
}
class VerifyIdentityPopupViewController: UIViewController {
    @IBOutlet weak var img_photo: UIImageView!
    @IBOutlet weak var img_selfie: UIImageView!
    var imagePicker = UIImagePickerController()
    var verifyUserVM = VerifyUserViewModel()
    var typeImage:String?
    var delegate:VerifyAccountViewController?
    var uploaded_pic = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    }
    
    @IBAction func actionTakePhoto(_ sender: Any) {
        typeImage = "photo"
        self.imagePicker.sourceType = .photoLibrary
        //self.imagePicker.cameraDevice = .rear
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    @IBAction func actionUploadPhoto(_ sender: Any) {
        typeImage = "photo"
        if let img = img_photo.image {
            verifyImageAPIcalling(image: img)
        }else{
            Enough.shared.showOnlyAlert(message: "Please take a photo first.", viewController: self)
        }
    }
    @IBAction func actionTakeSelfie(_ sender: Any) {
        typeImage = "selfie"
        self.imagePicker.sourceType = .photoLibrary
        //self.imagePicker.cameraDevice = .front
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    @IBAction func actionUploadSelfie(_ sender: Any) {
        typeImage = "selfie"
        if let img = img_selfie.image {
            verifyImageAPIcalling(image: img)
        }else{
            Enough.shared.showOnlyAlert(message: "Please take a selfie first.", viewController: self)
        }
    }
    
    @IBAction func actionCross(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension VerifyIdentityPopupViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            if let data = img.jpegData(compressionQuality: 0.5){
               let _ = data.count/1024
                if typeImage! == "photo"{
                    img_photo.image = img
                }else if typeImage! == "selfie"{
                    img_selfie.image = img
                }
                picker.dismiss(animated: true, completion: nil)
                return
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
        func verifyImageAPIcalling(image: UIImage) {
            var param:[String:String] = [:]
            var keyValue:String = ""
            let data = image.jpegData(compressionQuality: 0.5)!
            if typeImage! == "photo"{
               param = ["user_id":Enough.shared.loginUser.id,"selfie_image":""]
                keyValue = "id_image"
            }else if typeImage! == "selfie"{
               param = ["user_id":Enough.shared.loginUser.id,"id_image":""]
                keyValue = "selfie_image"
            }
            verifyUserVM.photoVerification(parameter: param, key:keyValue,pathImage:data,viewController: self) { (result) in
                DispatchQueue.main.async {
                   if result["status"]?.int == 0{
                        Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                    }else{
                    self.uploaded_pic.append(self.typeImage!)
                    Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: "\(self.typeImage!) \(Alert_Msg.photoUploaded_Success)", viewController: self) {
                            self.delegate!.checkStatus()
                        if self.uploaded_pic.contains("selfie") && self.uploaded_pic.contains("photo"){
                            self.dismiss(animated: true, completion: nil)
                        }
                        }
                    }
                }
            }
        }
}
