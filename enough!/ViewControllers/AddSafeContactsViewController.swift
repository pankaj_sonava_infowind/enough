//
//  AddSafeContactsViewController.swift
//  enough!
//
//  Created by mac on 08/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol ContactAdded {
    func contactAdded()
}
class AddSafeContactsViewController: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_phonNumber: UITextField!
    @IBOutlet weak var txt_lastName: UITextField!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var view_dosent_account: UIView!
    @IBOutlet weak var lbl_foundName: UILabel!
    @IBOutlet weak var view_successPopUp: UIView!
    @IBOutlet weak var btn_username: UIButton!
    @IBOutlet weak var btn_email: UIButton!
    @IBOutlet weak var btn_phoneNumber: UIButton!
    @IBOutlet weak var view_searchBY: UIView!
    @IBOutlet weak var txt_userInput: UITextField!
    @IBOutlet var view_forSearch: UIView!
    @IBOutlet weak var view_error: UIView!
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var lbl_errorDes: UILabel!
    
    @IBOutlet weak var btn_sendReq: UIButton!
    @IBOutlet weak var lbl_dropDownHeader: UILabel!
    @IBOutlet var view_dropDown: UIView!
    @IBOutlet weak var view_relationship: UIView!
    @IBOutlet weak var view_alreadyAcc: UIView!
    @IBOutlet var tbl_dropDown: UITableView!
    @IBOutlet weak var lbl_acc_type: UILabel!
    @IBOutlet weak var lbl_relation: UILabel!
    var selectedLabel = UILabel()
    var selectedValue = ""
    let enougheAccArray = [EnoughAccount.already_acc,EnoughAccount.hasNot_acc]
    let relationArray = [Relationships.father,Relationships.mother,Relationships.huby,Relationships.wife,Relationships.bro,Relationships.sis,Relationships.bro_inLaw,Relationships.sis_inLaw,Relationships.uncle,Relationships.aunt,Relationships.cousin,Relationships.friend,Relationships.other]
    var dropDownValues = [String]()
    var otherRelatoin = ""
    let addContactVM = AddContactViewModel()
    var searchField = "phone"
    var searchedUser:User!
    var btnSearchOption = UIButton()
    var selectedRelation = ""
    var contactDelegate:ContactAdded?
    var has_acc = true
    override func viewDidLoad() {
        super.viewDidLoad()
        view_dropDown.frame = view.frame
        view_forSearch.frame = view.frame
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func selectAccStatusBtnAction(_ sender: UIButton) {
        lbl_dropDownHeader.text = "Account"
        dropDownValues = enougheAccArray
        tbl_dropDown.reloadData()
        selectedLabel = lbl_acc_type
        view.addSubview(view_dropDown)
    }
    @IBAction func selectRelationBtnAction(_ sender: UIButton) {
        lbl_dropDownHeader.text = "Relationships"
        dropDownValues = relationArray
        tbl_dropDown.reloadData()
        selectedLabel = lbl_relation
        view.addSubview(view_dropDown)
    }
    @IBAction func selectDropDownBtnAction(_ sender: Any) {
        if selectedValue == Relationships.other{
            if otherRelatoin.isEmpty{
                Enough.shared.showOnlyAlert(message: Error_Alert.Missing_Relation, viewController: self)
                return
            }
            selectedRelation = otherRelatoin
            selectedLabel.text = selectedValue + otherRelatoin
        }else{
            selectedLabel.text = selectedValue
            selectedRelation = lbl_relation.text!
        }
        view_dropDown.removeFromSuperview()
        if lbl_relation.text! != "Relationship"{
            view_searchBY.isHidden = false
            if lbl_acc_type.text! == EnoughAccount.hasNot_acc{
                has_acc = false
                view_dosent_account.isHidden = false
            }else if selectedValue == EnoughAccount.already_acc{
                has_acc = true
                view_dosent_account.isHidden = true
            }
        }
        otherRelatoin = ""
        selectedValue = ""
    }
    @IBAction func closeDropDownView(_ sender: Any) {
        view_dropDown.removeFromSuperview()
    }
    @IBAction func phoneNumberBtnAction(_ sender: UIButton) {
        btnSearchOption = sender
        searchField = "phone"
        txt_userInput.placeholder = "Type phone number"
        view.addSubview(view_forSearch)
    }
    @IBAction func emailBtnAction(_ sender: UIButton) {
        btnSearchOption = sender
        searchField = "email"
        txt_userInput.placeholder = "Type email"
        view.addSubview(view_forSearch)
    }
    @IBAction func usernameBtnAction(_ sender: UIButton) {
        btnSearchOption = sender
        searchField = "username"
        txt_userInput.placeholder = "Type username / alias"
        view.addSubview(view_forSearch)
    }
    @IBAction func okErrorBtnAction(_ sender: Any) {
        txt_userInput.text = ""
        view_error.isHidden = true
    }
    @IBAction func closeSearchViewBtnAction(_ sender: Any) {
        view_forSearch.removeFromSuperview()
        if !view_successPopUp.isHidden{
            view_successPopUp.isHidden = true
            contactDelegate?.contactAdded()
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func sendReqBtnAction(_ sender: UIButton) {
        if btn_sendReq.isSelected {
            var param = [String:String]()
            if has_acc{
                param = ["user_id":Enough.shared.loginUser.id,"firstname":"","lastname":"","contact_email":searchedUser.email,
                "contactphone":searchedUser.mobile,"type":"1","relationship":selectedRelation,"contact_id":searchedUser.id]
            }else{
                param = ["user_id":Enough.shared.loginUser.id,"firstname":txt_name.text!,"lastname":txt_lastName.text!,"contact_email":txt_email.text!,
                         "contactphone":txt_phonNumber.text!,"type":"1","relationship":selectedRelation,"contact_id":"0"]
            }
            addContactVM.addSafeContact(viewController: self, parameter: param) { (result) in
                DispatchQueue.main.async {
                    if result["status"]?.int == 0{
                        Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Error_Alert.Contact_NotAddedd, viewController: self)
                    }else{
                        if self.has_acc{
                            self.view_successPopUp.isHidden = false
                            self.lbl_foundName.text = self.searchedContent
                            self.view.addSubview(self.view_forSearch)
                        }else{
                            Enough.shared.showAlertOneAction(title: "Sent", okayTitle: Alert_Msg.Ok, message: "Invitation has been sent to \(self.txt_name.text!) \(self.txt_lastName.text!)", viewController: self) {
                                self.contactDelegate?.contactAdded()
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    @IBAction func okFinalAlert(_ sender: UIButton) {
        view_forSearch.removeFromSuperview()
        view_successPopUp.isHidden = true
        contactDelegate?.contactAdded()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        if txt_userInput.text!.isEmpty{
            Enough.shared.showOnlyAlert(message: "Please enter text.", viewController: self)
            return
        }
        let param = ["search_contact":txt_userInput.text!,"user_id":Enough.shared.loginUser.id]
        addContactVM.searchUser(viewController: self, parameter: param) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 0{
                    self.setErrorAlert()
                }else{
                    if let data = result["data"]?.dictionary{
                        self.setSearchResult(data: data)
                    }
                }
            }
        }
    }
    func setErrorAlert() {
        view_error.isHidden = false
        if searchField == "phone"{
            lbl_error.text = "This phone number doesn't exist."
        }else if searchField == "email"{
            lbl_error.text = "This email address doesn't exist."
        }else{
            lbl_error.text = "This username doesn't exist."
        }
    }
    var searchedContent = ""
    func setSearchResult(data:[String:JSON]) {
        btn_phoneNumber.backgroundColor = UIColor.white
        btn_email.backgroundColor = UIColor.white
        btn_username.backgroundColor = UIColor.white
        
        btnSearchOption.backgroundColor = UIColor(named: "lightPink")
        btn_sendReq.backgroundColor = UIColor(named: "lightPurple")
        btn_sendReq.isSelected = true
        let userData = Enough.shared.getCoreDic(jsonDic: data)
        searchedUser = User.init(dic: userData)
        if searchField == "phone"{
            btnSearchOption.setAttributedTitle(Enough.shared.getMultipleFontText(key: "Phone:", value: " \(searchedUser.mobile)"), for: .normal)
            searchedContent = searchedUser.mobile
        }else if searchField == "email"{
            btnSearchOption.setAttributedTitle(Enough.shared.getMultipleFontText(key: "Email:", value: " \(searchedUser.email)"), for: .normal)
            searchedContent = searchedUser.email
        }else{
            btnSearchOption.setAttributedTitle(Enough.shared.getMultipleFontText(key: "Username:", value: " \(searchedUser.username)"), for: .normal)
            searchedContent = searchedUser.username
        }
        txt_userInput.text = ""
        view_forSearch.removeFromSuperview()
    }
    // Support Method
    func isValidUserEntry() -> (Bool,String) {
        if txt_name.text!.isEmpty{
            return (false,Error_Alert.Missing_Username)
        }
        if txt_lastName.text!.isEmpty {
            return (false,Error_Alert.Missing_Lastname)
        }
        if txt_phonNumber.text!.isEmpty {
            return (false,Error_Alert.Missing_Mobile)
        }
        if txt_email.text!.isEmpty{
            return (false,Error_Alert.Missing_Email)
        }
        if !Enough.shared.isValidEmail(txt_email.text!){
            return (false,Error_Alert.Invalid_Email)
        }
        return (true,"")
    }
}
extension AddSafeContactsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell") as? ContactDropDownTVC
        cell?.selectionStyle = .none
        cell?.lbl_relation.text  = dropDownValues[indexPath.row]
        cell?.txt_other.delegate = self
        if selectedValue == dropDownValues[indexPath.row]{
            cell?.backgroundColor = UIColor(named: "lightPink")//
            cell?.lbl_relation?.textColor = UIColor(named: "darkPurple")
            if selectedValue == Relationships.other{
                cell?.txt_other.becomeFirstResponder()
            }
        }else{
            cell?.backgroundColor = UIColor.white
            cell?.lbl_relation?.textColor = UIColor.black
        }
        if dropDownValues[indexPath.row] == Relationships.other{
            cell?.txt_other.isHidden = false
            cell?.txt_other.text = otherRelatoin
        }else{
            cell?.txt_other.isHidden = true
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedValue = dropDownValues[indexPath.row]
        tbl_dropDown.reloadData()
    }
}
extension AddSafeContactsViewController:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == txt_name || textField == txt_lastName || textField == txt_phonNumber || textField == txt_email {
            let valid = isValidUserEntry()
            if valid.0{
                btn_sendReq.backgroundColor = UIColor(named: "lightPurple")
                btn_sendReq.isSelected = true
            }
        }else{
            otherRelatoin = textField.text!
        }
    }
}
         
