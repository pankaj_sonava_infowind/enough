//
//  MissingPersonDetailViewController.swift
//  enough!
//
//  Created by mac on 10/08/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class MissingPersonDetailViewController: UIViewController {

    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var lblFName: UILabel!
    @IBOutlet weak var lblLName: UILabel!
    @IBOutlet weak var lblMissingDate: UILabel!
    @IBOutlet weak var lblMissingPlace: UILabel!
    @IBOutlet weak var lblMissingAge: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblHairColor: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHairLength: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblHairStyle: UILabel!
    @IBOutlet weak var lblEyes: UILabel!
    @IBOutlet weak var lblEthnicity: UILabel!
    @IBOutlet weak var lblGlasses: UILabel!
    @IBOutlet weak var lblBuild: UILabel!
    @IBOutlet weak var textVUniqueSign: UITextView!
    @IBOutlet weak var textVSpecialNeeds: UITextView!
    
    var person:Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues()
    }
    func setValues(){
        lblFName.text = person.firstname
        lblLName.text = person.lastname
       // lblMissingDate.text = person.dob
        //lblMissingPlace.text = person.
        //lblMissingAge.text = person.ag
        lblHeight.text = person.height
        lblHairColor.text = person.haircolor
        lblWeight.text = person.weight
        lblHairLength.text = person.hairlength
        lblGender.text = person.gender
        lblHairStyle.text = person.hairstyle
        lblEyes.text = person.eyes
        lblEthnicity.text = person.ethnicity
        lblGlasses.text = person.glasses
        lblBuild.text = person.build
        textVUniqueSign.text = person.unique_sign
        textVSpecialNeeds.text = person.special_health_condition
        var img_url = ApiManager.base_url + person!.image_path!
                   if let range = img_url.range(of: "./../"){
                       img_url.removeSubrange(range)
                   }
                   personImage.sd_setImage(with: URL(string: img_url)) { (image, error, cacheType, url) in
                       if let img = image{
                           self.personImage.image = img
                       }
                   }
       }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
