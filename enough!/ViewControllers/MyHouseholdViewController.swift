//
//  MyHouseholdViewController.swift
//  enough!
//
//  Created by mac on 08/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class MyHouseholdViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPopoverPresentationControllerDelegate,HouseholdProtocol {
    
    @IBOutlet weak var tblVMyHousehold: UITableView!
    @IBOutlet weak var tooltip: UIView!
    var q1 :Bool = false
    let addHouseholdVM = AddHouseholdViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getHouseholdList()
        tooltip.isHidden = true
    }
    
    func getHouseholdList()  {
        self.addHouseholdVM.allHouseholdList.removeAll()
        addHouseholdVM.getHouseholdList(viewController: self) {
            DispatchQueue.main.async {
                self.tblVMyHousehold.reloadData()
            }
        }
    }
    
    @IBAction func actionQues(_ sender: Any) {
        if q1 == false{
            tooltip.isHidden = false
            q1 = true
        }else{
            tooltip.isHidden = true
            q1 = false
        }
    }
    
    @IBAction func actoinBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionAddMember(_ sender: Any) {
        let EditAddHouseholdPopUp = self.storyboard?.instantiateViewController(withIdentifier: "EditAddHouseholdPopupViewController") as! EditAddHouseholdPopupViewController
        EditAddHouseholdPopUp.modalPresentationStyle = .overCurrentContext
        EditAddHouseholdPopUp.modalTransitionStyle = .crossDissolve
        EditAddHouseholdPopUp.typeScreen = "Add"
        EditAddHouseholdPopUp.addHouseholdVM = self.addHouseholdVM
        EditAddHouseholdPopUp.delegate = self
        self.present(EditAddHouseholdPopUp, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addHouseholdVM.allHouseholdList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVMyHousehold.dequeueReusableCell(withIdentifier: "MySafeContactsCell") as! MyContactsTableViewCell
        cell.lblName.text! = addHouseholdVM.allHouseholdList[indexPath.row].firstname!
        cell.btnEdit.addTarget(self, action: #selector(editHousehold(sender:)), for: .touchUpInside)
        cell.btnCancel.addTarget(self, action: #selector(deleteHousehold(sender:)), for: .touchUpInside)
        cell.btnEdit.tag = indexPath.row
        cell.btnCancel.tag = indexPath.row
        return cell
    }
    
    @objc func editHousehold(sender: UIButton){
        let index = sender.tag
        let EditAddHouseholdPopUp = self.storyboard?.instantiateViewController(withIdentifier: "EditAddHouseholdPopupViewController") as! EditAddHouseholdPopupViewController
        EditAddHouseholdPopUp.modalPresentationStyle = .overCurrentContext
        EditAddHouseholdPopUp.modalTransitionStyle = .crossDissolve
        EditAddHouseholdPopUp.typeScreen = "Edit"
        EditAddHouseholdPopUp.id = addHouseholdVM.allHouseholdList[index].id
        EditAddHouseholdPopUp.index = index
        EditAddHouseholdPopUp.addHouseholdVM = self.addHouseholdVM
        EditAddHouseholdPopUp.delegate = self
        self.present(EditAddHouseholdPopUp, animated: true, completion: nil)
    }
    
    @objc func deleteHousehold(sender: UIButton){
        let index = sender.tag
        let DeleteHouseholdPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "DeleteHouseholdPopupViewController") as! DeleteHouseholdPopupViewController
        DeleteHouseholdPopupVC.modalPresentationStyle = .overCurrentContext
        DeleteHouseholdPopupVC.modalTransitionStyle = .crossDissolve
        DeleteHouseholdPopupVC.id = addHouseholdVM.allHouseholdList[index].id
        DeleteHouseholdPopupVC.addHouseholdVM = self.addHouseholdVM
        DeleteHouseholdPopupVC.delegate = self
        DeleteHouseholdPopupVC.index = index
        self.present(DeleteHouseholdPopupVC, animated: true, completion: nil)
    }
    
    
}
