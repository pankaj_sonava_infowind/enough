//
//  SettingsViewController.swift
//  enough!
//
//  Created by mac on 07/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SDWebImage

class SettingsViewController: UIViewController {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAlias: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblEnough: UILabel!
    
    let settingVM = SettingsViewModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setUserData()
        let att = NSMutableAttributedString(string: "Join The Good Ones Community");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightPink, range: NSRange(location: 0, length: 4))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkPink, range: NSRange(location: 5, length: 13))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightPink, range: NSRange(location: 15, length:  13))
        //btnJoinCommunity.setAttributedTitle(att, for: [])
        var img_url = ApiManager.base_url + Enough.shared.loginUser.profile_image!
        if let range = img_url.range(of: "./../"){
            img_url.removeSubrange(range)
        }
        imgUser.sd_setImage(with: URL(string: img_url)) { (image, error, cacheType, url) in
            if let img = image{
                self.imgUser.image = img
            }
        }
    }
    func setUserData(){
           lblName.text = Enough.shared.loginUser.name!
           lblAlias.text = Enough.shared.loginUser.alias!
           lblUserName.text = Enough.shared.loginUser.username
           lblEmail.text = Enough.shared.loginUser.email
           //lblEnough.text = Enough.shared.loginUser.!
       }
    @IBAction func actionEditImgProfile(_ sender: Any) {
        let VerifyPhotoPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyPhotoPopupViewController") as! VerifyPhotoPopupViewController
        VerifyPhotoPopupVC.delegate = self
        VerifyPhotoPopupVC.modalPresentationStyle = .overCurrentContext
        VerifyPhotoPopupVC.modalTransitionStyle = .crossDissolve
        self.present(VerifyPhotoPopupVC, animated: true, completion: nil)
    }
    @IBAction func actionVerifyAccount(_ sender: Any) {
        let VerifyAccountVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyAccountViewController") as! VerifyAccountViewController
        VerifyAccountVC.modalPresentationStyle = .overCurrentContext
        VerifyAccountVC.modalTransitionStyle = .crossDissolve
        //VerifyAccountVC.delegate = self
        self.present(VerifyAccountVC, animated: true, completion: nil)
    }
    
    @IBAction func actionEdit(_ sender: Any) {
        let MyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
               self.navigationController?.pushViewController(MyProfileVC, animated: true)
    }
    
    @IBAction func actionMyProfile(_ sender: Any) {
        let MyProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        self.navigationController?.pushViewController(MyProfileVC, animated: true)
    }
    
    @IBAction func actionMyCodes(_ sender: Any) {
        let MyCodesVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCodesViewController") as! MyCodesViewController
        self.navigationController?.pushViewController(MyCodesVC, animated: true)
    }
    
    @IBAction func actionMyContacts(_ sender: Any) {
    }
    
     @IBAction func actionMyHousehold(_ sender: Any) {
           let MyHouseholdVC = self.storyboard?.instantiateViewController(withIdentifier: "MyHouseholdViewController") as! MyHouseholdViewController
           self.navigationController?.pushViewController(MyHouseholdVC, animated: true)
       }
       
       @IBAction func actionMyMissingPersonInfo(_ sender: Any) {
           let MyRecordsVC = self.storyboard?.instantiateViewController(withIdentifier: "MyRecordsViewController") as! MyRecordsViewController
           self.navigationController?.pushViewController(MyRecordsVC, animated: true)
       }
    
    @IBAction func actionMyAudioTriggers(_ sender: Any) {
        let MyVoiceTriggerVC = self.storyboard?.instantiateViewController(withIdentifier: "MyVoiceTriggerViewController") as! MyVoiceTriggerViewController
        self.navigationController?.pushViewController(MyVoiceTriggerVC, animated: true)
    }
    
    @IBAction func actionMyAlertsSettings(_ sender: Any) {
        let MyAlertSettingsVC = self.storyboard?.instantiateViewController(withIdentifier: "MyAlertSettingsViewController") as! MyAlertSettingsViewController
               self.navigationController?.pushViewController(MyAlertSettingsVC, animated: true)
    }
    
    @IBAction func actionMytriggerModes(_ sender: Any) {
    }
    
    @IBAction func actionMyTextAlerts(_ sender: Any) {
    }
    
    @IBAction func actionMyEmergencyNumbers(_ sender: Any) {
    }
    
    @IBAction func actionAlertsNotifications(_ sender: Any) {
    }
    
    @IBAction func actionMyAlertHistry(_ sender: Any) {
    }
    
    @IBAction func actionJoinCommunity(_ sender: Any) {
    }
    
    @IBAction func actionEnoughLbl(_ sender: Any) {
    }
    
    @IBAction func actionJoinSkyNetwork(_ sender: Any) {
    }
    
    @IBAction func actionMakeDonation(_ sender: Any) {
    }
    
    @IBAction func actionRate(_ sender: Any) {
    }
    
    @IBAction func actionShare(_ sender: Any) {
    }
    
    @IBAction func actionTwitter(_ sender: Any) {
    }
    
    @IBAction func actionFacebook(_ sender: Any) {
    }
    
    @IBAction func actionEnoughInitiative(_ sender: Any) {
    }
    
    @IBAction func actionPrivacySecurity(_ sender: Any) {
    }
    
    @IBAction func actionDataUsageStorage(_ sender: Any) {
    }
    @IBAction func actionHelpFeedback(_ sender: Any) {
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
extension SettingsViewController:ProfilePicAction{
    func selectImage(image: UIImage) {
        imgUser.image = image
        let data = image.jpegData(compressionQuality: 0.5)!
        settingVM.uploadProPic(picData: data, viewController: self) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 0{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                }else{
                    if var loginData = UserDefaults.standard.value(forKey: "Login_Data") as? [String:String]{
                        loginData["profile_image"] = result["img_path"]?.string ?? ""
                    }
                    Enough.shared.loginUser.profile_image = result["img_path"]?.string ?? ""
                    Enough.shared.showOnlyAlert(message: Alert_Msg.Pic_Updatted, viewController: self)
                }
            }
        }
    }
}
