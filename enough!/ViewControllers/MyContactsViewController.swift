//
//  MyContactsViewController.swift
//  enough!
//
//  Created by mac on 06/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol SafeContactUpdate {
    func updateHome()
}
class MyContactsViewController: UIViewController {
  
    @IBOutlet weak var tblVSafeContact: UITableView!
    // Add safe contact
    @IBOutlet var view_addFearContact: UIView!
    @IBOutlet weak var lbl_relatoinship: UILabel!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_lastName: UITextField!
    @IBOutlet weak var txt_phonNumber: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var tooltipsView: UITextView!
    @IBOutlet weak var tooltipView2: UITextView!
    
    var addContactVM = AddContactViewModel()
    var verifyUserVM = VerifyUserViewModel()
    var delegate:AlertsViewController?
    var q1:Bool = false
    var q2:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        getSafeContact()
        view_addFearContact.frame = view.frame
        txt_phonNumber.addPadding(padding: .equalSpacing(5))
        txt_email.addPadding(padding: .equalSpacing(5))
        txt_name.addPadding(padding: .equalSpacing(5))
        txt_lastName.addPadding(padding: .equalSpacing(5))
        tooltipsView.isHidden = true
        tooltipView2.isHidden = true
    }
    func getSafeContact()  {
        addContactVM.getSafeContacts(viewController: self) {
            DispatchQueue.main.async {
                self.tblVSafeContact.reloadData()
                if self.addContactVM.safeContacts.count == 0{
                    self.updateHomeSteps(value: "")
                }else{
                    self.updateHomeSteps(value: "1")
                }
                self.getFeareContact()
            }
        }
    }
    func getFeareContact()  {
        addContactVM.getFearContacts(viewController: self) {
            DispatchQueue.main.async {
                self.tblVSafeContact.reloadData()
            }
        }
    }
    func updateHomeSteps(value:String) {
           let param = ["user_id":Enough.shared.loginUser.id,"add_safe_contacts":value]
        print("param:",param)
           Enough.shared.enough_status.acc_veryfied = true
           verifyUserVM.updateHomeSteps(viewController: self, param: param){ (result) in
               if result["status"]?.int == 1{
                   if var homeStep = UserDefaults.standard.value(forKey: "HomeSteps") as? [String:String]{
                       homeStep["add_safe_contacts"] = value
                       UserDefaults.standard.set(homeStep, forKey: "HomeSteps")
                    self.delegate!.updateHome()
                   }
               }
           }
       }
    @objc func actionAddSafeContacts(_ sender: UIButton) {
        let AddSafeContactsVC = storyboard?.instantiateViewController(withIdentifier: "AddSafeContactsViewController") as! AddSafeContactsViewController
        AddSafeContactsVC.contactDelegate = self
        navigationController?.pushViewController(AddSafeContactsVC, animated: true)
    }
    @objc func actionAddFearContacts(_ sender: UIButton) {
        view.addSubview(view_addFearContact)
    }
    
    @IBAction func selectRelatoinBtnAction(_ sender: UIButton) {
        let relationView = RelationView(frame: view.bounds)
        relationView.dropDownValues = relationArray
        relationView.tbl_list.reloadData()
        relationView.selectedCell_bg = UIColor(named: "lightRed")
        relationView.selectedCell_text = UIColor(named: "darkRed")
        relationView.btn_ok.backgroundColor = UIColor(named: "darkRed")
        relationView.delegate = self
        view.addSubview(relationView)
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func saveFearContactBtnAction(_ sender: UIButton) {
        let param = ["user_id":Enough.shared.loginUser.id,"firstname":txt_name.text!,"lastname":txt_lastName.text!,"contact_email":txt_email.text!,
                     "contactphone":txt_phonNumber.text!,"type":"0","relationship":lbl_relatoinship.text!]
        addContactVM.addFearContact(viewController: self, parameter: param) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 0{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Error_Alert.Contact_NotAddedd, viewController: self)
                }else{
                    self.getFeareContact()
                    self.view_addFearContact.removeFromSuperview()
                }
            }
        }
    }
    @IBAction func cancelAddFearBtnAction(_ sender: UIButton) {
        view_addFearContact.removeFromSuperview()
    }
}
extension MyContactsViewController:RelationViewActions{
    func croBtnClick(sender: UIButton) {
        
    }
    func okBtnClick(sender: UIButton) {
        let selectedValue = sender.layer.value(forKey: "SelectedValue") as! String
        lbl_relatoinship.text = selectedValue
        let valid = isValidUserEntry()
        if valid.0{
            btnSave.backgroundColor = UIColor(named: "darkRed")
            btnSave.isSelected = true
        }
    }
}
extension MyContactsViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return addContactVM.safeContacts.count
        }else {
            return addContactVM.fearContacts.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MySafeContactsCell") as! MyContactsTableViewCell
        if indexPath.section == 0{
            cell.contactDetails = addContactVM.safeContacts[indexPath.row]
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(deleteContact(sender:)), for: .touchUpInside)
        }else{
            cell.contactDetails = addContactVM.fearContacts[indexPath.row]
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(deleteContact(sender:)), for: .touchUpInside)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableCell(withIdentifier: "ContactSectionTVC") as! ContactSectionTVC
        if section == 0{
            view.lbl_safeContact.text  = "My safe conntacts"
            view.btn_addContact.setTitle("Add safe contact  ", for: .normal)
            view.btn_addContact.addTarget(self, action: #selector(actionAddSafeContacts(_:)), for: .touchUpInside)
            view.btn_contact.addTarget(self, action: #selector(safeContactReq), for: .touchUpInside)
            view.btn_toolTip.addTarget(self, action: #selector(showTooltips), for: .touchUpInside)
        }else{
            view.lbl_safeContact.text  = "My fear conntacts"
            view.btn_addContact.setTitle("Add fear contact  ", for: .normal)
            view.btn_addContact.addTarget(self, action: #selector(actionAddFearContacts(_:)), for: .touchUpInside)
            view.btn_toolTip.addTarget(self, action: #selector(showTooltips2), for: .touchUpInside)
        }
        view.btn_addContact.titleLabel?.underline()
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    @objc func deleteContact(sender:UIButton) {
        let infoPopUp = InfoPopUpView(frame: view.bounds)
        infoPopUp.btn_ok = sender
        infoPopUp.btn_cross = sender
        infoPopUp.lbl_info.text = Alert_Msg.Confirm_Delete_Contact
        infoPopUp.delegate = self
        view.addSubview(infoPopUp)
    }
    @objc func safeContactReq() {
        let reqVC = storyboard?.instantiateViewController(withIdentifier: "RequestListViewController") as! RequestListViewController
        navigationController?.pushViewController(reqVC, animated: true)
    }
    @objc func showTooltips() {
        tooltipView2.isHidden = true
        q2 = false
        if q1 == false{
            tooltipsView.isHidden = false
            q1 = true
        }else{
            tooltipsView.isHidden = true
            q1 = false
        }
    }
    @objc func showTooltips2() {
        tooltipsView.isHidden = true
        q1 = false
           if q2 == false{
               tooltipView2.isHidden = false
               q2 = true
           }else{
               tooltipView2.isHidden = true
               q2 = false
           }
       }
    // Support Method
    func isValidUserEntry() -> (Bool,String) {
        var isValid = true
        var msg = ""
        if lbl_relatoinship.text! == "Select relationship" {
            msg = Error_Alert.Miss_Relation
            isValid = false
        }
        if txt_name.text!.isEmpty{
            msg = Error_Alert.Missing_Username
            isValid = false
        }
        if txt_lastName.text!.isEmpty {
            msg = Error_Alert.Missing_Lastname
            isValid = false
        }
        if txt_phonNumber.text!.isEmpty {
            msg = Error_Alert.Missing_Mobile
            isValid = false
        }
        if txt_email.text!.isEmpty{
            msg = Error_Alert.Missing_Email
            isValid = false
        }
        if !Enough.shared.isValidEmail(txt_email.text!){
            msg = Error_Alert.Invalid_Email
            isValid = false
        }
        if isValid{
            btnSave.backgroundColor = UIColor(named: "darkRed")
            btnSave.isSelected = true
        }else{
            btnSave.backgroundColor = UIColor.lightGray
            btnSave.isSelected = false
        }
        return (isValid,msg)
    }
}
extension MyContactsViewController: InfoPopUpActions{
    func crossBtnAction(sender: UIButton) {
        
    }
    
    func okBtnAction(sender: UIButton) {
        let contact_id = sender.layer.value(forKey: "contact_id") as! String
        addContactVM.deleteContact(contact_id: contact_id, viewController: self) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 0{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                }else{
                    self.removeContact(index: sender.tag)
                    
                }
            }
        }
    }
    func removeContact(index:Int) {
        self.addContactVM.safeContacts.remove(at: index)
        if self.addContactVM.safeContacts.count == 0{
            self.updateHomeSteps(value: "")
        }
        self.tblVSafeContact.reloadData()
    }
}
extension MyContactsViewController:ContactAdded{
    func contactAdded() {
        
    }
}
extension MyContactsViewController:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == txt_name || textField == txt_lastName || textField == txt_phonNumber || textField == txt_email {
            _ = isValidUserEntry()
            
        }
    }
}
