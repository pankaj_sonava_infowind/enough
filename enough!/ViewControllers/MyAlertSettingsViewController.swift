//
//  MyAlertSettingsViewController.swift
//  enough!
//
//  Created by mac on 08/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class MyAlertSettingsViewController: UIViewController {
    
    @IBOutlet weak var btnLiveFeed: UIButton!
    @IBOutlet weak var btnVideoCloud: UIButton!
    @IBOutlet weak var btnAudioCloud: UIButton!
    @IBOutlet weak var btnCallSafeContacts: UIButton!
    @IBOutlet weak var btnSendMessages: UIButton!
    @IBOutlet weak var btnCallAuthority: UIButton!
    @IBOutlet weak var btnAuthorityNumber: UIButton!
    @IBOutlet weak var btnGroupCall: UIButton!
    @IBOutlet weak var btnGroupChat: UIButton!
    @IBOutlet weak var btnNotifyTheCommunity: UIButton!
    @IBOutlet weak var btnShareLiveFeed: UIButton!
    @IBOutlet weak var btnShareLocation: UIButton!
    @IBOutlet weak var btnShareAlertUpdate: UIButton!
    
    var liveFeed:String = "1"
    var videoCloud:String = "1"
    var audioCloud:String = "1"
    var callSafeContacts:String = "1"
    var sendMessages:String = "1"
    var callAuthority:String = "1"
    var authorityNumber:String = "1"
    var groupCall:String = "1"
    var groupChat:String = "1"
    var notyfyCommunity:String = "1"
    var shareLiveFeed:String = "1"
    var shareLocation:String = "1"
    var shareAlertUpdate:String = "1"
    var alertSettingVM = AlertSettingViewModel()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserAlert()
        
    }
    
    func getUserAlert(){
        alertSettingVM.getUserAlertSetting(viewController: self) {
            print(self.alertSettingVM.getAlertSettingArr)
            self.liveFeed = self.alertSettingVM.getAlertSettingArr["live_feed"]!
            self.videoCloud = self.alertSettingVM.getAlertSettingArr["video_cloud_recording"]!
            self.audioCloud = self.alertSettingVM.getAlertSettingArr["audio_cloud_recording"]!
            self.callSafeContacts = self.alertSettingVM.getAlertSettingArr["call_safe_contacts"]!
            self.sendMessages = self.alertSettingVM.getAlertSettingArr["send_msg_safe_contacts"]!
            self.callAuthority = self.alertSettingVM.getAlertSettingArr["call_authority"]!
            self.authorityNumber = self.alertSettingVM.getAlertSettingArr["authority_number"]!
            self.groupCall = self.alertSettingVM.getAlertSettingArr["groupcall"]!
            self.groupChat = self.alertSettingVM.getAlertSettingArr["groupchat"]!
            self.notyfyCommunity = self.alertSettingVM.getAlertSettingArr["notify_community"]!
            self.shareLiveFeed = self.alertSettingVM.getAlertSettingArr["share_feed_for_community"]!
            self.shareLocation = self.alertSettingVM.getAlertSettingArr["share_location_for_community"]!
            self.shareAlertUpdate = self.alertSettingVM.getAlertSettingArr["share_alert_updates_for_community"]!
            
            DispatchQueue.main.async {
                if self.liveFeed == ""{
                    self.btnLiveFeed.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnLiveFeed.setImage(UIImage(named: "live"), for: [])
                }
                if self.videoCloud == ""{
                    self.btnVideoCloud.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnVideoCloud.setImage(UIImage(named: "live"), for: [])
                }
                if self.audioCloud == ""{
                    self.btnAudioCloud.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnAudioCloud.setImage(UIImage(named: "live"), for: [])
                }
                if self.callSafeContacts == ""{
                    self.btnCallSafeContacts.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnCallSafeContacts.setImage(UIImage(named: "live"), for: [])
                }
                if self.sendMessages == ""{
                    self.btnSendMessages.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnSendMessages.setImage(UIImage(named: "live"), for: [])
                }
                if self.callAuthority == ""{
                    self.btnCallAuthority.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnCallAuthority.setImage(UIImage(named: "live"), for: [])
                }
                if self.authorityNumber == ""{
                    self.btnAuthorityNumber.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnAuthorityNumber.setImage(UIImage(named: "live"), for: [])
                }
                if self.groupCall == ""{
                    self.btnGroupCall.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnGroupCall.setImage(UIImage(named: "live"), for: [])
                }
                if self.groupChat == ""{
                    self.btnGroupChat.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnGroupChat.setImage(UIImage(named: "live"), for: [])
                }
                if self.notyfyCommunity == ""{
                    self.btnNotifyTheCommunity.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnNotifyTheCommunity.setImage(UIImage(named: "live"), for: [])
                }
                if self.shareLiveFeed == ""{
                    self.btnShareLiveFeed.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnShareLiveFeed.setImage(UIImage(named: "live"), for: [])
                }
                if self.shareLocation == ""{
                    self.btnShareLocation.setImage(UIImage(named: "off"), for: [])
                       }else{
                    self.btnShareLocation.setImage(UIImage(named: "live"), for: [])
                       }
                if self.shareAlertUpdate == ""{
                    self.btnShareAlertUpdate.setImage(UIImage(named: "off"), for: [])
                }else{
                    self.btnShareAlertUpdate.setImage(UIImage(named: "live"), for: [])
                }
            }
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionLiveFeedLiveOff(_ sender: Any) {
        if liveFeed == ""{
            btnLiveFeed.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "live_feed", value: "1")
            liveFeed = "1"
        }else{
            btnLiveFeed.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "live_feed", value: "0")
            liveFeed = ""
        }
    }

    @IBAction func actionVideoCloudLiveOff(_ sender: Any) {
        if videoCloud == ""{
            btnVideoCloud.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "video_cloud_recording", value: "1")
            videoCloud = "1"
        }else{
            btnVideoCloud.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "video_cloud_recording", value: "0")
            videoCloud = ""
        }
        
    }
    @IBAction func actionAudioCloudLiveOff(_ sender: Any) {
        if audioCloud == ""{
            btnAudioCloud.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "audio_cloud_recording", value: "1")
            audioCloud = "1"
        }else{
            btnAudioCloud.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "audio_cloud_recording", value: "0")
            audioCloud = ""
        }
    }
    
    @IBAction func actionCallSafeContactsLiveOff(_ sender: Any) {
        if callSafeContacts == ""{
            btnCallSafeContacts.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "call_safe_contacts", value: "1")
            callSafeContacts = "1"
        }else{
            btnCallSafeContacts.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "call_safe_contacts", value: "0")
            callSafeContacts = ""
        }
    }
    
    @IBAction func actionSendMessagesLiveOff(_ sender: Any) {
        if sendMessages == ""{
            btnSendMessages.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "send_msg_safe_contacts", value: "1")
            sendMessages = "1"
        }else{
            btnSendMessages.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "send_msg_safe_contacts", value: "0")
            sendMessages = ""
        }
    }
    
    @IBAction func actionCallAuthorityLiveOff(_ sender: Any) {
        if callAuthority == ""{
            btnCallAuthority.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "call_authority", value: "1")
            callAuthority = "1"
        }else{
            btnCallAuthority.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "call_authority", value: "0")
            callAuthority = ""
        }
    }
    @IBAction func actionAuthorityNumberLiveOff(_ sender: Any) {
        if authorityNumber == ""{
            btnAuthorityNumber.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "authority_number", value: "1")
            authorityNumber = "1"
        }else{
            btnAuthorityNumber.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "authority_number", value: "0")
            authorityNumber = ""
        }
    }
    
    @IBAction func actionGroupCallLiveOff(_ sender: Any) {
        if groupCall == ""{
            btnGroupCall.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "groupcall", value: "1")
            groupCall = "1"
        }else{
            btnGroupCall.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "groupcall", value: "0")
            groupCall = ""
        }
    }
    
    @IBAction func actionGroupChatLiveOff(_ sender: Any) {
        if groupChat == ""{
            btnGroupChat.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "groupchat", value: "1")
            groupChat = "1"
        }else{
            btnGroupChat.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "groupchat", value: "0")
            groupChat = ""
        }
    }
    
    @IBAction func actionNotifyCommunityLiveOff(_ sender: Any) {
        if notyfyCommunity == ""{
            btnNotifyTheCommunity.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "notify_community", value: "1")
            notyfyCommunity = "1"
        }else{
            btnNotifyTheCommunity.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "notify_community", value: "0")
            notyfyCommunity = ""
        }
    }
    
    @IBAction func actionShareLiveFeedLiveOff(_ sender: Any) {
        if shareLiveFeed == ""{
            btnShareLiveFeed.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "share_feed_for_community", value: "1")
            shareLiveFeed = "1"
        }else{
            btnShareLiveFeed.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "share_feed_for_community", value: "0")
            shareLiveFeed = ""
        }
    }
    
    @IBAction func actionShareLocationLiveOff(_ sender: Any) {
        if shareLocation == ""{
            btnShareLocation.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "share_location_for_community", value: "1")
            shareLocation = "1"
        }else{
            btnShareLocation.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "share_location_for_community", value: "0")
            shareLocation = ""
        }
    }
    
    @IBAction func actionShareAlertUpdateLiveOff(_ sender: Any) {
        if shareAlertUpdate == ""{
            btnShareAlertUpdate.setImage(UIImage(named: "live"), for: [])
            userAlertSettingAPICall(param: "share_alert_updates_for_community", value: "1")
            shareAlertUpdate = "1"
        }else{
            btnShareAlertUpdate.setImage(UIImage(named: "off"), for: [])
            userAlertSettingAPICall(param: "share_alert_updates_for_community", value: "0")
            shareAlertUpdate = ""
        }
    }
    
    func userAlertSettingAPICall(param:String,value:String){
        alertSettingVM.userAlertSetting(viewController: self, param: param, value: value) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                    print("updated succesfully")
                }else{
                    print("result",result)
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
}
