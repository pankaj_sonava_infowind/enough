//
//  SignUpViewController.swift
//  enough!
//
//  Created by mac on 01/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol RegistrationCompleted {
func userRegistered(emali:String,password:String)
}

class SignUpViewController: UIViewController {
    @IBOutlet weak var txt_userName: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_lastName: UITextField!
    @IBOutlet weak var txt_mobileNo: UITextField!
    @IBOutlet weak var lbl_gender: UILabel!
    @IBOutlet weak var lbl_dob: UILabel!
    @IBOutlet weak var lbl_country: UILabel!
    @IBOutlet var view_genderSelection: UIView!
    @IBOutlet weak var lbl_selectGender: UILabel!
    @IBOutlet weak var viewSelectProNown: UIView!
    @IBOutlet weak var txt_genderDesc: UITextField!
    @IBOutlet var tbl_dropDown: UITableView!
    @IBOutlet weak var lbl_pronown: UILabel!
    @IBOutlet var view_picker: UIView!
    @IBOutlet weak var date_pickerView: UIDatePicker!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var height_picker: NSLayoutConstraint!
    @IBOutlet weak var btn_countryCode: UIButton!
    
    // Class Variables
    var genderArray = ["Male","Female","Custom"]
    var customArray = ["She: 'Wish her a happy birthday!'","He: 'Wish him a happy birthday!'","They: 'Wish them a happy birthday!'"]
    var dropDownData = [String]()
    var selectedLabel = UILabel()
    var isDOB = true
    let authVM = AuthViewModel()
    var gender_pronown = ""
    var gender_dec = ""
    var delegate:RegistrationCompleted?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUp()
 }
  func viewSetUp(){
    authVM.getCountryList(viewcontroller: self) {}
    txt_userName.placeholderColor(text: "Username*", color: UIColor.darkGray)
    txt_password.placeholderColor(text: "Password*", color: UIColor.darkGray)
    txt_email.placeholderColor(text: "Email*", color: UIColor.darkGray)
    txt_name.placeholderColor(text: "Name*", color: UIColor.darkGray)
    txt_lastName.placeholderColor(text: "Last Name*", color: UIColor.darkGray)
    txt_mobileNo.placeholderColor(text: "Mobile No*", color: UIColor.darkGray)
    
    txt_mobileNo.addPadding(padding: .equalSpacing(5))
    txt_userName.addPadding(padding: .equalSpacing(5))
    txt_password.addPadding(padding: .equalSpacing(5))
    txt_email.addPadding(padding: .equalSpacing(5))
    txt_name.addPadding(padding: .equalSpacing(5))
    txt_lastName.addPadding(padding: .equalSpacing(5))
    DispatchQueue.main.async {
        self.view_genderSelection.frame = self.view.frame
    }
  }
    @IBAction func selectCountryCodeBtnAction(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            if authVM.countryList.count > 0{
                dropDownData = authVM.countryList.map{ $0.phonecode}
                tbl_dropDown.reloadData()
                selectedLabel = btn_countryCode.titleLabel!
                tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 150))
                sender.superview!.superview!.addSubview(tbl_dropDown)
            }else{
                authVM.getCountryList(viewcontroller: self) {
                    DispatchQueue.main.async {
                        self.dropDownData = self.authVM.countryList.map{ $0.phonecode}
                        self.tbl_dropDown.reloadData()
                    }
                }
            }
        }else{
            sender.tag = 0
            tbl_dropDown.removeFromSuperview()
        }
    }
    @IBAction func actionLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSignUp(_ sender: Any) {
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let param = ["username":txt_userName.text!,
                     "password":txt_password.text!,
                     "fname":txt_name.text!,
                     "lname":txt_lastName.text!,
                     "email":txt_email.text!,
                     "mobile":btn_countryCode.titleLabel!.text! + txt_mobileNo.text!,
                     "gender":lbl_gender.text!.lowercased(),
                     "gender_pronoun":gender_pronown,
                     "gender_optional":gender_dec,
                     "country":lbl_country.text!,
                     "city":"Indore",
                     "dob":lbl_dob.text!]
        authVM.registerUser(viewController: self, parameter: param) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                    Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.Registration_Success, viewController: self) {
                        self.delegate?.userRegistered(emali: self.txt_email.text!, password: self.txt_password.text!)
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    @IBAction func genderBtnAction(_ sender: UIButton) {
        view.addSubview(view_genderSelection)
    }
    @IBAction func dobBtnAction(_ sender: UIButton) {
        isDOB = true
        date_pickerView.isHidden = false
        pickerView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func countryBtnAction(_ sender: UIButton) {
        if authVM.countryList.count > 0{
            showCountryPicker()
        }else{
            authVM.getCountryList(viewcontroller: self) {
                DispatchQueue.main.async {
                    self.showCountryPicker()
                }
            }
        }
    }
    func showCountryPicker() {
        isDOB = false
        date_pickerView.isHidden = true
        pickerView.isHidden = false
        pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func closeGenderSelectionViewBtnAction(_ sender: UIButton) {
        view_genderSelection.removeFromSuperview()
    }
    @IBAction func selectGenderBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_selectGender
        dropDownData = genderArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func saveGenderBtnAction(_ sender: UIButton) {
        lbl_gender.text = lbl_selectGender.text
        view_genderSelection.removeFromSuperview()
        gender_dec = txt_genderDesc.text!
    }
    @IBAction func selectPronownBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_pronown
        dropDownData = customArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+130), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func selectPickerContentAction(_ sender: UIButton) {
        if isDOB{
            Date_Formatter.dateFormat = "yyyy-MM-dd"
            lbl_dob.text = Date_Formatter.string(from: date_pickerView.date)
        }else{
            lbl_country.text = authVM.countryList[pickerView.selectedRow(inComponent: 0)].name
        }
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}
extension SignUpViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell")
        cell?.textLabel?.text = dropDownData[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let text = dropDownData[indexPath.row]
        if selectedLabel == lbl_selectGender{
            if text == "Custom" {
                viewSelectProNown.isHidden = false
                txt_genderDesc.isHidden = false
            }else{
                viewSelectProNown.isHidden = true
                txt_genderDesc.isHidden = true
                txt_genderDesc.text = ""
                gender_pronown = ""
            }
        }
        if selectedLabel == lbl_pronown{
            gender_pronown = text
        }
        if selectedLabel == lbl_selectGender ||  selectedLabel == lbl_pronown{
            selectedLabel.text = text
        }else{
            btn_countryCode.setTitle(text, for: .normal)
        }
        tbl_dropDown.removeFromSuperview()
    }
    // Support Method
    func isValidUserEntry() -> (Bool,String) {
            if txt_userName.text!.isEmpty{
            return (false,Error_Alert.Missing_Username)
        }
        if txt_email.text!.isEmpty{
            return (false,Error_Alert.Missing_Email)
        }
        if !Enough.shared.isValidEmail(txt_email.text!){
            return (false,Error_Alert.Invalid_Email)
        }
        if txt_mobileNo.text!.isEmpty {
            return (false,Error_Alert.Missing_Mobile)
        }
        if txt_password.text!.isEmpty {
            return (false,Error_Alert.Missing_Password)
        }
        if lbl_gender.text! == "Gender"{
            return (false,Error_Alert.Missing_Gender)
        }
        if lbl_dob.text! == "Date of birth"{
            return (false,Error_Alert.Missing_DOB)
        }
        if lbl_country.text! == "Country"{
            return (false,Error_Alert.Missing_Country)
        }
        return (true,"")
    }
}
extension SignUpViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return authVM.countryList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return authVM.countryList[row].name
    }
}
