//
//  LoginViewController.swift
//  enough!
//
//  Created by mac on 01/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txtFusername: UITextField!
    @IBOutlet weak var txtFPassword: UITextField!
    
    let authVM = AuthViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUp()
    }
    func viewSetUp(){
        txtFusername.placeholderColor(text: "Username*", color: UIColor.darkGray)
        txtFPassword.placeholderColor(text: "Password*", color: UIColor.darkGray)
        txtFusername.addPadding(padding: .equalSpacing(5))
        txtFPassword.addPadding(padding: .equalSpacing(5))
    }
    @IBAction func actionLogin(_ sender: Any) {
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let param = ["username":txtFusername.text!,
                     "password":txtFPassword.text!]
        authVM.loginUser(viewController: self, parameter: param) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                    if let data = result["data"]?.dictionary{
                        let loginData = Enough.shared.getCoreDic(jsonDic: data)
                        UserDefaults.standard.setValue(loginData, forKey: "Login_Data")
                        let user = User.init(dic: loginData)
                        Enough.shared.loginUser = user
                        Enough.shared.enough_status.acc_created = true
                        let SWRevealVC = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                        self.navigationController?.pushViewController(SWRevealVC, animated: true)
                    }
                }else{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    
    @IBAction func actionRegister(_ sender: Any) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        signUpVC.delegate = self
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @IBAction func actionLostinfo(_ sender: Any) {
        
    }
    // Support Method
    func isValidUserEntry() -> (Bool,String) {
        if txtFusername.text!.isEmpty{
            return (false,Error_Alert.Missing_Username)
        }
        if txtFPassword.text!.isEmpty {
            return (false,Error_Alert.Missing_Password)
        }
        return (true,"")
    }
}
extension LoginViewController:RegistrationCompleted{
    func userRegistered(emali: String, password: String) {
        txtFusername.text = emali
        txtFPassword.text = password
    }
}
