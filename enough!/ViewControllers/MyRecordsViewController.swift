//
//  MyRecordsViewController.swift
//  enough!
//
//  Created by mac on 08/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol MissingPersonUpdate {
    func updateHome()
}

class MyRecordsViewController: UIViewController {

    var isDelete = true
    @IBOutlet weak var tblVRecords: UITableView!
    let missingPersonVM = MissingPersonViewModel()
    let addHouseholdVM = AddHouseholdViewModel()
    let verifyUserVM = VerifyUserViewModel()
    var delegate:AlertsViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        getPeronRecord()
    }
    func getPeronRecord() {
        missingPersonVM.getPersonRecord(viewController: self) {
            DispatchQueue.main.async {
                self.tblVRecords.reloadData()
                if self.missingPersonVM.missingPerson.count == 0{
                    self.updateHomeSteps(value: "")
                }else{
                    self.updateHomeSteps(value: "1")
                }
            }
        }
    }
    func updateHomeSteps(value:String) {
        let param = ["user_id":Enough.shared.loginUser.id,"missing_person_template":value]
        print("param:",param)
        if value == "1"{
            Enough.shared.enough_status.miss_person_record = true
        }else{
            Enough.shared.enough_status.miss_person_record = false
        }
        verifyUserVM.updateHomeSteps(viewController: self, param: param){ (result) in
            if result["status"]?.int == 1{
                if var homeStep = UserDefaults.standard.value(forKey: "HomeSteps") as? [String:String]{
                    homeStep["missing_person_template"] = value
                    UserDefaults.standard.set(homeStep, forKey: "HomeSteps")
                    self.delegate!.updateHome()
                }
            }
        }
    }
    @IBAction func actionQues(_ sender: Any) {
    }
    @IBAction func actionAddRecords(_ sender: Any) {
        let MissingPersonTemplateVC = self.storyboard?.instantiateViewController(withIdentifier: "MissingPersonTemplateViewController") as! MissingPersonTemplateViewController
        MissingPersonTemplateVC.personDelegate = self
        self.navigationController?.pushViewController(MissingPersonTemplateVC, animated: true)
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true  )
    }
}
extension MyRecordsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return missingPersonVM.missingPerson.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonRecordTVC") as! PersonRecordTVC
        cell.personDetails = missingPersonVM.missingPerson[indexPath.row]
        cell.btn_edit.tag = indexPath.row
        cell.btn_plush.tag = indexPath.row
        cell.btn_cancel.tag = indexPath.row
        cell.btn_edit.addTarget(self, action: #selector(editPerson(sender:)), for: .touchUpInside)
        cell.btn_plush.addTarget(self, action: #selector(plushPerson(sender:)), for: .touchUpInside)
        cell.btn_cancel.addTarget(self, action: #selector(deletePerson(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let MissingPersonDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MissingPersonDetailViewController") as! MissingPersonDetailViewController
        MissingPersonDetailVC.person = missingPersonVM.missingPerson[indexPath.row]
        self.navigationController?.pushViewController(MissingPersonDetailVC, animated: true)
    }
    @objc func editPerson(sender:UIButton) {
        let MissingPersonTemplateVC = self.storyboard?.instantiateViewController(withIdentifier: "MissingPersonTemplateViewController") as! MissingPersonTemplateViewController
        MissingPersonTemplateVC.person = missingPersonVM.missingPerson[sender.tag]
        MissingPersonTemplateVC.personDelegate = self
        self.navigationController?.pushViewController(MissingPersonTemplateVC, animated: true)
    }
    @objc func deletePerson(sender:UIButton) {
        isDelete = true
        let infoPopUp = InfoPopUpView(frame: view.bounds)
        infoPopUp.btn_ok = sender
        infoPopUp.btn_cross = sender
        infoPopUp.lbl_info.text = Alert_Msg.Confirm_Delete_Record
        infoPopUp.delegate = self
        view.addSubview(infoPopUp)
    }
    @objc func plushPerson(sender:UIButton) {
        isDelete = false
        let infoPopUp = InfoPopUpView(frame: view.bounds)
        infoPopUp.btn_ok = sender
        infoPopUp.btn_cross = sender
        infoPopUp.lbl_info.text = Alert_Msg.Confirm_AddHouseHold_Record
        infoPopUp.delegate = self
        view.addSubview(infoPopUp)
    }
}
extension MyRecordsViewController:InfoPopUpActions{
    func crossBtnAction(sender: UIButton) {
        
    }
    
    func okBtnAction(sender: UIButton) {
    let persn = missingPersonVM.missingPerson[sender.tag]
        let templet_id = persn.id
        if isDelete{
            missingPersonVM.deleteRecord(template_id: templet_id, viewController: self) { (result) in
                DispatchQueue.main.async {
                    if result["status"]?.int == 0{
                        Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                    }else{
                        self.removePerson(index: sender.tag)
                    }
                }
            }
        }else{
            let param = [ "user_id":Enough.shared.loginUser.id,"firstname":persn.firstname!,"lastname":persn.lastname!,"contact_email":"","contactphone":"","relationship":persn.relation!,"type":"2"]
            print("param",param)
            addHouseholdVM.addHousehold(viewController: self, parameter: param) { (result) in
                DispatchQueue.main.async {
                    if result["status"]?.int == 1{
                            Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.addHousehold_Success, viewController: self) {
                                //self.removePerson(index: sender.tag)
                            }
                    }else{
                        print("result",result)
                        Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                    }
                }
            }
        }
    }
    func removePerson(index:Int) {
        self.missingPersonVM.missingPerson.remove(at: index)
        if self.missingPersonVM.missingPerson.count == 0{
            self.updateHomeSteps(value: "")
        }
        self.tblVRecords.reloadData()
    }
}
extension MyRecordsViewController:PersonAction{
    func personAdded() {
        self.missingPersonVM.missingPerson.removeAll()
        self.tblVRecords.reloadData()
        getPeronRecord()
    }
}
