//
//  MyProfileViewController.swift
//  enough!
//
//  Created by mac on 07/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import Firebase

protocol ProfileUpdateCompleted {
func  profileUpdated(emali:String,password:String)
}

class MyProfileViewController: UIViewController,UIPopoverPresentationControllerDelegate,addressProtocol {
    
    // MARK: Screens Outlets
    @IBOutlet weak var txt_userName: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_lastName: UITextField!
    @IBOutlet weak var txt_alias: UITextField!
    @IBOutlet weak var txt_headLine: UITextField!
    @IBOutlet weak var txt_phonNumber: UITextField!
    @IBOutlet weak var txt_dob: UITextField!
    @IBOutlet weak var txt_address: UITextField!
    @IBOutlet weak var txt_gender: UITextField!
    @IBOutlet weak var txt_pronown: UITextField!
    @IBOutlet weak var txtFStreetNo: UITextField!
    @IBOutlet weak var txtFApartment: UITextField!
    @IBOutlet weak var txtFCity: UITextField!
    @IBOutlet weak var txtFState: UITextField!
    @IBOutlet weak var txtFZipcode: UITextField!
    @IBOutlet var view_picker: UIView!
    @IBOutlet weak var date_pickerView: UIDatePicker!
    @IBOutlet weak var height_picker: NSLayoutConstraint!
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet var view_genderSelection: UIView!
    @IBOutlet weak var lbl_selectGender: UILabel!
    @IBOutlet weak var viewSelectProNown: UIView!
    @IBOutlet weak var txt_genderDesc: UITextField!
    @IBOutlet var tbl_dropDown: UITableView!
    @IBOutlet weak var lbl_pronown: UILabel!
    
    @IBOutlet weak var btn_verify: UIButton!
    @IBOutlet weak var btnEmailverify: UIButton!
    @IBOutlet weak var btnVerifyId: UIButton!
    let authVM = AuthViewModel()
    let verifyUserVM = VerifyUserViewModel()
    
    var genderArray = ["Male","Female","Custom"]
       var customArray = ["She: 'Wish her a happy birthday!'","He: 'Wish him a happy birthday!'","They: 'Wish them a happy birthday!'"]
       var dropDownData = [String]()
       var selectedLabel = UILabel()
    var gender_pronown = ""
       var gender_dec = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUp()
        self.isVerifyAccount()
        getStatusAPIcall()
    }
    func getStatusAPIcall(){
        authVM.getVerifiedStatus(viewController: self) {
            DispatchQueue.main.async {
                self.isVerifyAccount()
            }
        }
    }
    func isVerifyAccount() {
        if Enough.shared.loginUser.mobile_verify_status == "1"{
            let attributedString = NSMutableAttributedString(string: "Verified  ")
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            btn_verify.setAttributedTitle(attributedString, for: .normal)
            btn_verify.tintColor = UIColor(named: "lightMilky")
            btn_verify.tag = 1
        }
        if Enough.shared.loginUser.email_verify_status == "1"{
            let attributedString = NSMutableAttributedString(string: "Verified  ")
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            btnEmailverify.setAttributedTitle(attributedString, for: .normal)
            btnEmailverify.tintColor =  UIColor(named: "lightMilky")
            btnEmailverify.tag = 2
        }
        if Enough.shared.loginUser.id_verify_status == "1"{
            let attributedString = NSMutableAttributedString(string: "Verified  ")
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            btnVerifyId.setAttributedTitle(attributedString, for: .normal)
            btnVerifyId.tintColor = UIColor(named: "lightMilky")
            btnVerifyId.tag = 3
        }
    }
    func viewSetUp(){
        authVM.getCountryList(viewcontroller: self) {}
        btnUpdate.setTitle("Update", for: .normal)
        txt_userName.placeholderColor(text: "Enter username", color: UIColor.darkGray)
        txt_email.placeholderColor(text: "Enter email", color: UIColor.darkGray)
        txt_name.placeholderColor(text: "Enter name", color: UIColor.darkGray)
        txt_lastName.placeholderColor(text: "Enter lastname", color: UIColor.darkGray)
        txt_alias.placeholderColor(text: "Enter alias", color: UIColor.darkGray)
        txt_headLine.placeholderColor(text: "Enter headline", color: UIColor.darkGray)
        txt_phonNumber.placeholderColor(text: "Enter phone number", color: UIColor.darkGray)
        txt_dob.placeholderColor(text: "Enter date of birth", color: UIColor.darkGray)
        txt_address.placeholderColor(text: "Enter address", color: UIColor.darkGray)
        txt_gender.placeholderColor(text: "Enter gender", color: UIColor.darkGray)
        txt_pronown.placeholderColor(text: "Enter pronown", color: UIColor.darkGray)
        
        txt_userName.text = Enough.shared.loginUser.username
        txt_email.text = Enough.shared.loginUser.email
        txt_name.text = Enough.shared.loginUser.name
        txt_lastName.text = Enough.shared.loginUser.lastname
        txt_alias.text = Enough.shared.loginUser.alias
        txt_headLine.text = Enough.shared.loginUser.headline
        txt_phonNumber.text = Enough.shared.loginUser.mobile
        txt_dob.text = Enough.shared.loginUser.dob
        txt_address.text = "\(Enough.shared.loginUser.city ?? ""),\(Enough.shared.loginUser.country ?? "")"
        txt_gender.text = Enough.shared.loginUser.gender
        txt_pronown.text = Enough.shared.loginUser.gender_pronoun
        
        txtFStreetNo.placeholderColor(text: "Street and number", color: UIColor.darkGray)
        txtFApartment.placeholderColor(text: "Apartment, suite, unit, building, floor etc", color: UIColor.darkGray)
        txtFCity.placeholderColor(text: "City", color: UIColor.darkGray)
        txtFState.placeholderColor(text: "state / province / region", color: UIColor.darkGray)
        txtFZipcode.placeholderColor(text: "Zipcode", color: UIColor.darkGray)
        
        txtFStreetNo.addPadding(padding: .equalSpacing(5))
        txtFApartment.addPadding(padding: .equalSpacing(5))
        txtFCity.addPadding(padding: .equalSpacing(5))
        txtFState.addPadding(padding: .equalSpacing(5))
        txtFZipcode.addPadding(padding: .equalSpacing(5))
        DispatchQueue.main.async {
               self.view_genderSelection.frame = self.view.frame
           }
        _ = isValidUserEntry()
    }
    
    func addressValue(country: String, street: String, apartment: String, city: String, state: String, zipcode: String) {
        txt_address.text = country
        txtFStreetNo.text = street
        txtFApartment.text = apartment
        txtFCity.text = city
        txtFState.text = state
        txtFZipcode.text = zipcode
         _ = isValidUserEntry()
    }
    @IBAction func actionSelectDatePicker(_ sender: Any) {
        Date_Formatter.dateFormat = "yyyy-MM-dd"
        txt_dob.text = Date_Formatter.string(from: date_pickerView.date)
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 0
            self.view.layoutIfNeeded()
        }
         _ = isValidUserEntry()
    }
    @IBAction func closeGenderSelectionViewBtnAction(_ sender: UIButton) {
        view_genderSelection.removeFromSuperview()
    }
    @IBAction func selectGenderBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_selectGender
        dropDownData = genderArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func saveGenderBtnAction(_ sender: UIButton) {
        txt_gender.text = lbl_selectGender.text
        txt_pronown.text = lbl_pronown.text
        view_genderSelection.removeFromSuperview()
        gender_dec = txt_genderDesc.text!
         _ = isValidUserEntry()
    }
    @IBAction func selectPronownBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_pronown
        dropDownData = customArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+130), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    // MARK: Screens Action
    @IBAction func actionVerifyEmail(_ sender: Any) {
        if btnEmailverify.tag == 2{
            return
        }
        navigateToVerifyAccount()
    }
    
    @IBAction func actionVerifyId(_ sender: Any) {
        if btnVerifyId.tag == 3{
            return
        }
        navigateToVerifyAccount()
    }
    func navigateToVerifyAccount()  {
        let VerifyAccountVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyAccountViewController") as! VerifyAccountViewController
        VerifyAccountVC.modalPresentationStyle = .overCurrentContext
        VerifyAccountVC.modalTransitionStyle = .crossDissolve
        //VerifyAccountVC.delegate = self
        self.present(VerifyAccountVC, animated: true, completion: nil)
    }
    @IBAction func actionVerifyMobileNo(_ sender: Any) {
        if btn_verify.tag == 1{
            return
        }
        navigateToVerifyAccount()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOpenDatePicker(_ sender: UIButton) {
        date_pickerView.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func editAddressBtnAction(_ sender: Any) {
        let addressPopUp = self.storyboard?.instantiateViewController(withIdentifier: "Address_PopupViewController") as! Address_PopupViewController
        addressPopUp.modalPresentationStyle = .overCurrentContext
        addressPopUp.modalTransitionStyle = .crossDissolve
        addressPopUp.delegate = self
        self.present(addressPopUp, animated: true, completion: nil)
    }
    
    @IBAction func editGenderBtnAction(_ sender: Any) {
        view.addSubview(view_genderSelection)
    }
    
    @IBAction func actionUpdate(_ sender: Any) {
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let param = [ "email":txt_email.text!,"mobile":txt_phonNumber.text!,"fname":txt_name.text!,"lname":txt_lastName.text!,"gender":txt_gender.text!,"dob":txt_dob.text!,"city":txtFCity.text!,"country":Enough.shared.loginUser.country!,"alias":txt_alias.text!,"headline":txt_headLine.text!,"user_id":Enough.shared.loginUser.id,"gender_pronoun":txt_gender.text!,"gender_optional":txt_pronown.text!]
        print("param",param)
        authVM.updateUser(viewController: self, parameter: param) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                    self.updateHomeSteps()
                        Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.ProfileUpdate_Success, viewController: self) {
                    }
                }else{
                    print("result",result)
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    func updateHomeSteps() {
        let param = ["user_id":Enough.shared.loginUser.id,"complete_your_profile":"1"]
        Enough.shared.enough_status.pro_completed = true
        verifyUserVM.updateHomeSteps(viewController: self, param: param){ (result) in
            if result["status"]?.int == 1{
                if var homeStep = UserDefaults.standard.value(forKey: "HomeSteps") as? [String:String]{
                    homeStep["complete_your_profile"] = "1"
                    UserDefaults.standard.set(homeStep, forKey: "HomeSteps")
                }
            }
            
        }
    }
    func isValidUserEntry() -> (Bool,String) {
        if txt_userName.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Username)
        }
        if txt_password.text!.isEmpty {
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Password)
        }
        if txt_email.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Email)
        }
        if !Enough.shared.isValidEmail(txt_email.text!){
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Invalid_Email)
        }
        if txt_name.text!.isEmpty {
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Name)
        }
        if txt_lastName.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Lastname)
        }
        if txt_alias.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Alias)
        }
        if txt_headLine.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Headline)
        }
        if txt_phonNumber.text!.isEmpty {
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_PhonNumber)
        }
        if txt_dob.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_DOB)
        }
        if txt_address.text!.isEmpty{
            return (false,Error_Alert.Missing_Address)
        }
        if txt_gender.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Gender)
        }
        if txt_pronown.text!.isEmpty {
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Pronoun)
        }
        checkButtonEnable(isValid: true)
        return (true,"")
    }
    func checkButtonEnable(isValid:Bool) {
        if isValid{
            btnUpdate.backgroundColor = UIColor(named: "darkPink")
            btnUpdate.isSelected = true
        }else{
            btnUpdate.backgroundColor = UIColor.lightGray
            btnUpdate.isSelected = false
        }
    }
    
}

extension MyProfileViewController: UITableViewDataSource,UITableViewDelegate{
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dropDownData.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell")
    cell?.textLabel?.text = dropDownData[indexPath.row]
    return cell!
}
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let text = dropDownData[indexPath.row]
    if selectedLabel == lbl_selectGender{
        if text == "Custom" {
            viewSelectProNown.isHidden = false
            txt_genderDesc.isHidden = false
        }else{
            viewSelectProNown.isHidden = true
            txt_genderDesc.isHidden = true
            txt_genderDesc.text = ""
            gender_pronown = ""
        }
    }
    if selectedLabel == lbl_pronown{
        gender_pronown = text
    }
    if selectedLabel == lbl_selectGender ||  selectedLabel == lbl_pronown{
        selectedLabel.text = text
    }
    _ = isValidUserEntry()
    tbl_dropDown.removeFromSuperview()
}
}

extension MyProfileViewController:UITextFieldDelegate,UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        _ = isValidUserEntry()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = isValidUserEntry()
    }
}
