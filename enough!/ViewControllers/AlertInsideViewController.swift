//
//  AlertInsideViewController.swift
//  enough!
//
//  Created by mac on 02/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import CoreLocation

class AlertInsideViewController: UIViewController {
    
    @IBOutlet weak var tblVAlerts: UITableView!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet var view_filter: UIView!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_emergency: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_lessThan5Min: UILabel!
    @IBOutlet weak var lbl_community: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet var tbl_dropDown: UITableView!
    
    var header:HeaderView?
    var distanceArray = [Alert_Distances.meter_100,Alert_Distances.meter_100to500,Alert_Distances.meter_500to1km,Alert_Distances.km_1]
    var emerg_TypArray = [Alert_Emergency_Type.all,Alert_Emergency_Type.domestic,Alert_Emergency_Type.public_space_ind,Alert_Emergency_Type.public_space_out]
    var alertStatusArray = [Alert_Status.all,Alert_Status.by_victim,Alert_Status.by_safeContact,Alert_Status.by_responder]
    var timeArray = [Alert_Time.less_5min,Alert_Time.betwen_5to10Min,Alert_Time.over_10Min]
    var alertShowArray = [Alert_Show.show_community,Alert_Show.my_contact]
    var dropDownArray = [String]()
    var selectedLabel = UILabel()
    let alertSettingVM = AlertSettingViewModel()
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.header?.checkTriggerMode()
       }
    func viewSetup() {
      DispatchQueue.main.async {
          self.header = HeaderView(frame: self.view_header.frame)
          self.header?.headerDelegate = self
          self.view_header.addSubview(self.header!)
        self.view_filter.frame = self.view.frame
      }
        accessLatLong()
  }
    func getAlertList() {
        alertSettingVM.getUserAlertList(viewController: self) {
            DispatchQueue.main.async {
                self.tblVAlerts.reloadData()
            }
        }
    }
    func accessLatLong() {
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    @IBAction func filterBtnAction(_ sender: Any) {
        assignFilter()
        view.addSubview(view_filter)
    }
    @IBAction func distanceSelectBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_distance
        dropDownArray = distanceArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func emergencySelectBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_emergency
        dropDownArray = emerg_TypArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func statusSelectBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_status
        dropDownArray = alertStatusArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func timeSelectBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_lessThan5Min
        dropDownArray = timeArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 150))
        sender.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func communitySelectBtnAction(_ sender: UIButton) {
        selectedLabel = lbl_community
        dropDownArray = alertShowArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 100))
        sender.superview!.superview!.addSubview(tbl_dropDown)
    }
    @IBAction func saveFilterBtnAction(_ sender: UIButton) {
        getFilter()
        view_filter.removeFromSuperview()
        alertSettingVM.alert_list.removeAll()
        tblVAlerts.reloadData()
        getAlertList()
    }
    @IBAction func cancelFilterBtnAction(_ sender: UIButton) {
        view_filter.removeFromSuperview()
    }
    func assignFilter() {
        lbl_distance.text = alertSettingVM.alertDistance
        lbl_emergency.text = alertSettingVM.alert_emergency_type
        lbl_status.text = alertSettingVM.alert_status
        lbl_lessThan5Min.text = alertSettingVM.alert_time_diffrance
        lbl_community.text = alertSettingVM.alert_Community
    }
    func getFilter()  {
        alertSettingVM.alertDistance = lbl_distance.text!
        alertSettingVM.alert_emergency_type = lbl_emergency.text!
        alertSettingVM.alert_status = lbl_status.text!
        alertSettingVM.alert_time_diffrance = lbl_lessThan5Min.text!
        alertSettingVM.alert_Community = lbl_community.text!
    }
}
extension AlertInsideViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbl_dropDown{
            return dropDownArray.count
        }
        return alertSettingVM.alert_list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbl_dropDown{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell")
            cell?.textLabel?.text = dropDownArray[indexPath.row]
            return cell!
        }
      let cell = tblVAlerts.dequeueReusableCell(withIdentifier: "AlertsCell") as! AlertsTableViewCell
        cell.alert_details = alertSettingVM.alert_list[indexPath.row]
      return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbl_dropDown{
            let text = dropDownArray[indexPath.row]
            selectedLabel.text = text
            tbl_dropDown.removeFromSuperview()
        }else if tableView == tblVAlerts{
            let alertDetailsVC = storyboard?.instantiateViewController(withIdentifier: "AlertDetailsViewController") as! AlertDetailsViewController
            alertDetailsVC.alert = alertSettingVM.alert_list[indexPath.row]
            alertDetailsVC.alert_list = alertSettingVM.alert_list
            alertDetailsVC.alert_list.remove(at: indexPath.row)
            navigationController?.pushViewController(alertDetailsVC, animated: true)
        }
    }
}
extension AlertInsideViewController:HeaderProtocol{
    func okButtonPress(isLive: Bool) {
        if isLive{
            SettingsViewModel().updateUserMode(userMode: "live", viewController: self) {
                DispatchQueue.main.async {
                    self.header?.checkTriggerMode()
                }
            }
            //self.header?.btnTestLiveToggle.isSelected = true
        }else{
            self.header?.btnOnOfToggle.isSelected = true
        }
        Enough.shared.triggerMode = .on
        self.header?.checkTriggerMode()
    }
    func closeButtonPress(isLive: Bool) {
        
    }
}
extension AlertInsideViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if Enough.shared.currentLoc == nil{
            Enough.shared.currentLoc = locations.first
            getAlertList()
            return
        }
        Enough.shared.currentLoc = locations.first
    }
}
