//
//  MissingPersonTemplateViewController.swift
//  enough!
//
//  Created by mac on 08/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol PersonAction {
    func personAdded()
}

class MissingPersonTemplateViewController: UIViewController,RelationProtocol {
    
    @IBOutlet weak var lbl_selectImg: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var view_weight: UIView!
    @IBOutlet weak var view_height: UIView!
    @IBOutlet weak var view_scrollContent: UIView!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtFLastName: UITextField!
    @IBOutlet weak var txtFAlias: UITextField!
    @IBOutlet weak var txtFDOB: UITextField!
    @IBOutlet var tbl_dropDown: UITableView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblBuild: UILabel!
    @IBOutlet weak var lblEye: UILabel!
    @IBOutlet weak var lblHairColor: UILabel!
    @IBOutlet weak var lblGlasses: UILabel!
    @IBOutlet weak var lblHairLength: UILabel!
    @IBOutlet weak var lblEthnicity: UILabel!
    @IBOutlet weak var lblHairStyle: UILabel!
    @IBOutlet weak var lblSelectRelation: UILabel!
    @IBOutlet weak var txtVUniqueSigns: UITextView!
    @IBOutlet weak var txtVSpecialNeeds: UITextView!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblCm: UILabel!
    @IBOutlet weak var lblKg: UILabel!
    @IBOutlet var view_picker: UIView!
    @IBOutlet weak var date_pickerView: UIDatePicker!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var height_picker: NSLayoutConstraint!
    
    var heightArray = ["cm","ft"]
    var weightArray = ["kg","lbs"]
    var genderArray = ["Male","Female"]
    var buildArray = ["Average height and weight","Average height and heavy","Average height and thin","Tall and average weight","Tall and heavy","Tall and thin","Short and average weight","Short and heavy","Short and thin"]
    var eyeArray = ["Amber","Blue – light","Blue - dark","Brown – light","Brown – medium","Brown - dark","Grey","Green","Hazel"]
    var hairColorArray = ["Auburn (red-brown)","Blonde – light","Blonde - dark","Brown – light","Brown – medium","Brown – dark","Black","Grey","Red - natural","White","Blue","Green","Red","Pink","Purple"]
    var glassesArray = ["Yes","No"]
    var hairLengthArray = ["Bald","Shaved","Short","Shoulders","Medium","Long"]
    var ethnicityArray = ["Asian/Chinese","Black/African","Indian/Pakistani","Latino/Hispanic","Middle Eastern","Native American","Pacific Islander","White/Caucasian"]
    var hairStyleArray = ["Straight","Wavy","Curly","Spiky","Ponytail","Dreadlocks"]
    var dropDownData = [String]()
    var selectedLabel = UILabel()
    var isDOB = true
    var cmArray:[Int] = []
    var ftArray:[Double] = []
    var kgArray:[Int] = []
    var lbsArray:[Int] = []
    var isHeight = true
    var selectedProfileData:Data!
    var imagePicker = UIImagePickerController()
    var missingPersonVM = MissingPersonViewModel()
    var personDelegate:PersonAction?
    var person:Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 60...200{
            cmArray.append(i)
        }
        for i in stride(from: 2.0, to: 7.3, by: 0.1){
            ftArray.append(i.rounded(toPlaces: 1))
        }
        for i in 10...200{
            kgArray.append(i)
        }
        for i in 20...400{
            lbsArray.append(i)
        }
        viewSetUp()
        imagePicker.delegate = self
    }
    
    func viewSetUp(){
        txtFName.placeholderColor(text: "Name", color: UIColor.darkGray)
        txtFLastName.placeholderColor(text: "Last Name", color: UIColor.darkGray)
        txtFAlias.placeholderColor(text: "Alias / AKA / Also goes by", color: UIColor.darkGray)
        txtFDOB.placeholderColor(text: "Date of birth", color: UIColor.darkGray)
        
        txtFName.addPadding(padding: .equalSpacing(5))
        txtFLastName.addPadding(padding: .equalSpacing(5))
        txtFAlias.addPadding(padding: .equalSpacing(5))
        txtFDOB.addPadding(padding: .equalSpacing(5))
        view_height.addBottomBorder()
        view_weight.addBottomBorder()
        
        if person != nil{
            btnSave.setTitle("Update", for: .normal)
            txtFName.text = person.firstname
            txtFLastName.text = person.lastname
            txtFAlias.text = person.alias
            lblHeight.text = person.height
            lblCm.text = person.height_type
            lblWeight.text = person.weight
            lblKg.text = person.weight_type
            txtFDOB.text = person.dob
            lblGender.text = person.gender
            lblBuild.text = person.build
            lblEye.text = person.eyes
            lblHairColor.text = person.haircolor
            lblGlasses.text = person.glasses
            lblHairLength.text = person.hairlength
            lblEthnicity.text = person.ethnicity
            lblHairStyle.text = person.hairstyle
            lblSelectRelation.text = person.relation
            txtVUniqueSigns.text = person.unique_sign
            txtVSpecialNeeds.text = person.special_health_condition
            lbl_selectImg.text = (person.image_path! as NSString).lastPathComponent
            _ = isValidUserEntry()
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func uploadPicBtnAction(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Select Image", message: "Browse image from?", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(galleryAction)
        actionSheet.addAction(cancelAction)
        present(actionSheet, animated: true, completion: nil)
    }
    @IBAction func actionQues(_ sender: Any) {
    }
    
    @IBAction func actionCmHeight(_ sender: UIButton) {
        selectedLabel = lblCm
        dropDownData = heightArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 100))
        view_scrollContent.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionKgWeight(_ sender: UIButton) {
        selectedLabel = lblKg
        dropDownData = weightArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.frame.origin.x, y: sender.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.frame.size.width, height: 100))
        view_scrollContent.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionHeightValue(_ sender: Any) {
        isDOB = false
        isHeight = true
        date_pickerView.isHidden = true
        pickerView.isHidden = false
        pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func actionWeightValue(_ sender: Any) {
        isDOB = false
        isHeight = false
        date_pickerView.isHidden = true
        pickerView.isHidden = false
        pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func actionDOB(_ sender: Any) {
        isDOB = true
        date_pickerView.isHidden = false
        pickerView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func actionGender(_ sender: UIButton) {
        selectedLabel = lblGender
        dropDownData = genderArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 100))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
        
        //openDropDowmTV(selectedLabel:lblGender,dropDownData:genderArray,sender:sender, height: 100)
    }
    
    @IBAction func actionBuild(_ sender: UIButton) {
        selectedLabel = lblBuild
        dropDownData = buildArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 450))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionEye(_ sender: UIButton) {
        selectedLabel = lblEye
        dropDownData = eyeArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 450))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionHairColor(_ sender: UIButton) {
        selectedLabel = lblHairColor
        dropDownData = hairColorArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 1250))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionGlasses(_ sender: UIButton) {
        selectedLabel = lblGlasses
        dropDownData = glassesArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 100))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionHairLegth(_ sender: UIButton) {
        selectedLabel = lblHairLength
        dropDownData = hairLengthArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 300))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionEthnicity(_ sender: UIButton) {
        selectedLabel = lblEthnicity
        dropDownData = buildArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 400))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionHairStyle(_ sender: UIButton) {
        selectedLabel = lblHairStyle
        dropDownData = hairStyleArray
        tbl_dropDown.reloadData()
        tbl_dropDown.frame = CGRect(origin: CGPoint(x: sender.superview!.superview!.frame.origin.x, y: sender.superview!.superview!.frame.origin.y+50), size: CGSize(width: sender.superview!.superview!.frame.size.width, height: 300))
        sender.superview!.superview!.superview!.addSubview(tbl_dropDown)
    }
    
    @IBAction func actionSelect(_ sender: UIButton) {
        let relationView = RelationView(frame: view.bounds)
        relationView.dropDownValues = relationArray
        relationView.tbl_list.reloadData()
        relationView.delegate = self
        view.addSubview(relationView)
        return
    }
    
    func selectedRelation(relation: String) {
        lblSelectRelation.text! = relation
    }
    
    @IBAction func actionSave(_ sender: UIButton) {
        let validation = isValidUserEntry()
        if !validation.0{
            Enough.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        var param = ["user_id":Enough.shared.loginUser.id,"firstname":txtFName.text!,"lastname":txtFLastName.text!,
                     "alias":txtFAlias.text!,"height":lblHeight.text!,"height_type":lblCm.text!,
                     "weight":lblWeight.text!,"weight_type":lblKg.text!,"dob":txtFDOB.text!,"gender":lblGender.text!,
                     "build":lblBuild.text!,"eyes":lblEye.text!,"haircolor":lblHairColor.text!,"glasses":lblGlasses.text!,
                     "hairlength":lblHairLength.text!,"ethnicity":lblEthnicity.text!,"hairstyle":lblHairStyle.text!,
                     "relation":lblSelectRelation.text!,"unique_sign":txtVUniqueSigns.text!,"special_health_condition":txtVSpecialNeeds.text!   ]
        var endPoint = ApiManager.addMissingPerson_endPoint
        if person != nil{
            param["template_id"] = person.id
            endPoint = ApiManager.updateMissingPerson_endPoint
        }
        missingPersonVM.addOrUpdateMissingPerson(viewController: self, parameter: param, picData: selectedProfileData, endPoint: endPoint) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 0{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Error_Alert.Contact_NotAddedd, viewController: self)
                }else{
                    Enough.shared.showAlertOneAction(title: "Done", okayTitle: Alert_Msg.Ok, message: "\(self.txtFName.text!) \(self.txtFLastName.text!) added succefully", viewController: self) {
                        self.personDelegate?.personAdded()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    @IBAction func selectPickerContentAction(_ sender: UIButton) {
        if isDOB{
            Date_Formatter.dateFormat = "yyyy-MM-dd"
            txtFDOB.text = Date_Formatter.string(from: date_pickerView.date)
        }else{
            if isHeight{
                if lblCm.text! == "cm"{
                    let value = String(cmArray[pickerView.selectedRow(inComponent: 0)])
                    lblHeight.text = value
                }else{
                    let value = String(ftArray[pickerView.selectedRow(inComponent: 0)])
                    lblHeight.text = value
                }
            }else{
                if lblKg.text! == "kg"{
                    let value = String(kgArray[pickerView.selectedRow(inComponent: 0)])
                    lblWeight.text = value
                }else{
                    let value = String(lbsArray[pickerView.selectedRow(inComponent: 0)])
                    lblWeight.text = value
                }
            }
        }
        UIView.animate(withDuration: 0.3) {
            self.height_picker.constant = 0
            self.view.layoutIfNeeded()
        }
    }
        // Support Method
    func isValidUserEntry() -> (Bool,String) {
        if lblSelectRelation.text! == "Select" {
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Miss_Relation)
        }
        if txtFName.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Username)
        }
        if txtFLastName.text!.isEmpty {
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Lastname)
        }
        if txtFAlias.text!.isEmpty {
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Alias)
        }
        if txtFDOB.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_DOB)
        }
        if lblHeight.text! == "Height"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Miss_Height)
        }
        if lblWeight.text! == "Weight"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Miss_Weight)
        }
        if lblGender.text! == "Gender"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Gender)
        }
        if lblBuild.text! == "Build"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Build)
        }
        if lblEye.text! == "Eye"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Eye)
        }
        if lblHairColor.text! == "Hair color"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_HairColor)
        }
        if lblGlasses.text! == "Glasses"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Glass)
        }
        if lblHairLength.text! == "Hair length"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_HairLeanth)
        }
        if lblEthnicity.text! == "Ethnicity"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Ethnicity)
        }
        if lblHairStyle.text! == "Hair style"{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_HairStyle)
        }
        if txtVUniqueSigns.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_Sign)
        }
        if txtVSpecialNeeds.text!.isEmpty{
            checkButtonEnable(isValid: false)
            return (false,Error_Alert.Missing_HealthCondition)
        }
        if selectedProfileData == nil{
            if lbl_selectImg.text!.isEmpty{
                checkButtonEnable(isValid: false)
                return (false,Error_Alert.Missing_Pic)
            }
        }
        checkButtonEnable(isValid: true)
        return (true,"")
    }
    func checkButtonEnable(isValid:Bool) {
        if isValid{
            btnSave.backgroundColor = UIColor(named: "darkPink")
            btnSave.isSelected = true
        }else{
            btnSave.backgroundColor = UIColor.lightGray
            btnSave.isSelected = false
        }
    }
}
extension MissingPersonTemplateViewController: RelationViewActions{
    func croBtnClick(sender:UIButton){
        
    }
    func okBtnClick(sender:UIButton){
        let selectedValue = sender.layer.value(forKey: "SelectedValue") as! String
        lblSelectRelation.text = selectedValue
        _ = isValidUserEntry()
    }
}

extension MissingPersonTemplateViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell")
        cell?.textLabel?.text = dropDownData[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let text = dropDownData[indexPath.row]
        selectedLabel.text = text
        tbl_dropDown.removeFromSuperview()
        _ = isValidUserEntry()
    }
}

extension MissingPersonTemplateViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isHeight{
            if lblCm.text! == "cm"{
                return cmArray.count
            }else{
                return ftArray.count
            }
        }else{
            if lblKg.text! == "kg"{
                return kgArray.count
            }else{
                return lbsArray.count
            }
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if isHeight{
            if lblCm.text! == "cm"{
                let cm = String(cmArray[row])
                return cm
            }else{
                let ft = String(ftArray[row])
                return ft
            }
        }else{
            if lblKg.text! == "Kg"{
                let kg = String(kgArray[row])
                return kg
            }else{
                let lbs = String(lbsArray[row])
                return lbs
            }
        }
    }
}
extension MissingPersonTemplateViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            if let data = img.jpegData(compressionQuality: 0.5){
                self.selectedProfileData = data
                let sizeKB = data.count/1024
                lbl_selectImg.text = "Select File (\(sizeKB)KB)"
                _ = isValidUserEntry()
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
extension MissingPersonTemplateViewController:UITextFieldDelegate,UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        _ = isValidUserEntry()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = isValidUserEntry()
    }
}
