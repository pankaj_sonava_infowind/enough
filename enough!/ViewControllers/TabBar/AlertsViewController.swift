//
//  AlertsViewController.swift
//  enough!
//
//  Created by mac on 01/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//
import UIKit
import CoreLocation

class AlertsViewController: UIViewController{
   
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var tblVOptions: UITableView!
    @IBOutlet weak var height_tbl: NSLayoutConstraint!
    @IBOutlet var view_triger: UIView!
    @IBOutlet weak var view_conditionNotFullFill: UIView!
    @IBOutlet weak var view_conditionFullFill: UIStackView!
    @IBOutlet weak var lbl_bottomSuggestion: UILabel!
    @IBOutlet weak var lbl_condition: UILabel!
    @IBOutlet weak var btn_triggerAlert: UIButton!
    var arrOption = [EnoughStatus_Field.create_acc,EnoughStatus_Field.complete_pro,EnoughStatus_Field.verified_acc,EnoughStatus_Field.create_code,EnoughStatus_Field.record_custom_audio,EnoughStatus_Field.addSafe_contact,EnoughStatus_Field.createRec_missPerson,EnoughStatus_Field.test_alert]
    var header:HeaderView?
    let verifiedVM = VerifyUserViewModel()
    
    var alerSettingsVM = AlertSettingViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getHome()
        self.viewSetup()
    }
    
    func getHome(){
        verifiedVM.getHomeSteps(viewController: self) {
            DispatchQueue.main.async {
                self.tblVOptions.reloadData()
            }
        }
    }
    func viewSetup() {
        btn_triggerAlert.imageView?.contentMode = .scaleAspectFit
        
        //lbl_createYurAcc.strikeThrough()
        height_tbl.constant = 44*CGFloat(arrOption.count)
        self.view_triger.frame = self.view.frame
        DispatchQueue.main.async {
            self.header = HeaderView(frame: self.view_header.frame)
            self.header?.headerDelegate = self
            self.view_header.addSubview(self.header!)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tblVOptions.reloadData()
    }
    @IBAction func close_trigerViewBtnAction(_ sender: Any) {
        view_triger.removeFromSuperview()
    }
    @IBAction func okTrigerViewBtnAction(_ sender: Any) {
        view_triger.removeFromSuperview()
    }
}
extension AlertsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOption.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell") as! OptionsTableViewCell
        cell.lblOption.text = arrOption[indexPath.row]
        cell.setRowData(status: Enough.shared.checkAcc_status(field: arrOption[indexPath.row]))
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrOption[indexPath.row] == EnoughStatus_Field.complete_pro{
            let myProfileVC = storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
            navigationController?.pushViewController(myProfileVC, animated: true)
        }else if arrOption[indexPath.row] == EnoughStatus_Field.addSafe_contact{
            let myContactVC = storyboard?.instantiateViewController(withIdentifier: "MyContactsViewController") as! MyContactsViewController
            myContactVC.delegate = self
            navigationController?.pushViewController(myContactVC, animated: true)
        }else if arrOption[indexPath.row] == EnoughStatus_Field.createRec_missPerson{
            let myRecordVC = storyboard?.instantiateViewController(withIdentifier: "MyRecordsViewController") as! MyRecordsViewController
            myRecordVC.delegate = self
            navigationController?.pushViewController(myRecordVC, animated: true)
        }else if arrOption[indexPath.row] == EnoughStatus_Field.test_alert{
            if !Enough.shared.checkAcc_status(field: arrOption[indexPath.row]){
                if Enough.shared.triggerMode == .off{
                    view.addSubview(view_triger)
                    view_conditionFullFill.isHidden = false
                    view_conditionNotFullFill.isHidden = true
                    lbl_bottomSuggestion.isHidden = false
                }else{
                    trigerAlert()
                }
            }else{
                view.addSubview(view_triger)
                view_conditionFullFill.isHidden = true
                view_conditionNotFullFill.isHidden = false
                lbl_bottomSuggestion.isHidden = true
                lbl_condition.text = Enough.shared.getRemainingCondition(field: arrOption[indexPath.row])
            }
        }else if arrOption[indexPath.row] == EnoughStatus_Field.create_code{
            let myRecordVC = storyboard?.instantiateViewController(withIdentifier: "MyCodesViewController") as! MyCodesViewController
            myRecordVC.delegate = self
            navigationController?.pushViewController(myRecordVC, animated: true)
        }else if arrOption[indexPath.row] == EnoughStatus_Field.verified_acc{
            let VerifyAccountVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyAccountViewController") as! VerifyAccountViewController
            VerifyAccountVC.modalPresentationStyle = .overCurrentContext
            VerifyAccountVC.modalTransitionStyle = .crossDissolve
            //VerifyAccountVC.delegate = self
            self.present(VerifyAccountVC, animated: true, completion: nil)
        }
    }
    func trigerAlert(){
        alerSettingsVM.createTrigerAlert(viewController: self) { (result) in
            DispatchQueue.main.async {
                if result["status"]?.int == 1{
                    let trigeredAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "TriggeredAlertViewController") as! TriggeredAlertViewController
                    self.navigationController?.pushViewController(trigeredAlertVC, animated: true)
                }else{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                }
            }
        }
    }
}
extension AlertsViewController:HeaderProtocol,MissingPersonUpdate,SafeContactUpdate,CodeUpdate{
func updateHome() {
    getHome()
}
    func okButtonPress(isLive: Bool) {
        if isLive{
            SettingsViewModel().updateUserMode(userMode: "live", viewController: self) {
                DispatchQueue.main.async {
                    self.header?.checkTriggerMode()
                }
            }
            //self.header?.btnTestLiveToggle.isSelected = true
        }else{
            self.header?.btnOnOfToggle.isSelected = true
        }
        Enough.shared.triggerMode = .on
        self.header?.checkTriggerMode()
    }
    func closeButtonPress(isLive: Bool) {
    }
}
