//
//  MapViewController.swift
//  enough!
//
//  Created by mac on 01/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
       @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
    }

}
