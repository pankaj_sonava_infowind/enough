//
//  TabBarViewController.swift
//  enough!
//
//  Created by mac on 01/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:UIFont(name: "NunitoSans-Regular", size: 16)]
        appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if Enough.hasSafeArea{
            tabBar.frame.size.height = 100
            tabBar.frame.origin.y = view.frame.height - 100
        }else{
            tabBar.frame.size.height = 76
            tabBar.frame.origin.y = view.frame.height - 76
        }
    }
}
