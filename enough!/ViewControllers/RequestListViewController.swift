//
//  RequestListViewController.swift
//  enough!
//
//  Created by Pankaj Sonava on 17/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class RequestListViewController: UIViewController {

    @IBOutlet weak var tbl_req: UITableView!
    let contactVM = AddContactViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRequestList()
    }
    func getRequestList()  {
        contactVM.getRequestsList(viewController: self) {
            DispatchQueue.main.async {
                self.tbl_req.reloadData()
            }
        }
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
extension RequestListViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactVM.allRequest.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTVC") as! RequestTVC
        cell.requestDetails = contactVM.allRequest[indexPath.row]
        cell.btn_cancel.addTarget(self, action: #selector(cancelReq(sender:)), for: .touchUpInside)
        cell.btn_accept.addTarget(self, action: #selector(acceptReq(sender:)), for: .touchUpInside)
        cell.btn_cancel.tag = indexPath.row
        cell.btn_accept.tag = indexPath.row
        return cell
    }
    @objc func acceptReq(sender:UIButton){
        let req_id = sender.layer.value(forKey: "req_id") as! String
        contactVM.aaceptReq(req_id: req_id, viewController: self) { result in
            DispatchQueue.main.async {
                if result["status"]?.int == 0{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                }else{
                    Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.Req_Accepted, viewController: self) {
                        self.contactVM.allRequest.remove(at: sender.tag)
                        self.tbl_req.reloadData()
                    }
                }
            }
        }
    }
    @objc func cancelReq(sender:UIButton){
        let req_id = sender.layer.value(forKey: "req_id") as! String
        contactVM.cancelReq(req_id: req_id, viewController: self) { result in 
            DispatchQueue.main.async {
                if result["status"]?.int == 0{
                    Enough.shared.showOnlyAlert(message: result["message"]?.string ?? Alert_Msg.Wrong, viewController: self)
                }else{
                    Enough.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: Alert_Msg.Req_Deleted, viewController: self) {
                        self.contactVM.allRequest.remove(at: sender.tag)
                        self.tbl_req.reloadData()
                    }
                }
            }
        }
    }
}
