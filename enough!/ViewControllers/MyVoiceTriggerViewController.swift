//
//  MyVoiceTriggerViewController.swift
//  enough!
//
//  Created by mac on 09/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class MyVoiceTriggerViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
 
    @IBOutlet weak var tblVVoiceTriggers: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 4
        }else {
            return 4
        }
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVVoiceTriggers.dequeueReusableCell(withIdentifier: "VoiceCell") as! VoiceTriggersTableViewCell
        if indexPath.section == 0{
            cell.view.backgroundColor = .lightGray
            cell.btnPlay.tag = indexPath.row
            cell.btnMicroPhone.tag = indexPath.row
            cell.lblName.textColor = .darkGray
            cell.lblType.textColor = .darkGray
            cell.btnCross.isHidden = true
            cell.btnTest.isHidden = true
            cell.btnPlay.addTarget(self, action: #selector(playBtnAction(sender:)), for: .touchUpInside)
            cell.btnMicroPhone.addTarget(self, action: #selector(microphoneBtnAction(sender:)), for: .touchUpInside)
        }else{
            cell.view.backgroundColor = UIColor(named: "lightPink")
            cell.btnTest.tag = indexPath.row
            cell.btnPlay.tag = indexPath.row
            cell.btnCross.tag = indexPath.row
            cell.btnMicroPhone.tag = indexPath.row
            cell.btnTest.addTarget(self, action: #selector(testBtnAction(sender:)), for: .touchUpInside)
            cell.btnPlay.addTarget(self, action: #selector(playBtnAction(sender:)), for: .touchUpInside)
            cell.btnCross.addTarget(self, action: #selector(crossBtnAction(sender:)), for: .touchUpInside)
            cell.btnMicroPhone.addTarget(self, action: #selector(microphoneBtnAction(sender:)), for: .touchUpInside)
        }
        return cell
     }
    @objc func testBtnAction(sender:UIButton){
        let TastVoiceCommandPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "TastVoiceCommandPopupViewController") as! TastVoiceCommandPopupViewController
        TastVoiceCommandPopupVC.modalPresentationStyle = .overCurrentContext
        TastVoiceCommandPopupVC.modalTransitionStyle = .crossDissolve
        self.present(TastVoiceCommandPopupVC, animated: true, completion: nil)
    }
    @objc func playBtnAction(sender:UIButton){
           
       }
    @objc func crossBtnAction(sender:UIButton){
        let DeleteVoiceCommandPopUpVC = self.storyboard?.instantiateViewController(withIdentifier: "DeleteVoiceCommandPopUpViewController") as! DeleteVoiceCommandPopUpViewController
        DeleteVoiceCommandPopUpVC.modalPresentationStyle = .overCurrentContext
        DeleteVoiceCommandPopUpVC.modalTransitionStyle = .crossDissolve
        self.present(DeleteVoiceCommandPopUpVC, animated: true, completion: nil)
       }
    @objc func microphoneBtnAction(sender:UIButton){
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tblVVoiceTriggers.dequeueReusableCell(withIdentifier: "SectionHeader") as! SectionHeader
        if section == 1{
            view.lblHeader.text = "MY HOUSEHOLD"
            return view
        }
        return view
    }
     
}
