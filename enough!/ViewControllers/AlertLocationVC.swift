//
//  AlertLocationVC.swift
//  enough!
//
//  Created by Pankaj Sonava on 07/08/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import GoogleMaps

class AlertLocationVC: UIViewController {
    
    @IBOutlet weak var btn_cancelResponse: UIButton!
    @IBOutlet weak var btn_updateAlert: UIButton!
    @IBOutlet weak var btn_viewAlert: UIButton!
    @IBOutlet weak var lbl_verifiedOrNot: UILabel!
    @IBOutlet weak var lbl_nameAlertUser: UILabel!
    @IBOutlet weak var imgAlertUser: UIImageView!
    @IBOutlet weak var map_view: GMSMapView!
    
    var alert:Alert!
    var alertList:[Alert]!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUp()
    }
    func viewSetUp() {
        btn_viewAlert.titleLabel?.underline()
        btn_cancelResponse.titleLabel?.underline()
        lbl_nameAlertUser.text = alert.user.name
        var img_url = ApiManager.base_url + alert.user.profile_image
        if let range = img_url.range(of: "./../"){
            img_url.removeSubrange(range)
        }
        imgAlertUser.sd_setImage(with: URL(string: img_url)) { (image, error, cacheType, url) in
            if let img = image{
                self.imgAlertUser.image = img
            }
        }
        let camera = GMSCameraPosition.camera(withLatitude: Double(alert.latitude ?? "0") ?? 0.0, longitude:Double(alert.longitude ?? "0") ?? 0.0, zoom: 16.0)
        map_view.camera = camera
        setMarkerOnMap(image: UIImage(named: "red_marker")!, select_alert: alert)
        for alrt in alertList {
            setMarkerOnMap(image: UIImage(named: "pink_marker")!, select_alert: alrt)
        }
    }
    func setMarkerOnMap(image:UIImage,select_alert:Alert) {
        let markerImage = image
        let markerView = UIImageView(image: markerImage)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(select_alert.latitude ?? "0") ?? 0.0, longitude: Double(select_alert.longitude ?? "0") ?? 0.0)
        marker.iconView = markerView
        marker.title = select_alert.user.name
        marker.snippet = ""
        marker.map = map_view
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func updateAlertAction(_ sender: UIButton){
    }
    @IBAction func cancelRes_btnAction(_ sender: Any) {
    }
    @IBAction func viewAlertBtnAction(_ sender: Any) {
    }
}
