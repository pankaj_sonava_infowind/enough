//
//  TriggeredAlertViewController.swift
//  enough!
//
//  Created by mac on 06/07/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class TriggeredAlertViewController: UIViewController {
    
    @IBOutlet weak var view_header: UIView!
    var header:HeaderView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    func viewSetup() {
        DispatchQueue.main.async {
            self.header = HeaderView(frame: self.view_header.frame)
            self.view_header.addSubview(self.header!)
            self.perform(#selector(self.callAfterMoment), with: nil, afterDelay: 0.1)
        }
    }
    @objc func callAfterMoment() {
        self.header?.hideButtons()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewSetup()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    @IBAction func actionCancel(_ sender: Any) {
       
    }
    
    @IBAction func actionView(_ sender: Any) {
    }
    
    @IBAction func actionDomestic(_ sender: Any) {
    }
    
    @IBAction func actionPSIndoor(_ sender: Any) {
    }
    
    @IBAction func actionPSOutdoor(_ sender: Any) {
    }
    
    @IBAction func actionUpdateAlert(_ sender: Any) {
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
